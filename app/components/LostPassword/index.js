import React, { Component } from 'react';
import { Link } from 'react-router';

import FormLostPassword from './FormLostPassword';
import ToroBrand from 'components/shared/ToroBrand'

export default class LostPassword extends Component {
  render() {
    return (
      <div>
      <div className="login-box">
        <div className="login-logo">
        <Link to="index2.html" className="logo">
          <ToroBrand />
        </Link>
        </div>

          <FormLostPassword/>

          {/*
          <div className="social-auth-links text-center">
            <p>- OR -</p>
            <a href="#" className="btn btn-block btn-social btn-facebook btn-flat"><i className="fa fa-facebook"></i> Sign in using
              Facebook</Link>
            <a href="#" className="btn btn-block btn-social btn-google btn-flat"><i className="fa fa-google-plus"></i> Sign in using
              Google+</Link>
          </div>
          */}


      </div>

      </div>




    );
  }
}
