import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import  LoadingOverlay  from 'components/shared/LoadingOverlay';
import { actionCreators } from 'actions/auth';

@connect(
  (store) => ({
    loading: store.get('auth').loading,
    errorMessageAlUsuario: store.get('auth') && store.get('auth').errorMessageAlUsuario ? store.get('auth').errorMessageAlUsuario : '',
  }),
  (dispatch) => ({
    fetchLogin: bindActionCreators(actionCreators, dispatch).fetchLogin
  })
)

export default class FormLostPassword extends Component {

  static propTypes = {
    loading: React.PropTypes.bool,
    errorMessageAlUsuario: React.PropTypes.string,
    fetchLogin: React.PropTypes.func.isRequired
  }

  constructor(props, context) {
    super(props, context);

    this.state = {
      username: '',
      password: ''
    };

    this.handleUserChange = this.handleUserChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleUserChange(e) {
    this.setState({
      username: e.target.value
    })
  }

  handlePasswordChange(e) {
    this.setState({
      password: e.target.value
    })
  }

  handleSubmit(e) {
    e.preventDefault();

    this.props.fetchLogin(this.state.username, this.state.password)

    return false;
  }

  render() {
    let result = null;

      result = (
        <div>



        <div className="login-box-body box">
        { this.props.loading ?  <LoadingOverlay /> : ''}
        <p className="login-box-msg">Recuperar Contraseña</p>
        <form onSubmit={this.handleSubmit}>
          <div className="form-group has-feedback">
            <input type="email" className="form-control" placeholder="Email"  onChange={this.handleUserChange} value={this.state.username} autoFocus="true" />
            <span className="glyphicon glyphicon-envelope form-control-feedback"></span>
          </div>
          <div className="row">
            <div className="col-xs-4 col-xs-offset-8">
              <div className="form-group">
              <input type="submit" className="btn btn-primary btn-block btn-flat pull-right" value='Recuperar'/>

              </div>
            </div>
          </div>

        </form>

        { this.props.errorMessageAlUsuario ?
          <div className="form-group has-error">
                  <span className="help-block">{this.props.errorMessageAlUsuario}</span>
         </div> : ''}



      </div>
        </div>


      );

    return result;
  }

}
