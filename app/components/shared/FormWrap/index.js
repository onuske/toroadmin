import React from 'react';
import _ from 'lodash';

// import InputHidden from 'components/InputHidden';
// import InputText from 'components/InputText';
// import InputLocation from 'components/InputLocation';
// import InputChoice from 'components/InputChoice';
import LoadingOverlay from '../LoadingOverlay';
import SuccessOverlay from '../SuccessOverlay';

export default class FormWrap extends React.Component {

  static propTypes = {
    titulo: React.PropTypes.string.isRequired,
    onSubmit: React.PropTypes.func.isRequired,
    loading: React.PropTypes.bool,
    errorMessage: React.PropTypes.string,
    successMessage: React.PropTypes.string,
    success: React.PropTypes.bool,
    children: React.PropTypes.array
    // onSuccess: React.PropTypes.func
  }

  constructor(props, context) {
    super(props, context);
    this.state = {
      //formFields: {},
      errorMessage: props.errorMessage
    };
    this.formFields= {};
    //this.onInputChange = this.onInputChange.bind(this);
    this.onFormSubmit = this.onFormSubmit.bind(this);

    //this.childrens = {};
  }

  /*componentWillReceiveProps(nextProps) {e
    if (nextProps.errorMessage) {
      this.setState({ errorMessage: nextProps.errorMessage })
    }
    if (nextProps.success && !this.props.success && this.props.onSuccess) {
      this.props.onSuccess(this.state.formFields)
    }
  }*/

  /*onInputChange(event) {
    const nextState = this.state;
    if (event.target.value.length > 0) {
      nextState.formFields[event.target.id] = event.target.value;
    } else {
      delete nextState.formFields[event.target.id];
    }
    this.setState(nextState);
    // reset error status
    this.childrens[event.target.id].cleanErrorMessage();
    this.resetFormError();
  }*/

  onFormSubmit() {
    React.Children.map(this.props.children, (child) => {});

    // colectar datos para la validación
    const missingFields = [];
    _.forOwn(this.props.children,
      (child) => {
        if (child.props.mandatory) {
          if ((child.props.value === '') || (child.props.value === undefined)) {
            missingFields.push(child)
          }
        }
      }
    );

    // validar datos
    if (missingFields.length > 0) {
      let errorMessage = 'Falta completar el campo ';
      if (missingFields.length > 1) {
        errorMessage = 'Faltan completar los campos: ';
      }

      missingFields.map((missingField) => {
        errorMessage += missingField.props.name + ', ';
      });

      errorMessage = errorMessage.substring(0, errorMessage.length - 2);
      this.setState({ errorMessage: errorMessage });
      return;
    }

    this.setState({ errorMessage: null });

    React.Children.map(this.props.children, (child) => {
      this.formFields[child.props.id] = child.props.value;
    });
    this.props.onSubmit(this.formFields);
    /*
    const mandatoryFields = [];
    _.forOwn(this.childrens,
      (child) => {
        if (child.props.mandatory) {
          mandatoryFields.push(child.props.id)
        }
      }
    );
    const missingFields = _.difference(mandatoryFields, Object.keys(this.state.formFields));
    // validar datos
    if (missingFields.length > 0) {
      this.childrenWithProps = this.childrenWithProps.map(
        (child) => {
          let ret = child;
          if (missingFields.indexOf(child.props.id) >= 0) {
            ret = React.cloneElement(child, {
              errorMessage: 'Este campo es necesario'
            })
          }
          return ret;
        }
      );
      this.forceUpdate();
      return;
    }
    this.props.onSubmit(this.state.formFields)
    */
  }

  resetFormError() {
    this.setState({ errorMessage: null });
  }

  render() {
    React.Children.map(this.props.children, (child) => {});
    return (
      <div className="col-md-12">
        <div className="box box-primary">
          { this.props.loading ? <LoadingOverlay /> : ''}
          { this.props.success ? <SuccessOverlay /> : ''}
          <div className="box-header with-border">
            <h3 className="box-title">{this.props.titulo}</h3>
          </div>
          <div className="box-body">
            <form>
              {this.props.children}
            </form>
          </div>
          <div className={`box-footer ${(this.state.errorMessage ? 'has-error' : '')} ${(this.props.success ? 'has-success' : '')}`}>
            { this.props.loading ?
              <button type="submit" className="btn btn-primary disabled" >cargando</button>
              :
                <button type="submit" className="btn btn-primary" onClick={this.onFormSubmit}>Guardar</button>
            }
            { this.state.errorMessage ? <span className="help-block">{this.state.errorMessage}</span> : '' }
            { this.props.success ? <span className="help-block">{this.props.successMessage}</span> : '' }
          </div>
        </div>
      </div>
    );
  }
  /*
  render() {
    // inyecto la function que captura los cambios en todos los inputs para poder mandarlos en el request
    if (!this.childrenWithProps) {
      this.childrenWithProps = React.Children.map(this.props.children,
       (child) => {
         let result = child;
         // take the default value of hidden inputs
         if (child.type === InputHidden) {
           const nextState = this.state;
           nextState.formFields[child.props.id] = child.props.value;
           this.setState(nextState);
         } else if((child.type === InputChoice)||(child.type === InputText)){
           result = React.cloneElement(child, {
             onChange: this.onInputChange,
             ref: (input) => { if (input) this.childrens[input.props.id] = input; }
           })
         }
         return result;
       }
      );
    }

    return (
      <div className="col-md-12">
        <div className="box box-primary">
          { this.props.loading ? <LoadingOverlay /> : ''}
          { this.props.success ? <SuccessOverlay /> : ''}
          <div className="box-header with-border">
            <h3 className="box-title">{this.props.titulo}</h3>
          </div>
          <div className="box-body">
            <form>
              {this.childrenWithProps}
            </form>
          </div>
          <div className={`box-footer ${(this.state.errorMessage ? 'has-error' : '')} ${(this.props.success ? 'has-success' : '')}`}>
            { this.props.loading ?
              <button type="submit" className="btn btn-primary disabled" >cargando</button>
              :
                <button type="submit" className="btn btn-primary" onClick={this.onSubmit}>Guardar</button>
            }
            { this.state.errorMessage ? <span className="help-block">{this.state.errorMessage}</span> : '' }
            { this.props.success ? <span className="help-block">{this.props.successMessage}</span> : '' }
          </div>
        </div>
      </div>
    );
  }
  */

}
