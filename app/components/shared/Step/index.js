import React from 'react';

export default class Step extends React.PureComponent {

  // stepTitle is used by SteppedProcess component and thus required here
  static propTypes = {
    stepTitle: React.PropTypes.string.isRequired, // eslint-disable-line react/no-unused-prop-types
    header: React.PropTypes.node,
    children: React.PropTypes.node
  };

  render() {
    return (
      <div className="row">
        <section className="col-sm-12 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2">
          {this.props.header}
          {this.props.children}
        </section>
      </div>
    )
  }
}
