import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Select from 'react-select';

import { actionCreators } from 'actions/layout';
import { actionCreators as actionCreatorsApi } from 'actions/api';

@connect(
  (store) => ({
    layout: store.get('layout') || {},
    obtenerPeriodosState: store.get('api').obtenerPeriodos || {loading: true}

  }),
  (dispatch) => ({
    obtenerPeriodos: bindActionCreators(actionCreatorsApi, dispatch).obtenerPeriodos,
    obtenerPeriodosReset: bindActionCreators(actionCreators, dispatch).selectPeriodo,
    selectPeriodo: bindActionCreators(actionCreators, dispatch).selectPeriodo
  })
)
export default class SelectPeriodo extends React.Component {

  static propTypes = {
    layout: React.PropTypes.object, // eslint-disable-line react/no-unused-prop-types
    obtenerPeriodos: React.PropTypes.func,
    selectPeriodo: React.PropTypes.func,
    periodoActual: React.PropTypes.func,
    diaCierre: React.PropTypes.string,
    rangoPeriodos: React.PropTypes.array, // rangoPeriodos={[-9,3]} el 0 cuenta
    value: React.PropTypes.string,
    onChange: React.PropTypes.func
  }

  constructor(props) {
    super(props);
    this.state = { options: [], defaultIdPeriodoSelected: '', idPeriodoActual: '' }
    this.setChange = this.setChange.bind(this);
    this.buildCombo = this.buildCombo.bind(this);
  }

  componentWillMount() {

    if(this.props.rangoPeriodos && this.props.diaCierre) {
      let rangeFrom = this.props.rangoPeriodos[0];
      let rangeTo = this.props.rangoPeriodos[1];
      let currPeriodoFormatted = null;

      var now = new Date();
      var from = new Date();
      from.setMonth(from.getMonth() + rangeFrom)
      var to = new Date();
      to.setMonth(to.getMonth() + rangeTo)

      var data = [];
      while(from <= to){
        from.setMonth(from.getMonth()+1)
        var month = from.getMonth();
        month++;
        var year = from.getFullYear();
        var periodoFormatted = `${(month < 10 ? `0${month}` : month)}${year}`;

        data.push(
          {
            idPeriodo: periodoFormatted
          }
        );

        if(month == now.getMonth()+1 && year == now.getFullYear()){
          currPeriodoFormatted = periodoFormatted;
        }
      }

      this.buildCombo(data.reverse(), currPeriodoFormatted);

    }else{

      if (!_.isEmpty(this.props.obtenerPeriodosState.data)) {
        this.buildCombo(this.props.obtenerPeriodosState.data)
      } else if (this.props.layout.consorcio ) {
        this.props.obtenerPeriodos({ idConsorcio: this.props.layout.consorcio.idConsorcio });
      }
    }
  }

  buildCombo(data, idPeriodoActual) {

    var values = [];
    for(var i in data){
      values.push(
        {
          value: data[i].idPeriodo,
          label: data[i].idPeriodo
        }
      );
    }

    if (idPeriodoActual) {
      this.setState({ options : values });
      this.props.selectPeriodo(idPeriodoActual)
      this.props.onChange(idPeriodoActual)
    } else {

      idPeriodoActual = values[0].value;
      this.setState({ options : values, defaultIdPeriodoSelected: values[0].value, idPeriodoActual: idPeriodoActual });

      if (this.props.layout.consorcioSelected && !this.props.layout.consorcioSelected.idPeriodoSelected) {
        this.props.selectPeriodo(idPeriodoActual, idPeriodoActual);
      }
    }

  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.obtenerPeriodosState.data && !this.props.obtenerPeriodosState.data) {
      var sortedData = nextProps.obtenerPeriodosState.data.reverse();
      this.buildCombo(sortedData);
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (!this.props.rangoPeriodos && (prevProps.layout.consorcio.idConsorcio !== this.props.layout.consorcio.idConsorcio)) {
      this.props.obtenerPeriodos({ idConsorcio: this.props.layout.consorcio.idConsorcio });
    }
  }

  setChange(val) {
    if(this.props.onChange) {
      this.props.onChange(val.value)
    }
    this.props.selectPeriodo(val.value);
  }

  render() {
    return (
      <div>

        {
          this.props.layout.consorcioSelected && this.props.layout.consorcioSelected.idPeriodoSelected
        ?
          <Select
            name="form-field-name"
            value={this.props.layout.consorcioSelected.idPeriodoSelected}
            options={this.state.options}
            onChange={this.setChange}
          />
        :
          ""
        }

      </div>
    );
  }
}
