import React from 'react';

export default class InputMoney extends React.Component {

  static propTypes = {
    id: React.PropTypes.string.isRequired,
    name: React.PropTypes.string.isRequired,
    onChange: React.PropTypes.func,
    mandatory: React.PropTypes.bool,
    errorMessage: React.PropTypes.string,
    value: React.PropTypes.string,
    inputStyle: React.PropTypes.string,
    widthLabel: React.PropTypes.string,
    width: React.PropTypes.string
  }

  constructor(props, context) {
    super(props, context);
    this.state = {
      errorMessage: this.props.errorMessage, negativo: false
    };
    this.onChange = this.onChange.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.errorMessage !== this.state.errorMessage) {
      this.setState({ errorMessage: nextProps.errorMessage })
    }
  }

  onChange(e) {
    const target = e.target;
    const value = target.type === 'checkbox' ? target.checked : target.value.replace(/\D/g, '');
    if (target.type !== 'checkbox' && Math.sign(e.target.value) == 1 && this.state.negativo) {
      e.target.value = String(-e.target.value);
    } else {

      if (target.type === 'checkbox' && this.props.value) {
        if (!value) {
          e.target.value = String(Math.abs(this.props.value));
        } else if (Math.sign(this.props.value) == 1) {
          e.target.value = String(-this.props.value);
        }
        e.target.id = this.props.id;
        this.setState({ negativo: value })
      }
    }
    parseInt(e.target.value);

    this.props.onChange(e);
  }

  cleanErrorMessage() {
    this.setState({ errorMessage: '' })
  }

  render() {
    let widthLabel = 2, widthInput = 10, width = 12; // col-sm-12
    if (this.props.inputStyle === 'inline') {

      if (this.props.widthLabel) widthLabel = this.props.widthLabel;
      widthInput = 12 - widthLabel;
    }
    if (this.props.width) width = this.props.width;

    return (
      <div className={`form-group ${(this.state.errorMessage ? 'has-error' : '')} col-sm-${width}`}>
        <label className={this.props.inputStyle === 'inline' ? `col-sm-${widthLabel} control-label` : 'control-label'} htmlFor={this.props.name}>{this.props.name} {this.props.mandatory ? '*' : ''}</label>
        <div className={`input-money ${this.props.inputStyle === 'inline' ? 'col-sm-'+widthInput : ''}`}>
          <div className="input-group">
            {                           
              this.props.inputStyle === 'inline'
            ?
              <div>
                <input
                  id={this.props.id}
                  type="text"
                  name={this.props.name}
                  onChange={this.onChange}
                  value={this.props.value}
                  className="form-control"
                />
              </div>
            :
              <input
                id={this.props.id}
                type="text"
                name={this.props.name}
                onChange={this.onChange}
                value={this.props.value}
                className="form-control"
              />
            }
            <span className="input-group-addon">
            <div>
              <div>
                <input type="checkbox" onChange={this.onChange}/>
              </div>
              <div>
                <label className="control-label">Negativo</label>
              </div>
            </div>
            </span>
          </div>
          {
            this.state.errorMessage
          ?
            <span className="help-block">{this.state.errorMessage}</span>
          :
            ""
          }
        </div>
      </div>
    );
  }
}
