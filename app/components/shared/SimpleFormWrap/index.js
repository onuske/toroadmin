import React from 'react';
import _ from 'lodash';

import LoadingOverlay from '../LoadingOverlay';
import SuccessOverlay from '../SuccessOverlay';

export default class SimpleFormWrap extends React.Component {

  static propTypes = {
    titulo: React.PropTypes.string.isRequired,
    onSubmit: React.PropTypes.func,
    loading: React.PropTypes.bool,
    errorMessage: React.PropTypes.string,
    successMessage: React.PropTypes.string,
    success: React.PropTypes.bool,
    children: React.PropTypes.array,
    formStyle: React.PropTypes.string,
    disabled: React.PropTypes.bool
    // onSuccess: React.PropTypes.func
  }

   constructor(props, context) {
    super(props, context);
    this.state = {
      //formFields: {},
      errorMessage: props.errorMessage
    };
    this.formFields= {};
    this.onFormSubmit = this.onFormSubmit.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.errorMessage && nextProps.errorMessage !== this.props.errorMessage) {
      const firstWord = nextProps.errorMessage.split(" ")
      if (firstWord[0]) {
        const missingFields = [];
        _.forOwn(this.props.children, (child, key) => {
          if ((child.props.value !== '') && (child.props.id === firstWord[0])) {
            missingFields.push({ id: child.props.id, name: child.props.name, errorMessage: nextProps.errorMessage })
            this.props.onSubmit(missingFields);
            this.setState({ errorMessage: "el campo "+ firstWord[0] + " es incorrecto"});
          }
        })
      }
    }
  }


  onFormSubmit() {
    React.Children.map(this.props.children, (child) => {});

    // colectar datos para la validación
    const missingFields = [];
    _.forOwn(this.props.children,
      (child, key) => {
        if (child && child.props.mandatory) {
          if ((child.props.value === '') || (child.props.value === undefined)) {
            missingFields.push({ id: child.props.id, name: child.props.name, errorMessage: "Falta completar el campo" })
          }
        }
      }
    );

    // validar datos
    if (missingFields.length > 0) {
      let errorMessage = 'Falta completar el campo ';
      if (missingFields.length > 1) {
        errorMessage = 'Faltan completar los campos: ';
      }

      missingFields.map((missingField) => {
        errorMessage += missingField.name + ', ';
      });

      errorMessage = errorMessage.substring(0, errorMessage.length - 2);
      this.setState({ errorMessage: errorMessage });
      this.props.onSubmit(missingFields);
      return;
    }

    this.setState({ errorMessage: null });

    // React.Children.map(this.props.children, (child) => {
    //   console.log("SUB--4", child)
    //   this.formFields[child.props.id] = child.props.value;
    // });
    this.props.onSubmit();
  }

  render() {
    return (
        <div className={`box box-primary ${(this.props.formStyle === 'inline' ? 'form-horizontal' : '')}`}>
          { this.props.loading ? <LoadingOverlay /> : ''}
          { this.props.success ? <SuccessOverlay /> : ''}
          <div className="box-header with-border">
            <h3 className="box-title">{this.props.titulo}</h3>
          </div>
          <div className="box-body">
            {this.props.children}
          </div>
          <div className={`box-footer ${(this.props.errorMessage ? 'has-error' : '')} ${(this.props.success ? 'has-success' : '')}`}>
          {
              this.props.onSubmit
            ?
              <div>
                {
                  this.props.loading
                ?
                  <button type="submit" className="btn btn-primary disabled" >cargando</button>
                :
                  <button type="submit" className="btn btn-primary" disabled={this.props.disabled} onClick={this.onFormSubmit}>Guardar</button>
                }
              </div>
            :
              ""
            }
            {
              this.props.onNext
            ?
              <button onClick={this.props.onNext} className="btn btn-success pull-right"> next </button>
            :
              ""
            }
            { this.state.errorMessage ? <span className="help-block">{this.state.errorMessage}</span> : '' }
            { this.props.success ? <span className="help-block">{this.props.successMessage}</span> : '' }
          </div>
        </div>
    );
  }

}
