import React from 'react';

import logoImg from '../../assets/img/logo-header.png'; // eslint-disable-line no-unused-vars

export default React.createClass({
  render: function() {
    return <div>
        <img src={logoImg} width="190" height="50" className="logo-header" alt="logo-toro" />
      </div>
  }
});
