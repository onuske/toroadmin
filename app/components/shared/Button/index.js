import React from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';

export default class Button extends React.PureComponent {

static propTypes = {};

constructor(props) {
    super(props);
  }

  render() {
    if (this.props.loading) {
      return (
        <button className={`${this.props.className}`} disabled={true}>
          <i className="fa fa-refresh fa-spin"></i> {this.props.children}
        </button>
      );
    } else {
      return (
        <button className={this.props.className} role="button" onClick={this.props.onClick}>
          {this.props.children}
        </button>
      );
    }
  }
}
