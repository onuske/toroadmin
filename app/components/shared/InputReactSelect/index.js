import React from 'react';
import Select from 'react-select';

export default class InputReactSelect extends React.Component {

  static propTypes = {
    id: React.PropTypes.any.isRequired,
    name: React.PropTypes.string,
    onChange: React.PropTypes.func,
    mandatory: React.PropTypes.bool,
    errorMessage: React.PropTypes.string,
    value: React.PropTypes.any,
    options: React.PropTypes.array,
    inputStyle: React.PropTypes.string,
    disabled: React.PropTypes.bool,
    widthLabel: React.PropTypes.string,
    width: React.PropTypes.string,
    clearable: React.PropTypes.bool
  }

  render() {
    let widthLabel = 2, widthInput = 10, width = 12; // col-sm-12
    if (this.props.inputStyle === 'inline') {
      if (this.props.widthLabel) widthLabel = this.props.widthLabel;
      widthInput = 12 - widthLabel;
    }
    if (this.props.width) width = this.props.width;

    return (
      <div className={`form-group ${(this.props.errorMessage ? 'has-error' : '')} col-sm-${width}`}>
        {
          this.props.name
        ?  
          <label className={this.props.inputStyle === 'inline' ? `col-sm-${widthLabel} control-label` : 'control-label'} htmlFor={this.props.id}>{this.props.name} {this.props.mandatory ? '*' : ''}</label>
        :
          ""  
        }
        {
          this.props.inputStyle === 'inline'
        ?
          <div className={`${this.props.inputStyle === 'inline' ? 'col-sm-'+widthInput : ''}`}>
            <Select
              id={this.props.id}
              options={this.props.options}
              onChange={(seleccion) => {this.props.onChange(this.props.id, seleccion.value)}}
              value={this.props.value}
              clearable={this.props.clearable}
              removeSelected={false}
              disabled={this.props.disabled}
            />
          </div>
        :
          <Select
            id={this.props.id}
            options={this.props.options}
            onChange={(seleccion) => {this.props.onChange(this.props.id, seleccion.value)}}
            value={this.props.value}
            clearable={this.props.clearable}
            removeSelected={false}
            disabled={this.props.disabled}
          />
        }
        {this.props.errorMessage
          ?
            <span className="help-block">{this.props.errorMessage}</span>
          :
            ""
         }
      </div>
    );
  }
}
