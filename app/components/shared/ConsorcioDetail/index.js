import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { actionCreators } from 'actions/api';

@connect(
  (store) => ({
    obtenerConsorcioState: store.get('api').obtenerConsorcio || {},
    consorcio: store.get('layout') && store.get('layout').consorcio ? store.get('layout').consorcio : {},
    unidades: store.get('layout') && store.get('layout').unidades ? store.get('layout').unidades : []
  }),
  (dispatch) => ({
    listarUnidades: bindActionCreators(actionCreators, dispatch).listarUnidades,
    obtenerConsorcio: bindActionCreators(actionCreators, dispatch).obtenerConsorcio
  })
)
export default class ConsorcioDetail extends React.Component {

  static propTypes = {
    idConsorcio: React.PropTypes.number.isRequired,
    listarUnidadesState: React.PropTypes.object,
    obtenerConsorcio: React.PropTypes.func.isRequired,
    obtenerConsorcioState: React.PropTypes.object,
    consorcio: React.PropTypes.object
  }

  componentWillMount() {
    if (this.props.idConsorcio) {
      this.props.listarUnidades({idConsorcio: this.props.idConsorcio});
      this.props.obtenerConsorcio({idConsorcio: this.props.idConsorcio});
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.idConsorcio && nextProps.idConsorcio !== this.props.idConsorcio) {
      this.props.listarUnidades({idConsorcio: nextProps.idConsorcio});
      this.props.obtenerConsorcio({idConsorcio: nextProps.idConsorcio});
    }
  }

  render() {

    let unidades = [];
    if (this.props.unidades) {
      unidades = this.props.unidades.map((unidad, index) => (
        (<dd key={index}>{unidad.numero} - {unidad.unidad} ( {unidad.descripcion} ) <small>[{unidad.coeficienteArancel}%]</small></dd>)
      ))
    }

    return (
      <div className="box box-solid">
        <div className="box-header with-border">
          <i className="fa fa-building"></i>
          <h3 className="box-title">Consorcio - {this.props.consorcio.nombre}</h3>
        </div>
        <div className="box-body">
          <dl className="dl-horizontal">
            <dt>Consorcio</dt>
              <dd key="1">{this.props.consorcio.nombre}</dd>
              <dd key="2">{`${this.props.consorcio.direccionCalle} ${this.props.consorcio.direccionNumero} - ${this.props.consorcio.localidad}`}</dd>
            <dt>Unidades</dt>
            {unidades}
          </dl>
        </div>
      </div>
    );
  }
}
