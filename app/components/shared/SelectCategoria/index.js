import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { actionCreators } from 'actions/api';
import InputReactSelect from 'components/shared/InputReactSelect';

const CATEGORIA = {
  PAGO_DE_EXPENSAS : 1,
  INGRESO_A_CUENTA : 2,
  GASTO: 3, // GASTOS_BANCARIOS : 6
  MOVIMIENTO_YA_LIQUIDADO : 4,
  COMPENSACION : 5,
}

@connect(
  (store) => ({
    subirArchivoState: store.get('api').subirArchivo || {},
    layout: store.get('layout') || {}
  }),
  (dispatch) => ({
    subirArchivo: bindActionCreators(actionCreators, dispatch).subirArchivo,
    subirArchivoReset: bindActionCreators(actionCreators, dispatch).subirArchivoReset
  })
)
export default class SelectCategoria extends Component {

  static propTypes = {
    item: React.PropTypes.object.isRequired,
    categorias: React.PropTypes.array,
    subCategorias: React.PropTypes.array,
    unidades: React.PropTypes.array,
    onChange: React.PropTypes.func,
    getUnidades: React.PropTypes.func,
    which: React.PropTypes.string,
    index: React.PropTypes.number,
    idMovimiento: React.PropTypes.number,
  }

  constructor(props) {
    super(props);
    this.setItemCombo = this.setItemCombo.bind(this);
    let item = props.which ? props.item : props.item.detail;
    let idCategoria = item.idCategoria !== null ? item.idCategoria : null ;
    let idSubCategoria = item.idSubCategoria !== null ? item.idSubCategoria : null ;
    let idUnidad = item.idUnidad !== null ? item.idUnidad : null ;
    this.state = {
      active: null,
      success: false,
      error: null,
      reset: false,
      loading: false,
      disabled: item.procesado ? true : false,
      inputValues: { "idCategoria": idCategoria, "idSubCategoria": idSubCategoria, "idUnidad": idUnidad }
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (!prevState.active && this.state.active) {
      let value = _.pickBy(this.state.inputValues, (val, key) => (val != null))
      this.props.onChange(this.props.idMovimiento, value, this.props.which, this.props.index)
      this.setState({ active: false });
    }
  }

  setItemCombo(index, value) {
    if (index === "idCategoria" && value === CATEGORIA.PAGO_DE_EXPENSAS && !this.props.unidades && this.props.getUnidades) {
      this.props.getUnidades()
    }

    this.setState((prevState, props) => {
      let inputValues = _.cloneDeep(prevState.inputValues);
      if (index === "idCategoria" && value === CATEGORIA.PAGO_DE_EXPENSAS ) {
        inputValues["idSubCategoria"] = null;
      } else if (index === "idCategoria" && value === CATEGORIA.GASTO) {
        inputValues["idUnidad"] = null;
      } else {
        inputValues["idUnidad"] = null;
        inputValues["idSubCategoria"] = null;
      }
      inputValues[index] = value;
      return { "inputValues": inputValues, active: true }
    })
  }

  render() {
    let categorias = !this.props.unidades ? _.filter(this.props.categorias, (elem) => (elem.value !== CATEGORIA.PAGO_DE_EXPENSAS)) : this.props.categorias 
    let item = this.props.item
    return (
      <div style={{display: 'flex'}} className="no-padding no-margin">
        <InputReactSelect
          id={"idCategoria"}
          onChange={this.setItemCombo}
          value={this.state.inputValues["idCategoria"]}
          options={categorias}
          errorMessage={this.state.inputValues['categoriaError']}
          clearable={false}
          disabled={this.state.disabled}
        />
        {
          this.state.inputValues["idCategoria"] === CATEGORIA.GASTO
        ?
          <InputReactSelect
            id={"idSubCategoria"}
            onChange={this.setItemCombo}
            value={this.state.inputValues["idSubCategoria"]}
            options={this.props.subCategorias}
            errorMessage={this.state.inputValues['subCategoriaError']}
            clearable={false}
            disabled={this.state.disabled}
          />
        :
          ""
        }
        {
          this.props.unidades && this.state.inputValues["idCategoria"] === CATEGORIA.PAGO_DE_EXPENSAS
        ?
          <InputReactSelect
            id={"idUnidad"}
            onChange={this.setItemCombo}
            value={this.state.inputValues["idUnidad"]}
            options={this.props.unidades}
            errorMessage={this.state.inputValues['unidadError']}
            clearable={false}
            disabled={this.state.disabled}
          />
        :
          ""
        }
      </div>
    );
  }

}