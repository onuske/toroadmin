import React from 'react';

export default class InputNumber extends React.Component {

  static propTypes = {
    id: React.PropTypes.string.isRequired,
    name: React.PropTypes.string.isRequired,
    onChange: React.PropTypes.func,
    mandatory: React.PropTypes.bool,
    errorMessage: React.PropTypes.string,
    value: React.PropTypes.number,
    inputStyle: React.PropTypes.string,
    widthLabel: React.PropTypes.string,
    width: React.PropTypes.string
  }

  constructor(props, context) {
    super(props, context);
    this.state = {
      errorMessage: this.props.errorMessage
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.errorMessage) {
      this.setState({ errorMessage: nextProps.errorMessage })
    }
  }

  cleanErrorMessage() {
    this.setState({ errorMessage: '' })
  }

  render() {
    let widthLabel = 2, widthInput = 10, width = 12; // col-sm-12
    if (this.props.inputStyle === 'inline') {
      if (this.props.widthLabel) widthLabel = this.props.widthLabel;
      widthInput = 12 - widthLabel;
    }
    if (this.props.width) width = this.props.width;

    return (
      <div className={`form-group ${(this.state.errorMessage ? 'has-error' : '')} col-sm-${width}`}>
        <label className={this.props.inputStyle === 'inline' ? `col-sm-${widthLabel} control-label` : 'control-label'} htmlFor={this.props.name}>{this.props.name} {this.props.mandatory ? '*' : ''}</label>
        {                           
          this.props.inputStyle === 'inline'
        ?
          <div className={`col-sm-${widthInput}`}>
            <input
              id={this.props.id}
              type="number"
              name={this.props.name}
              onChange={this.props.onChange}
              className="form-control"
              value={this.props.value}
            />
          </div>
        :
          <input
            id={this.props.id}
            type="number"
            name={this.props.name}
            onChange={this.props.onChange}
            className="form-control"
            value={this.props.value}
          />
        }
        {this.state.errorMessage
          ?
            <span className="help-block">{this.state.errorMessage}</span>
          :
           ''
         }
      </div>
    );
  }
}
