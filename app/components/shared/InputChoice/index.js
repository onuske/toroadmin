import React from 'react';

export default class InputChoice extends React.Component {

  static propTypes = {
    id: React.PropTypes.string.isRequired,
    name: React.PropTypes.string.isRequired,
    onChange: React.PropTypes.func,
    mandatory: React.PropTypes.bool,
    errorMessage: React.PropTypes.string,
    choices: React.PropTypes.array.isRequired,
    defaultChecked: React.PropTypes.string,
    value: React.PropTypes.string
  }

  constructor(props, context) {
    super(props, context);
    this.state = {
      errorMessage: this.props.errorMessage
    };

    this.localOnChange = this.localOnChange.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.errorMessage) {
      this.setState({ errorMessage: nextProps.errorMessage })
    }

    this.setState({ selectedOption: nextProps.value })
  }

  localOnChange(event) {
    if (this.props.onChange != null) {
      this.props.onChange({ target: { value: event.target.value, id: this.props.id } });
    }
  }

  cleanErrorMessage() {
    this.setState({ errorMessage: '' })
  }

  render() {
    const choices = this.props.choices.map((item, index) => {
      const defaultChecked = typeof item.defaultChecked !== 'undefined';
      return (
        <label className="radio-inline" htmlFor={`${this.props.id}_${index}`} key={index}>
          <input type="radio" checked={this.state.selectedOption === item.value} name={this.props.name} id={`${this.props.id}_${index}`} value={item.value} onChange={this.localOnChange} />
          {item.value}
        </label>
      )
    });

    return (

      <div className={`form-group ${(this.state.errorMessage ? 'has-error' : '')} col-sm-12`}>
        <label htmlFor={this.props.name}>{this.props.name} {this.props.mandatory ? '*' : ''}</label>
        <br />

        <div>
          {choices}
        </div>

        {this.state.errorMessage
          ?
            <span className="help-block">{this.state.errorMessage}</span>
          :
           ''
         }
      </div>
    );
  }
}
