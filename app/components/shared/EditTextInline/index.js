import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import ReactDOM from 'react-dom';

import { actionCreators } from 'actions/api';
import InputText from 'components/shared/InputText';

@connect(
  (store) => ({
    subirArchivoState: store.get('api').subirArchivo || {},
    layout: store.get('layout') || {}
  }),
  (dispatch) => ({
    subirArchivo: bindActionCreators(actionCreators, dispatch).subirArchivo,
    subirArchivoReset: bindActionCreators(actionCreators, dispatch).subirArchivoReset
  })
)
export default class EditTextInline extends Component {

  static propTypes = {
    id: React.PropTypes.number.isRequired,
    value: React.PropTypes.string,
    name: React.PropTypes.string,
    onChange: React.PropTypes.func,
    onSuccess: React.PropTypes.func,
    errorMessage: React.PropTypes.string,
    which: React.PropTypes.string,
    index: React.PropTypes.number,
    idMovimiento: React.PropTypes.number,
    loading: React.PropTypes.bool,
    success: React.PropTypes.bool,
    error: React.PropTypes.string
  }

  constructor(props) {
    super(props);
    this.onComponentValueChange = this.onComponentValueChange.bind(this);
    this.onClick = this.onClick.bind(this)
    this.saveChanges = this.saveChanges.bind(this)
    this.handleKeyDown = this.handleKeyDown.bind(this)

    this.state = {
      active: false,
      success: false,
      error: null,
      reset: false,
      loading: false,
      disabled: false,
      textInput: this.props.value
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.active) {  
      if (!this.props.loading) {
        let textInput = ReactDOM.findDOMNode(this.textInput)
        textInput.focus();
      }
      if (this.state.reset && !this.props.loading && !this.props.error) {
        setTimeout(() => {
          this.setState({ active: false, reset: false });
          this.props.onSuccess();
        }, 1500);
      }
    }
  }

  onComponentValueChange(e) {
    e.persist()
    this.setState({ "textInput": e.target.value })
  }

  onClick(e) {
    if (!this.props.loading) {
      this.setState({ active: true})
    }
  }

  saveChanges() {
    if (this.state.textInput !== this.props.value) {
      let value = { "observaciones": this.state.textInput} 
      this.props.onChange(this.props.idMovimiento, value, this.props.which, this.props.index)
      this.setState({ reset: true })
    } else {
      this.setState({ active: false })
    }
  }

  handleKeyDown(e) {
    if (e.keyCode == 13) {
      this.saveChanges()
    }
  }

  render() {
    let statusClassFormGroup = "";
    let statusClassFaIcon = "fa-pencil";

    if(this.state.active) {
      statusClassFormGroup = (() => {
        if (this.props.success) return "has-success"
        else if (this.props.error) return "has-error"
        else return ""
      })()
      statusClassFaIcon = (() => {
        if (this.props.success) return "fa-check"
        else if (this.props.error) return "fa-times"
        else if (this.props.loading) return "fa-refresh fa-spin"
        else return "fa-floppy-o"
      })()
    }
    return (
      <div className={`form-group ${statusClassFormGroup} ${(this.props.errorMessage ? 'has-error' : '')} col-sm-12`}>

        <div className={`input-group ${!this.state.active ? "edit-text-input-group" : ""}`}>
            {
              this.state.active
            ?
              <input
                ref={(input) => (this.textInput = input) }
                id={this.props.id}
                type="text"
                name={this.props.name}
                onChange={this.onComponentValueChange}
                className="form-control"
                value={this.state.textInput}
                onBlur={this.saveChanges}
                onKeyDown={this.handleKeyDown}
              />
            :
              <div onClick={this.onClick} className={`form-control edit-text-inline`}>
                {this.props.value }
              </div>
            }
            <span onClick={this.onClick} className={`input-group-addon ${!this.state.active ? "no-edit-addon" : ""}`}>
              <i className={`fa ${statusClassFaIcon} no-pointer-events`}></i>
            </span>
            {
              this.props.errorMessage
            ?
              <span className="help-block">{this.props.errorMessage}</span>
            :
              ""
            }
        </div>

      </div>

    );
  }

}