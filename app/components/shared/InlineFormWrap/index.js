import React from 'react';
import _ from 'lodash';

import LoadingOverlay from '../LoadingOverlay';
import SuccessOverlay from '../SuccessOverlay';

export default class InlineFormWrap extends React.Component {

  static propTypes = {
    onSubmit: React.PropTypes.func,
    loading: React.PropTypes.bool,
    errorMessage: React.PropTypes.string,
    successMessage: React.PropTypes.string,
    success: React.PropTypes.bool,
    children: React.PropTypes.array,
    disabled: React.PropTypes.bool
  }

  constructor(props, context) {
    super(props, context);
    this.state = {
      errorMessage: props.errorMessage
    };
    this.formFields= {};
    this.onFormSubmit = this.onFormSubmit.bind(this);
  }

  onFormSubmit() {
    React.Children.map(this.props.children, (child) => {});

    // colectar datos para la validación
    const missingFields = [];
    _.forOwn(this.props.children, (child, key) => {
        if (child.props.children) {
          _.forOwn(child.props.children, (child, key) => {
            if (child.props.children) {
              _.forOwn(child.props.children, (child, key) => {
                if (child && child.props && child.props.mandatory) {
                  if ((child.props.value === '') || (child.props.value === undefined)) {
                    missingFields.push({ id: child.props.id, name: child.props.name, errorMessage: "Falta completar el campo" })
                  }
                }
              })
            }
          })
        }
      }
    );

    // validar datos
    if (missingFields.length > 0) {
      let errorMessage = 'Falta completar el campo ';
      if (missingFields.length > 1) {
        errorMessage = 'Faltan completar los campos: ';
      }

      missingFields.map((missingField) => {
        errorMessage += missingField.name + ', ';
      });

      errorMessage = errorMessage.substring(0, errorMessage.length - 2);
      this.setState({ errorMessage: errorMessage });
      this.props.onSubmit(missingFields);
      return;
    }

    this.setState({ errorMessage: null });

    this.props.onSubmit();
  }

  render() {
    return (
      <div className="col-md-12">
        <div className="box box-primary form-horizontal">
          { this.props.loading ? <LoadingOverlay /> : ''}
          { this.props.success ? <SuccessOverlay /> : ''}
          <div className="box-body">
            <div>
              {this.props.children}
            </div>
          </div>
          <div className={`box-footer ${(this.props.errorMessage ? 'has-error' : '')} ${(this.props.success ? 'has-success' : '')}`}>
            {
              this.props.onSubmit
            ?
              <div>
              { 
                this.props.loading
              ?
                <button className="btn btn-primary disabled" >cargando</button>
              :
                <button className="btn btn-primary" disabled={this.props.disabled} onClick={this.onFormSubmit}>Guardar</button>
              }
              </div>
            :
              ""
            }
            { this.props.errorMessage ? <span className="help-block">{this.props.errorMessage}</span> : '' }
            { this.props.success ? <span className="help-block">{this.props.successMessage}</span> : '' }
          </div>
        </div>
      </div>
    );
  }

}
