import React from 'react';

export default class SuccessOverlay extends React.PureComponent {

  render() {
    return (
      <div className="overlay overlay-success">
        <i className="fa fa-check-circle"></i>
      </div>
    );
  }
}
