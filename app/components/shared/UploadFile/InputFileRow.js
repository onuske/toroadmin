import React, { Component } from 'react';

export default class InputFileRow extends Component {

  static propTypes = {
    onDelete: React.PropTypes.func,
    current: React.PropTypes.bool
  }

  constructor(props) {
    super(props);
    this.state = { statusClass: null }
    this.deleteFile = this.deleteFile.bind(this);
    this.getClassNames = this.getClassNames.bind(this);
  }

  deleteFile() {
    this.props.onDelete(this.props.id)
  }

  getClassNames(status) {
    switch (status) {
      case 'error':
        return { iconClass: 'fa-ban', divClass: 'has-danger', buttonClass: 'btn-danger' } // rojo
      case 'info':
        return { iconClass: 'fa-info', divClass: 'has-info', buttonClass: 'btn-info' } // celeste
      case 'warning':
        return { iconClass: 'fa-warning',divClass: 'has-warning', buttonClass: 'btn-warning' } // naranja
      case 'success': 
        return { iconClass: 'fa-check', divClass: 'has-success', buttonClass: 'btn-success disabled' } // verde
      case 'loading':  
        return { iconClass: 'fa-refresh fa-spin', divClass: '',  buttonClass: 'btn-default-toro disabled'} // verde      
      default :
        return { iconClass: '', divClass: '', buttonClass: 'btn-default-toro' } // none      
    }
  }

  render() {
    let myClass = this.getClassNames(this.props.status)

    return (

      <div className="box box-default box-solid">
        <div className="box-body" style={{"height": "50px", "padding": "8px"}} >

          <div className="col-md-10">
            <div className={`form-group input-group ${myClass.divClass}`}>
              <span className="input-group-addon">
                <i className={`icon fa ${myClass.iconClass}`}></i>
              </span>
              <button className={`btn btn-flat ${myClass.buttonClass} file-input box-toro`}>
                {this.props.children}

              </button>

            </div>
          </div>

          { 
            !this.props.current
          ?
            <div className="col-md-2">
              <div className="box-tools pull-right">
                <button type="submit" name="search" id="search-btn" className="btn btn-flat" onClick={this.deleteFile}><i className="fa fa-times" style={{"fontSize": "18px"}}></i>
                </button>
              </div>
            </div>
          :
            ""
          }
          {this.props.message}
          
        </div>

      </div>

    );
  }
}