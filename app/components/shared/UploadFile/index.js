import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { actionCreators } from 'actions/api';
import InputFile from 'components/shared/InputFile';
import InputFileRow from './InputFileRow';

@connect(
  (store) => ({
    subirArchivoState: store.get('api').subirArchivo || {},
    layout: store.get('layout') || {}
  }),
  (dispatch) => ({
    subirArchivo: bindActionCreators(actionCreators, dispatch).subirArchivo,
    subirArchivoReset: bindActionCreators(actionCreators, dispatch).subirArchivoReset
  })
)
export default class UploadFile extends Component {

  static propTypes = {
    onSuccess: React.PropTypes.func,
    archivosSubidos: React.PropTypes.array,
    onDelete: React.PropTypes.func,
    forceUpdate: React.PropTypes.string,
    shouldUpdate: React.PropTypes.bool,
    onClick: React.PropTypes.func
  }

  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.getInitialState = this.getInitialState.bind(this);
    this.deleteFile = this.deleteFile.bind(this);
    this.state = this.getInitialState();
  }

  getInitialState() {
    return  {
     success: false,
     error: null,
     reset: false,
     loading: false,
     status: null,
     helpText: "haga click en el boton para subir un archivo",
     numChildren: 1
    }
  }

  handleChange(e) {
    this.props.onClick();
    this.props.subirArchivo(e.target.files[0])
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.shouldUpdate) {
      if (nextProps.subirArchivoState.loading && !nextProps.subirArchivoState.success) {
        this.setState({ status: 'loading', helpText: 'subiendo archivo...' });
      } else if (nextProps.subirArchivoState.success && nextProps.subirArchivoState.data && nextProps.subirArchivoState.data !== this.props.subirArchivoState.data) {
        this.props.onSuccess(nextProps.subirArchivoState.data.data)
        this.setState({ reset: true, status: null, helpText: 'archivo subido' })
        this.props.subirArchivoReset();
      } else if (nextProps.subirArchivoState.error) {
        this.setState({ reset: true, status: 'success', helpText: 'hubo un error'});
      }
    }
  }

  deleteFile(uuid) {
    this.props.onDelete(uuid)
  }

  render() {
    const children = [];

    for(var i in this.props.archivosSubidos) {
      children.push(
        <InputFileRow key={this.props.archivosSubidos[i].uuid} id={this.props.archivosSubidos[i].uuid} status="success" onDelete={this.deleteFile}>
         {this.props.archivosSubidos[i].nombre}
        </InputFileRow>
      );
    }

    return (
      <div>
        {children}
        <InputFileRow current={true} status={this.state.status}>
          <InputFile
            ref="inputFile"
            name="file"
            accept=".pdf"
            placeholder= "Examinar.."
            className="input-class"
            onChange={this.handleChange}
            reset={this.state.reset}
          />
        </InputFileRow>
      </div>
    );
  }

}