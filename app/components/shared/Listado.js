import React from 'react';
import dateFormat from 'dateformat';

import LoadingOverlay from 'components/shared/LoadingOverlay';

export default class Listado extends React.Component {

  static propTypes = {
    title: React.PropTypes.string.isRequired,
    columns: React.PropTypes.array.isRequired,
    data: React.PropTypes.array,
    loading: React.PropTypes.bool.isRequired,
    error: React.PropTypes.string,
  }

  render() {
    return (
      <div>
        <div className="col-md-12">
          <div className="box">
            { this.props.loading ? (
              <div>
                <LoadingOverlay style={{bottom: 0, position: 'absolute', top: 0, left: 0, right: 0}} />
                <h3 className="">{this.props.title}</h3>
                <div className="box-body no-padding">
                  <table className="table table-condensed">
                    <tbody>
                      <tr>
                      {this.props.columns.map((item, index) => ((
                        <th key={index}>{item}</th>
                      )))}
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              ) : (
              <div>
                <div className="box-header">
                  <h3 className="box-title">{this.props.title}</h3>
                </div>
                <div className="box-body no-padding">
                  <table className="table table-condensed">
                    <tbody>
                      <tr>
                      {this.props.columns.map((item, index) => ((
                        <th key={index}>{item}</th>
                      )))}
                      </tr>
                      {this.props.data.map((item, index) => ((
                        <tr key={index}>
                          {this.props.columns.map((item2, index2) => ((
                            <td key={index2}>
                            {
                              item2 === "fecha" ?
                              dateFormat(item.fecha, "dd/mm/yyyy") :
                              item[item2]
                            }
                            </td>
                          )))}
                        </tr>
                      )))}
                    </tbody>
                  </table>
                </div>
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }

}
