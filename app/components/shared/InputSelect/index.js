import React from 'react';

export default class InputSelect extends React.Component {

  static propTypes = {
    id: React.PropTypes.string.isRequired,
    name: React.PropTypes.string.isRequired,
    onChange: React.PropTypes.func,
    mandatory: React.PropTypes.bool,
    errorMessage: React.PropTypes.string,
    value: React.PropTypes.string,
    options: React.PropTypes.array.isRequired
  }

  constructor(props, context) {
    super(props, context);
    this.state = {
      errorMessage: this.props.errorMessage
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.errorMessage) {
      this.setState({ errorMessage: nextProps.errorMessage })
    }
  }

  cleanErrorMessage() {
    this.setState({ errorMessage: '' })
  }

  render() {
    return (
      <div className={`form-group ${(this.state.errorMessage ? 'has-error' : '')}`}>
        <label htmlFor={this.props.name}>{this.props.name} {this.props.mandatory ? '*' : ''}</label>
        <select
          id={this.props.id}
          type="string"
          name={this.props.name}
          onChange={this.props.onChange}
          className="form-control"
          value={this.props.value}
        >
          {this.props.options}
        </select>
        {this.state.errorMessage
          ?
            <span className="help-block">{this.state.errorMessage}</span>
          :
           ''
         }
      </div>
    );
  }
}
