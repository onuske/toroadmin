import React from 'react';

export default class InputHidden extends React.Component {

  static propTypes = {
    id: React.PropTypes.string.isRequired,
    value: React.PropTypes.oneOfType([
      React.PropTypes.string,
      React.PropTypes.number
    ]),
    onChange: React.PropTypes.func
  }

  render() {
    return (
      <input
        id={this.props.id}
        type="hidden"
        name={this.props.id}
        value={this.props.value}
        onChange={this.props.onChange}
      />
    );
  }
}
