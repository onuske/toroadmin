import React from 'react';

export default class InlineSteps extends React.PureComponent {

  static propTypes = {
    stepsActive: React.PropTypes.number,
    stepsTitles: React.PropTypes.array
  };

  constructor(props) {
    super(props);
  }

  render() {
    const rows = this.props.stepsTitles.map((stepTitle, i) => {
      const stepNumber = i + 1;
      const estado = stepNumber <= this.props.stepsActive ? 'done' : '';
      const activo = stepNumber == this.props.stepsActive ? 'active' : '';

      return (
        <li key={i} className={`timelinepasos__step ${estado} ${activo}`}>
          <input className="timelinepasos__step-radio" type="radio" />
          <span className="timelinepasos__step-title">{stepTitle}</span>
          <i className="timelinepasos__step-marker">{stepNumber}</i>
        </li>
      );
    })

    return (
      <div>
        <div className="contiene_timeline">
          <ol className={`timelinepasos pasos_${this.props.stepsTitles.length}`}>
            {rows}
          </ol>
        </div>
      </div>
    );
  }
}
