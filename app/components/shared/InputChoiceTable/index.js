import React from 'react';

export default class InputChoice extends React.Component {

  static propTypes = {
    id: React.PropTypes.string.isRequired,
    name: React.PropTypes.string.isRequired,
    onChange: React.PropTypes.func,
    mandatory: React.PropTypes.bool,
    errorMessage: React.PropTypes.string,
    choices: React.PropTypes.array.isRequired
  }

  constructor(props, context) {
    super(props, context);
    this.state = {
      errorMessage: this.props.errorMessage
    };

    this.localOnChange = this.localOnChange.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.errorMessage) {
      this.setState({ errorMessage: nextProps.errorMessage })
    }
  }

  localOnChange(event) {
    this.value = event.target.value;

    this.setState({ value: event.target.value })

    if (this.props.onChange != null) {
      this.props.onChange({ target: { value: event.target.value, id: this.props.id } });
    }
  }

  cleanErrorMessage() {
    this.setState({ errorMessage: '' })
  }

  render() {
    const choices = this.props.choices.map((item, index) => {
      const iStyle = this.state.value === item.value ? { color: item.color } : {};
      return (
        <label className="radio-inline text-center input-choice-table-cell" htmlFor={`${this.props.id}_${index}`} key={index}>
          <input type="radio" className="input-choice" name={this.props.name} id={`${this.props.id}_${index}`} value={item.value} onChange={this.localOnChange} />

          <i className={`icon-${item.icon}`}></i>
          <br />
          {item.value}
        </label>
      )
    });

    return (
      <div className={`form-group ${(this.state.errorMessage ? 'has-error' : '')} col-sm-12`}>
        <label htmlFor={this.props.name}>{this.props.name} {this.props.mandatory ? '*' : ''}</label>
        <br />

        <div className="input-choice-table">
          {choices}
        </div>

        {this.state.errorMessage
          ?
            <span className="help-block">{this.state.errorMessage}</span>
          :
           ''
         }
      </div>
    );
  }
}
