import React from 'react';

export default class InputText extends React.Component {

  static propTypes = {
    id: React.PropTypes.string.isRequired,
    name: React.PropTypes.string.isRequired,
    onChange: React.PropTypes.func,
    mandatory: React.PropTypes.bool,
    errorMessage: React.PropTypes.string,
    value: React.PropTypes.string,
    inputStyle: React.PropTypes.string,
    widthLabel: React.PropTypes.string,
    width: React.PropTypes.string
  }

  render() {
    let widthLabel = 2, widthInput = 10, width = 12; // col-sm-12
    if (this.props.inputStyle === 'inline') {
      if (this.props.widthLabel) widthLabel = this.props.widthLabel;
      widthInput = 12 - widthLabel;
    }
    if (this.props.width) width = this.props.width;

    return (
      <div className={`form-group ${(this.props.errorMessage ? 'has-error' : '')} col-sm-${width}`}>
        <label className={this.props.inputStyle === 'inline' ? `col-sm-${widthLabel} control-label` : 'control-label'} htmlFor={this.props.id}>{this.props.name} {this.props.mandatory ? '*' : ''}</label>
        {
          this.props.inputStyle === 'inline'
        ?
          <div className={`${this.props.inputStyle === 'inline' ? 'col-sm-'+widthInput : ''}`}>
            <input
              id={this.props.id}
              type="text"
              name={this.props.name}
              onChange={this.props.onChange}
              className="form-control"
              value={this.props.value}
            />
            {
              this.props.errorMessage
            ?
              <span className="help-block">{this.props.errorMessage}</span>
            :
              ""
            }
          </div>
        :
          <div>
          
            <input
              id={this.props.id}
              type="text"
              name={this.props.name}
              onChange={this.props.onChange}
              className="form-control"
              value={this.props.value}
            />
          
            {
              this.props.errorMessage
            ?
              <span className="help-block">{this.props.errorMessage}</span>
            :
              ""
            }
          </div>
        }
      </div>
    );
  }
}
