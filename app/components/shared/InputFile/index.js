import React from 'react';

export default class InputFile extends React.Component {

  static propTypes = {
    onChange: React.PropTypes.func,
    reset: React.PropTypes.bool,
    disabled: React.PropTypes.bool
  }

  constructor(props, context) {
    super(props, context);
    this.state = this.getInitialState();

    this.getInitialState = this.getInitialState.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.reset) {
      this.setState(() => this.getInitialState());

      if (this.refs.inputFile.value) {
        this.refs.inputFile.value = "";
      }
    }
  }

  getInitialState() {
    return {
      value: '',
      styles: {
        parent: {
          position: 'relative'
        },
        file: {
          position: 'absolute',
          top: 0,
          left: 0,
          opacity: 0,
          width: '100%',
          zIndex: 1
        },
        text: {
          position: 'relative',
          zIndex: -1
        }
      }
    };
  }

  handleChange(e) {
    this.setState({
      value: e.target.value.split(/(\\|\/)/g).pop()
    });
    if (this.props.onChange) this.props.onChange(e);
  }

  render() {
    return (

      <div style={this.state.styles.parent}>

        <input
          type='file'
          name={this.props.name}
          className={this.props.className}
          onChange={this.handleChange}
          disabled={this.props.disabled}
          accept={this.props.accept}
          style={this.state.styles.file}
          ref="inputFile"
          disabled={this.props.disabled}
        />

        <input
          type='text'
          tabIndex='-1'
          name={this.props.name + '_filename'}
          value={this.state.value}
          className={this.props.className}
          placeholder={this.props.placeholder}
          disabled={this.props.disabled}
          style={this.state.styles.text}
        />

      </div>
    );
  }
}