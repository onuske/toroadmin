import React from 'react';

import InlineSteps from '../InlineSteps';

export default class SteppedProcess extends React.Component {

  static propTypes = {
    currentStep: React.PropTypes.number.isRequired,
    children: React.PropTypes.node.isRequired
  }

  constructor(props) {
    super(props);
    this.state = {
      stepsTitles: [],
      currentStep: props.currentStep
    };
  }

  componentWillMount() {
    this.setState({
      stepsTitles: this.props.children.map((child) => {
        return child.props.stepTitle;
      })
    })
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.currentStep) {
      this.setState({ currentStep: nextProps.currentStep });
    }
  }

  render() {
    const activeStep = []
    // enable the onle needed only
    this.props.children.forEach((item, index) => {
      if (index === this.state.currentStep - 1) {
        // pass down the header property
        activeStep.push(React.cloneElement(item, {
          header: (
            <InlineSteps
              stepsTitles={this.state.stepsTitles}
              stepsActive={this.state.currentStep}
            />
          )
        }));
      }
    })

    return (<div>{activeStep}</div>);
  }

}
