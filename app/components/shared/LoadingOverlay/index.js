import React from 'react';

export default class LoadingOverlay extends React.PureComponent {

  render() {
    return (
      
      <div className="overlay" style={this.props.style ? this.props.style : {}}>
        <i className="fa fa-refresh fa-spin"></i>
      </div>
    );
  }
}
