import React from 'react';
import CKEditor from "react-ckeditor-component";

export default class inputCKEditor extends React.Component {

  static propTypes = {
    id: React.PropTypes.string.isRequired,
    name: React.PropTypes.string.isRequired,
    onChange: React.PropTypes.func,
    mandatory: React.PropTypes.bool,
    errorMessage: React.PropTypes.string,
    value: React.PropTypes.any,
    config: React.PropTypes.object,
    activeClass: React.PropTypes.string,
    inputStyle: React.PropTypes.string,
    widthLabel: React.PropTypes.string,
    width: React.PropTypes.string,
    reset: React.PropTypes.bool
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.reset && this.inputCKEditor && this.inputCKEditor.state.content) {
      this.didReset = false;
      this.inputCKEditor.editorInstance.setData("");
    }
  }

  render() {

    let widthLabel = 2, widthInput = 10, width = 12; // col-sm-12
    if (this.props.inputStyle === 'inline') {
      if (this.props.widthLabel) widthLabel = this.props.widthLabel;
      widthInput = 12 - widthLabel;
    }
    if (this.props.width) width = this.props.width;

    return (
      <div className={`form-group ${(this.props.errorMessage ? 'has-error' : '')} col-sm-${width}`}>
        <label className={this.props.inputStyle === 'inline' ? `col-sm-${widthLabel} control-label` : 'control-label'} htmlFor={this.props.id}>{this.props.name} {this.props.mandatory ? '*' : ''}</label>
        {
          this.props.inputStyle === 'inline'
        ?
          <div className={`${this.props.inputStyle === 'inline' ? 'col-sm-'+widthInput : ''}`}>
            <CKEditor
              id={this.props.id}
              config={this.props.config}
              activeClass={this.props.activeClass}
              content={this.props.value}
              onChange={this.props.onChange}
              ref={(CKEditor) => { this.inputCKEditor = CKEditor; }}
            />
          </div>
        :
          <CKEditor
            id={this.props.id}
            config={this.props.config}
            activeClass={this.props.activeClass}
            content={this.props.value}
            onChange={this.props.onChange}
            ref={(CKEditor) => { this.inputCKEditor = CKEditor; }}
          />
        }
        {this.props.errorMessage
          ?
            <span className="help-block">{this.props.errorMessage}</span>
          :
            ""
         }
      </div>
    );
  }
}
