import React from 'react'
import PlacesAutocomplete, { geocodeByAddress } from './PlacesAutocomplete'

export default class InputLocation extends React.Component {

  static propTypes = {
    id: React.PropTypes.string.isRequired,
    name: React.PropTypes.string.isRequired,
    onChange: React.PropTypes.func,
    onSelect: React.PropTypes.func,
    mandatory: React.PropTypes.bool,
    mandatoryFields: React.PropTypes.array,
    errorMessage: React.PropTypes.string,
    value: React.PropTypes.object
  }

  constructor(props, context) {
    super(props, context);
    this.state = {
      address: '',
      errorMessage: this.props.errorMessage,
      currentState: 'empty'
    }
    this.handleSelect = this.handleSelect.bind(this)
    this.handleChange = this.handleChange.bind(this)
    this.parseResult = this.parseResult.bind(this)
    this.checkMandatoryFields = this.checkMandatoryFields.bind(this)
  }

  handleSelect(address) {
    this.setState({"address": address})

    geocodeByAddress(address, (err, results) => {
      if (!err) {
        var direccion = this.parseResult(results[0]);
        if(this.checkMandatoryFields(direccion)){
          this.setState({"direccion": direccion, "currentState": 'success'});
          if (this.props.onSelect != null) {
            this.props.onSelect(direccion);
          }
        }
        else{
          this.setState({"currentState": 'danger'});
          if (this.props.onSelect != null) {
            this.props.onSelect(undefined);
          }
        }
      }
    })
  }

  parseResult(result){
    var direccion = {};
    if(result != null) {
      result.address_components.map((item, idx) => {
        switch(item.types[0]) {
          case 'street_number' : direccion.numero = item.long_name;break;
          case 'route' : direccion.calle = item.long_name;break;
          case 'locality' : direccion.localidad = item.long_name;break;
          case 'administrative_area_level_2' : direccion.partido = item.long_name;break;
          case 'administrative_area_level_1' : direccion.provincia = item.long_name;break;
          case 'country' : direccion.pais = item.long_name;break;
          case 'postal_code' : direccion.codigoPostal = item.long_name;break;
        }
      });
      if((direccion.localidad === "")||(direccion.localidad === undefined)){
        result.address_components.map((item, idx) => {
          switch(item.types[1]) {
            case 'sublocality' : direccion.localidad = item.long_name;break;
          }
        });
      }
      direccion.latitud = result.geometry.location.lat();
      direccion.longitud = result.geometry.location.lng();
      direccion.raw = result.formatted_address;
    }
    return direccion;
  }

  checkMandatoryFields(direccion){
    for(var i=0;i<this.props.mandatoryFields.length;i++){
      if(direccion[this.props.mandatoryFields[i]]==undefined)
        return false;
    }
    return true;
  }

  handleChange(address) {
    this.setState({"address": address});
    this.setState({"currentState": 'empty'});
    if (this.props.onChange != null) {
      this.props.onChange(address);
    }
    if (this.props.onSelect != null) {
      this.props.onSelect(undefined);
    }
  }

  render() {
    const cssClasses = {
      root: 'form-group',
      input: 'form-control',
      autocompleteContainer: 'Demo__autocomplete-container'
    }

    const options = {
      location: new google.maps.LatLng(-34.6, -58.4), // eslint-disable-line no-undef
      radius: 2000,
      types: ['address']
    }

    const AutocompleteItem = ({ formattedSuggestion }) => (
      <div className="Demo__suggestion-item">
        <i className="fa fa-map-marker fa-map-marker-empty"/>
        <strong>  {formattedSuggestion.mainText}</strong>{' '}
        <small className="text-muted">{formattedSuggestion.secondaryText}</small>
      </div>
    );

    return (
      <div className={`form-group ${(this.state.errorMessage ? 'has-error' : '')} col-sm-12`}>
        <label htmlFor={this.props.name}>{this.props.name} {this.props.mandatory ? '*' : ''}</label>
        <PlacesAutocomplete
          id={this.props.id}
          name={this.props.name}
          value={this.state.address}
          onChange={this.handleChange}
          onSelect={this.handleSelect}
          classNames={cssClasses}
          autocompleteItem={AutocompleteItem}
          autoFocus={true}
          placeholder="calle - numero - localidad"
          hideLabel={true}
          options={options}
          currentState={this.state.currentState}
        />
        {this.state.errorMessage
          ?
            <span className="help-block">{this.state.errorMessage}</span>
          :
           ''
         }
      </div>
    );
  }
}
