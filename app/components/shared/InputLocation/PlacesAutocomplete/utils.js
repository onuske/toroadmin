export const geocodeByAddress = (address, callback) => {
  const geocoder = new google.maps.Geocoder() // eslint-disable-line no-undef
  const OK = google.maps.GeocoderStatus.OK // eslint-disable-line no-undef

  geocoder.geocode({ address }, (results, status) => {
    if (status !== OK) {
      callback({ status }, results)
      return
    }

    callback(null, results)
  })
}

export const geocodeByPlaceId = (placeId, callback) => {
  const geocoder = new google.maps.Geocoder() // eslint-disable-line no-undef
  const OK = google.maps.GeocoderStatus.OK // eslint-disable-line no-undef

  geocoder.geocode({ placeId }, (results, status) => {
    if (status !== OK) {
      callback({ status }, null)
      return
    }

    callback(null, results)
  })
}
