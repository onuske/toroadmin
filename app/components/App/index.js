import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { actionCreators } from 'actions/auth';

@connect(
  (store) => ({}),
  (dispatch) => ({
    checkLogin: bindActionCreators(actionCreators, dispatch).checkLogin
  })
)
export default class App extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  static propTypes = {
    children: React.PropTypes.node,
  };
  
  componentDidMount() {
    this.props.checkLogin()
  }

  render() {
    return (
      <div>
        {React.Children.toArray(this.props.children)}
      </div>
    );
  }
}
