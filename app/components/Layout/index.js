import React from 'react';

import Header from './Header'
import Body from './Body'
import Footer from './Footer'

export default class Layout extends React.PureComponent {

  static propTypes = {
    children: React.PropTypes.node,
    classBody: React.PropTypes.string
  }

  render() {
    return (
      <div>
        <Header />
        <Body classBody={this.props.classBody}>
          {this.props.children}
        </Body>
      </div>
    );
  }
}
