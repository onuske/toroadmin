import React from 'react';

export default class Footer extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <footer className="main-footer">
        <div className="pull-right hidden-xs">

        </div>
        <strong><b>Versión</b> Alpha 0.01</strong>

      </footer>
    );
  }
}
