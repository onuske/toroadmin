import React from 'react';

export default class Body extends React.PureComponent {

  static propTypes = {
    children: React.PropTypes.node,
    classBody: React.PropTypes.string
  };

  render() {
    return (
      <div className={`content-wrapper ${this.props.classBody ? this.props.classBody : '' }`}>
          {React.Children.toArray(this.props.children)}
      </div>
    );
  }
}
