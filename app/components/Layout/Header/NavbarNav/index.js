import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';

import userImg from './avatar.png'; // eslint-disable-line no-unused-vars

import { logout } from 'actions/auth';

@connect(
  (store) => ({
    layout: store.get('layout') || {usuario: {}}
  }),
  (dispatch) => ({
    logout: () => (dispatch(logout()))
  })
)
export default class NavbarNav extends React.Component {

  static propTypes = {
    
  };

  constructor(props) {
    super(props);

    this.onOpenUserDropdown = this.onOpenUserDropdown.bind(this);
  }

  onOpenUserDropdown(event) {
    event.preventDefault();
    event.currentTarget.classList.toggle('open');
  }

  render() {
    return(
      <div>
        <div className="navbar-custom-menu">
          <ul className="nav navbar-nav">
            <li className="dropdown user user-menu" onClick={this.onOpenUserDropdown}>
              <Link to="" className="dropdown-toggle" data-toggle="dropdown">
                <span className="hidden-xs">{this.props.layout.usuario ? `${this.props.layout.usuario.nombre} ${this.props.layout.usuario.apellido}` : ''}</span>
                <img src={userImg} className="user-image" alt="Profile" />
              </Link>
              <ul className="dropdown-menu">
                <li className="user-header">
                  <img src={userImg} className="img-circle" alt="User" />
                  <p>
                    {this.props.layout.usuario ? `${this.props.layout.usuario.nombre} ${this.props.layout.usuario.apellido}` : ''}
                    <small>administrador</small>
                  </p>
                </li>
                <li className="user-body">
                  <div className="row">
                    <div className="col-xs-4 text-center">
                      <Link to="#">Consorcios<br/>0</Link>
                    </div>
                    <div className="col-xs-4 text-center">
                      <Link to="#">Empleados<br/>0</Link>
                    </div>
                    <div className="col-xs-4 text-center">
                      <Link to="#">Proveedores<br/>0</Link>
                    </div>
                  </div>
                </li>
                <li className="user-footer">
                  <div className="pull-left">
                    <Link to="/" className="btn btn-default btn-flat">Gestion General</Link>
                  </div>
                  <div className="pull-right">
                    <a onClick={this.props.logout} className="btn btn-default btn-flat"><i className="fa fa-sign-out" aria-hidden="true"></i> Salir</a>
                  </div>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    );
  }
}
