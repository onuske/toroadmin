import React from 'react';
import { Link } from 'react-router';
import ToroBrand from 'components/shared/ToroBrand'

export default class NavbarBrand extends React.PureComponent {

  static propTypes = {}

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Link to="/" className="navbar-brand" style={{"padding":"0"}}>
        <ToroBrand/>
      </Link>
    );
  }
}
