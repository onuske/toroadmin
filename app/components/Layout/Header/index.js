import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link, withRouter } from 'react-router';

import NavbarBrand from './NavbarBrand';
import NavbarNav from './NavbarNav';

import { actionCreators } from 'actions/api';
import { actionCreators as layoutActions } from 'actions/layout';


@connect(
  (store) => ({
    email: store.get('auth').email,
    role: store.get('auth').role,
    layout: store.get('layout')
  }),
  (dispatch) => ({
    listarConsorcios: bindActionCreators(actionCreators, dispatch).listarConsorcios,
    unselectConsorcio: bindActionCreators(layoutActions, dispatch).unselectConsorcio,
  })
)

class Header extends React.PureComponent {

  static propTypes = {
    email: React.PropTypes.string,
    role: React.PropTypes.string
  }

  constructor(...args) {
    super(...args);
    this.state = {};
  }

  render() {
    return (
      <div>
      <header className="main-header">
        <nav className="navbar navbar-static-top">

            <NavbarBrand />
            <NavbarNav />
          
        </nav>
      </header>

        {/*<header className="main-header">
          <a href="" className="logo" onClick={this.unselectConsorcio}>
            <span className="logo-mini"><b>A</b>C</span>
            <span className="logo-lg"><b>ADM</b>CONSORCIO</span>
          </Link>
          <nav className="navbar navbar-static-top">
            <ToggleButton />
            <div className="navbar-custom-menu">
              <ul className="nav navbar-nav">
                <li className="dropdown user user-menu" onClick={this.handleUser}>
                  <a href="" className="dropdown-toggle" data-toggle="dropdown">
                    <img src={userImg} className="user-image" alt="Profile" />
                    <span className="hidden-xs">{ this.props.email }</span>
                  </Link>
                  <ul className="dropdown-menu">
                    <li className="user-header">
                      <img src={userImg} className="img-circle" alt="User" />
                      <p>
                        Juan Ravazzola
                        <small>{ this.props.role }</small>
                      </p>
                    </li>
                    <li className="user-footer">
                      <div className="pull-left">
                        <a href="" className="btn btn-default btn-flat"><i className="fa fa-user" aria-hidden="true"></i> Perfil</Link>
                      </div>
                      <div className="pull-right">
                        <a href="" className="btn btn-default btn-flat"><i className="fa fa-sign-out" aria-hidden="true"></i> Cerrar Sesión</Link>
                      </div>
                    </li>
                  </ul>
                </li>
                <li className="dropdown consorcios consorcio-menu" onClick={this.handleConsorcios}>
                  <a href="" className="dropdown-toggle" data-toggle="dropdown">
                    <span className="hidden-xs">Consorcios</span> <i className="fa fa-angle-down" aria-hidden="true"></i>
                  </Link>
                  <ul className="dropdown-menu">
                    <li className="consorcio-header">
                      <a href="" onClick={this.unselectConsorcio}>Gestion General</Link>
                    </li>
                    <li className="consorcio-header">
                      <ul className="consorcio-list">
                        {this.props.layout.consorcios ? this.props.layout.consorcios.map((consorcio, index) => {
                          return (<li key={index}><Link to={`/${consorcio.nombreUrl}`}>{consorcio.nombre}</Link></li>);
                        })
                        : '...cargando'}
                      </ul>
                    </li>
                    <li className="consorcio-footer">
                      <div className="pull-left">
                        <a href="" className="btn btn-default btn-flat"><i className="fa fa-th-list" aria-hidden="true"></i> Ver Todos</Link>
                      </div>
                      <div className="pull-right">
                        <a href="" className="btn btn-default btn-flat"><i className="fa fa-plus-circle" aria-hidden="true"></i> Nuevo Consorcio</Link>
                      </div>
                    </li>
                  </ul>
                </li>
                <li className="SearchTop">
                  <form action="" method="get" className="sidebar-form">
                    <div className="input-group">
                      <input type="text" name="q" className="form-control" placeholder="Buscar..." />
                      <span className="input-group-btn">
                        <button type="submit" name="search" id="search-btn" className="btn btn-flat">
                          <i className="fa fa-search"></i>
                        </button>
                      </span>
                    </div>
                  </form>
                </li>
              </ul>
            </div>
          </nav>
        </header>*/}
      </div>
    );
  }
}

export default withRouter(Header)
