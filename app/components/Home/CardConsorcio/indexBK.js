import React from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';

export default class CardConsorcio extends React.PureComponent {

  static propTypes = {
    consorcio: React.PropTypes.object.isRequired
  };

  constructor(props) {
    super(props);
  }

  render() {


      return (


        <div>
                <div className="col-xs-12"  style={{display: 'none'}}>
                <Link to={this.props.consorcio.nombreUrl} className="ListConsorcio">

                  <div className="info-box">
                      <div className="info-box-content-blue name">
                      <div className="info-box-icon">


                      <span className={"icon-" + this.props.consorcio.tipoConsorcio} ></span>


                      </div>
                                    </div>

                        <div className="info-box-content info-box-content-blue">
                        <h5 className="widget-user-username hideOverflow">{this.props.consorcio.nombre}</h5>
                        </div>
                        <div className="info-box-content white">
                        <h5 className="widget-user-desc hideOverflow">{this.props.consorcio.direccionCalle + " " + this.props.consorcio.direccionNumero + " " + this.props.consorcio.localidad }</h5>
                        </div>
                        <div className="info-box-content-white">
                        <div className="info-box-content  alertas">




                        <ul className="nav nav-stacked sub">
                          <li>Banco <span className={this.props.consorcio.saldo.banco>=0?"pull-right badge bg-green":"pull-right badge bg-red"}>{this.props.consorcio.saldo.bancoTexto}</span></li>
                          <li>Caja <span className={this.props.consorcio.saldo.caja>=0?"pull-right badge bg-green":"pull-right badge bg-red"}>{this.props.consorcio.saldo.cajaTexto}</span></li>

                        </ul>

                        </div>



                            <div className="info-box-content white alertas">
          <ul className="nav alerta-saldo">
                            <li>Saldo <span className={this.props.consorcio.saldo.total>=0?"pull-right badge bg-green":"pull-right badge bg-red"}>{this.props.consorcio.saldo.totalTexto}</span></li>
                          </ul>



                        </div>
                        <div className="info-box-content white alertas">
                        <ul className="nav alerta-saldo">
                            <li>Alertas <span className={this.props.consorcio.alertas==0?"pull-right badge bg-green":"pull-right badge bg-red"}>{this.props.consorcio.alertas}</span></li>
          </ul>


                            </div>
        <div className="info-box-content info-box-content-blue ingresar">
        <span className="small-box-footer">
        Ingresar <i className="fa fa-arrow-circle-right"></i>
        </span>
        </div>


                    </div>
        </div>



                  </Link>
                  </div>


                <div className="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                <Link to={this.props.consorcio.nombreUrl} className="CardConsorcio">

                  <div className="info-box">
                      <div className="info-box-icon">


                      <span className={"icon-" + this.props.consorcio.tipoConsorcio} ></span>


                      </div>

                        <div className="info-box-content">
                        <h5 className="widget-user-username hideOverflow">{this.props.consorcio.nombre}</h5>
                        <small className="widget-user-desc hideOverflow">{this.props.consorcio.direccionCalle + " " + this.props.consorcio.direccionNumero + " " + this.props.consorcio.localidad }</small>


                        </div>

                    </div>

                    <div className="box">
                    <div className="box-footer no-padding">
                      <ul className="nav nav-stacked alerta-saldo">
                        <li>Alertas <span className={this.props.consorcio.alertas==0?"pull-right badge bg-green":"pull-right badge bg-red"}>{this.props.consorcio.alertas}</span></li>
                        <li>Saldo <span className={this.props.consorcio.saldo.total>=0?"pull-right badge bg-green":"pull-right badge bg-red"}>{this.props.consorcio.saldo.totalTexto}</span></li>
                      </ul>
                      <ul className="nav nav-stacked sub">
                        <li>Banco <span className={this.props.consorcio.saldo.banco>=0?"pull-right badge bg-green":"pull-right badge bg-red"}>{this.props.consorcio.saldo.bancoTexto}</span></li>
                        <li>Caja <span className={this.props.consorcio.saldo.caja>=0?"pull-right badge bg-green":"pull-right badge bg-red"}>{this.props.consorcio.saldo.cajaTexto}</span></li>

                      </ul>
                    </div>
                    </div>

                    <span className="small-box-footer">
                      Ingresar <i className="fa fa-arrow-circle-right"></i>
                    </span>


                  </Link>
                  </div>

        </div>


      );

  }
}
