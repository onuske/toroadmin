import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import _ from 'lodash';
import { actionCreators } from 'actions/api';

import Layout from '../Layout'
import CardConsorcio from './CardConsorcio';
import NuevoConsorcio from './NuevoConsorcio';

@connect(
  (store) => ({
    listarConsorciosState: store.get('api').listarConsorcios || {}
  }),
  (dispatch) => ({
    listarConsorcios: bindActionCreators(actionCreators, dispatch).listarConsorcios,
  })
)

export default class PanelInico extends React.PureComponent {

  static propTypes = {
    listarConsorcios: React.PropTypes.func
  }

  constructor(props) {
    super(props);

    this.state = {
      "consorcios": props.listarConsorciosState.data || []
    };

  }

  componentWillMount() {
    this.props.listarConsorcios();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.listarConsorciosState.loading == false && nextProps.listarConsorciosState.success == true) {
      this.setState({"consorcios": nextProps.listarConsorciosState.data})
    }
  }
  render() {
    return (
      <Layout>

      <div className="container">
      <div className="row">
            <section className="content-header">
              <div className="col-sm-6 col-xs-12">
              <h1>
                Mis Consorcios
              </h1>
              </div>
            </section>

            </div>
            </div>

            <section  className="content">
            <div className="container">
            <div className="row">
              {
                this.state.consorcios.map((consorcio, index) => {
                  return (<CardConsorcio key={consorcio.idConsorcio} consorcio={consorcio} />)
                })
              }
              <NuevoConsorcio/>
              </div>
              </div>
            </section>
      </Layout>
    );
  }
}
