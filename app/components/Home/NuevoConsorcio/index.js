import React from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';

export default class NuevoConsorcio extends React.PureComponent {


  render() {

      return (
      <div className="col-xs-12 col-sm-12  col-md-4 col-lg-3">
        <Link to="/crearconsorcio" className="CardNuevoConsorcio">
        <div className="info-box">
            <div className="info-box-icon">
                <i className="fa fa-6 fa-plus" aria-hidden="true"></i>

              <h5 className="widget-user-username hideOverflow">Crear Nuevo Consorcio</h5>


              </div>

          </div>
        </Link>
        </div>
      );

  }
}
