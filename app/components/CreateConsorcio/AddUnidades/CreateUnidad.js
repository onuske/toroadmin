import React from 'react';

import InlineFormWrap from 'components/shared/InlineFormWrap';
import InputText from 'components/shared/InputText';
import InputMoney from 'components/shared/InputMoney';
import InputHidden from 'components/shared/InputHidden';
import InputReactSelect from 'components/shared/InputReactSelect';

export default class CreateUnidad extends React.Component {

  static propTypes = {
    onSubmit: React.PropTypes.func,
    loading: React.PropTypes.bool,
    success: React.PropTypes.bool,
    onSuccess: React.PropTypes.func
  }

  constructor(props) {
    super(props);

    // set initial state
    this.state = {};
    this.onSubmit = this.onSubmit.bind(this);
    this.onComponentValueChange = this.onComponentValueChange.bind(this);
    this.setItemCombo = this.setItemCombo.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.success) {
      var inputValues = this.state;
      for (var i in inputValues) {
        inputValues[i] = '';
      };
      this.setState(inputValues);
    }
  }

  onSubmit(missingFields) {
    if (missingFields) {
      var val = [];
      missingFields.map((value) => {
        val[`${value.id}Error`] = value.errorMessage;
      });
      this.setState(val);
    } else {
      // tomar todos los valores de los campos
      let params = {
        unidad: this.state.unidad,
        descripcion: this.state.descripcion,
        coeficienteArancel: this.state.coeficienteArancel,
        saldoInicial: this.state.saldoInicial,
        nombre: this.state.nombre,
        tipoResponsable: this.state.tipoResponsable,
        telefono: this.state.telefono,
        email: this.state.email
      }
      this.props.onSubmit(params);
    }
  }

  onComponentValueChange(event) {
    let val = {};
    // capture the input value
    val[event.target.id] = event.target.value;
    // remove input error if any
    val[`${event.target.id}Error`] = '';
    this.setState(val);
  }

  setItemCombo(index, value) {
    var val = [];
    val[index] = value;
    val[`${index}Error`] = '';
    this.setState(val);
  }

  render() {
    return (
      <InlineFormWrap
        onSubmit={this.onSubmit}
        loading={this.props.loading}
        errorMessage={this.props.error}
        successMessage="success"
        success={this.props.success}
        formStyle="inline"
        disabled={this.props.loading}
      >

        <div className="box box-primary form-unidades h-center">
          <div className="box-header">
            <h3 className="box-title">Unidad</h3>
          </div>
          <div className="box-body">
            <InputText id="unidad" name="Unidad" mandatory={true} value={this.state.unidad} onChange={this.onComponentValueChange} errorMessage={this.state.unidadError} inputStyle="inline" widthLabel="3"/>
            <InputText id="descripcion" name="Descripcion" mandatory={false} value={this.state.descripcion} onChange={this.onComponentValueChange} inputStyle="inline" widthLabel="3"/>
            <InputText id="coeficienteArancel" name="Coeficiente arancel (% de m²)" mandatory={true} value={this.state.coeficienteArancel} onChange={this.onComponentValueChange} errorMessage={this.state.coeficienteArancelError} inputStyle="inline" widthLabel="3"/>
            <InputMoney id="saldoInicial" name="Saldo inicial" mandatory={true} value={this.state.saldoInicial} onChange={this.onComponentValueChange} errorMessage={this.state.saldoInicialError} inputStyle="inline" widthLabel="3"/>
        
          </div>
        </div>

        <div className="box box-primary form-unidades h-center">
          <div className="box-header">
            <h3 className="box-title">Responsable</h3>
          </div>
          <div className="box-body">
            <InputText id="nombre" name="Nombre" mandatory={true} value={this.state.nombre} onChange={this.onComponentValueChange} errorMessage={this.state.nombreError} inputStyle="inline" widthLabel="3" />
            
            <InputReactSelect
              id="tipoResponsable"
              name="Tipo responsable"
              mandatory={true}
              onChange={this.setItemCombo}
              value={this.state.tipoResponsable}
              options={[{ label: "propietario", value: "propietario" }, { label: "inquilino", value: "inquilino"}, { label: "administrador", value: "administrador" }]}
              errorMessage={this.state.tipoResponsableError}
              inputStyle="inline"
              widthLabel="3"
            />

            <InputText id="telefono" name="Teléfono" value={this.state.telefono} onChange={this.onComponentValueChange} errorMessage={this.state.telefonoError} inputStyle="inline" widthLabel="3" />
            <InputText id="email" name="Email" value={this.state.email} onChange={this.onComponentValueChange} errorMessage={this.state.emailError} inputStyle="inline" widthLabel="3" />
          </div>
        </div>

      </InlineFormWrap>
    );
  }

}
