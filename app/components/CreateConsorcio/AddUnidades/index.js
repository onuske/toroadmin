import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import _ from 'lodash';

import { actionCreators } from 'actions/api';

import SimpleFormWrap from 'components/shared/SimpleFormWrap';
import InputText from 'components/shared/InputText';
import InputHidden from 'components/shared/InputHidden';
import ConsorcioDetail from 'components/shared/ConsorcioDetail';
import CreateUnidad from './CreateUnidad';

@connect(
  (store) => ({
    crearUnidadState: store.get('api').crearUnidad || {},
    crearResponsableState: store.get('api').crearResponsable || {}
  }),
  (dispatch) => ({
    crearUnidad: bindActionCreators(actionCreators, dispatch).crearUnidad,
    crearUnidadReset: bindActionCreators(actionCreators, dispatch).crearUnidadReset,
    crearResponsable: bindActionCreators(actionCreators, dispatch).crearResponsable,
    crearResponsableReset: bindActionCreators(actionCreators, dispatch).crearResponsableReset
  })
)
export default class AddUnidades extends React.Component {

  static propTypes = {
    idConsorcio: React.PropTypes.number.isRequired,
    onNext: React.PropTypes.func,
    crearUnidad: React.PropTypes.func.isRequired,
    crearUnidadState: React.PropTypes.object,
    crearResponsable: React.PropTypes.func.isRequired,
    crearResponsableState: React.PropTypes.object
  }

  constructor(props) {
    super(props);
    
    // set initial state
    this.state = { currentSubmit: {}, status: 'none', message: '' };

    this.onSubmit = this.onSubmit.bind(this);
    this.onNext = this.onNext.bind(this);
    this.crearResponsable = this.crearResponsable.bind(this);

  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.crearUnidadState.loading) {
      this.setState({ status: 'loading', message: 'creando unidad...' });

    } else if (nextProps.crearUnidadState.success) {
      let val = {};
      val["idUnidad"] = nextProps.crearUnidadState.data.idUnidad;
      this.setState({ "currentSubmit": _.extend( this.state.currentSubmit, val), status: 'loading', message: 'unidad creada' });

      this.crearResponsable(nextProps.crearUnidadState.data.idUnidad)
      this.props.crearUnidadReset();
    }

    if (nextProps.crearResponsableState.loading) {
      this.setState({ status: 'loading', message: 'creando responsable' });
    } else if (nextProps.crearResponsableState.success) {
      this.setState({ status: 'success', message: 'unidad creada' });
      this.props.crearResponsableReset();
      setTimeout(() => {
        this.setState({ currentSubmit: {}, status: 'none', message: '' });
      }, 500)
    }
  }

  onSubmit(params) {
    this.setState({ currentSubmit: params })

    // tomar todos los valores de los campos  
    this.props.crearUnidad({
      idConsorcio: this.props.idConsorcio,
      unidad: params.unidad,
      descripcion: params.descripcion,
      coeficienteArancel: params.coeficienteArancel,
      saldoInicial: params.saldoInicial
    });
  }

  crearResponsable(idUnidad) {

    this.props.crearResponsable({
      idUnidad: idUnidad,
      nombre: this.state.currentSubmit.nombre,
      tipoResponsable: this.state.currentSubmit.tipoResponsable,
      telefono: this.state.currentSubmit.telefono,
      email: this.state.currentSubmit.email
    });    
  }

  resetFormError() {
    this.setState({ errorMessage: null });
  }

  onNext() {
    this.props.onNext({idConsorcio: this.props.idConsorcio});
  }

  render() {
    return (
      <div>
        <ConsorcioDetail
          idConsorcio={this.props.idConsorcio}
        />

        <SimpleFormWrap
          titulo="Nueva unidad"
          onNext={this.onNext}
        >
          <InputHidden id="idConsorcio" name="consorcio" value={this.state.idConsorcio} />
            
          <CreateUnidad
            onSubmit={this.onSubmit}
            loading={this.state.status == 'loading'}
            errorMessage={this.props.crearUnidadState.error || this.props.crearResponsableState.error}
            successMessage="unidad agregada"
            success={this.state.status === 'success'}
          />
        </SimpleFormWrap>
      </div>
    );
  }

}
