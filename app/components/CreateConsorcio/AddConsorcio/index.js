import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import _ from 'lodash';

import Layout from 'components/Layout';
import FormWrap from 'components/shared/FormWrap';
import InputText from 'components/shared/InputText';
import InputLocation from 'components/shared/InputLocation';
import InputChoiceTable from 'components/shared/InputChoiceTable';
import InputHidden from 'components/shared/InputHidden';

import { actionCreators } from 'actions/api';

@connect(
  (store) => ({
    crearConsorcioState: store.get('api').crearConsorcio || {},
  }),
  (dispatch) => ({
    crearConsorcio: bindActionCreators(actionCreators, dispatch).crearConsorcio,
    crearConsorcioReset: bindActionCreators(actionCreators, dispatch).crearConsorcioReset
  })
)
export default class AddConsorcio extends React.Component {

  static propTypes = {
    crearConsorcio: React.PropTypes.func.isRequired,
    crearConsorcioState: React.PropTypes.object,
    onSuccess: React.PropTypes.func,
  }

  constructor(props) {
    super(props);

    // set initial state
    this.state = {
      direccion: {},
      nombre: '',
      tipo: ''
    };
    this.onChangeNombre = this.onChangeNombre.bind(this)
    this.onSelectDireccion = this.onSelectDireccion.bind(this)
    this.onChangeTipo = this.onChangeTipo.bind(this)
    this.onSubmit = this.onSubmit.bind(this)
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.crearConsorcioState.success) {
      setTimeout(() => {
        this.props.onSuccess({idConsorcio: this.props.crearConsorcioState.data.idConsorcio});
        this.props.crearConsorcioReset();
      }, 2000);
    }
  }

  onChangeNombre (event) {
    this.setState({nombre: event.target.value});
  }

  onSelectDireccion(direccion) {
    this.setState({"direccion": direccion});
  }

  onChangeTipo (event) {
    this.setState({tipo: event.target.value});
  }
  
  onSubmit(raw) {
    let newConsorcio = {}
    newConsorcio.nombre = raw.nombre
    newConsorcio.tipoConsorcio = raw.tipoConsorcio
    newConsorcio.direccionCalle = raw.location.calle
    newConsorcio.direccionNumero = raw.location.numero
    newConsorcio.latitud = raw.location.latitud
    newConsorcio.longitud = raw.location.longitud
    newConsorcio.localidad = raw.location.localidad
    newConsorcio.partido = raw.location.partido
    newConsorcio.provincia = raw.location.provincia
    newConsorcio.pais = raw.location.pais
    this.props.crearConsorcio(newConsorcio)
  }

  render() {
    return (
      <FormWrap
        titulo="Alta de Consorcio"
        onSubmit={this.onSubmit}
        loading={this.props.crearConsorcioState.loading}
        errorMessage={this.props.crearConsorcioState.error}
        successMessage="consorcio cargado exitosamente!"
        success={this.props.crearConsorcioState.success}
      >
        <InputText id="nombre" name="Nombre" mandatory={true} value={this.state.nombre} onChange={this.onChangeNombre}/>
        <InputLocation id="location" name="Dirección" mandatory={true} mandatoryFields={['calle','numero','localidad']} value={this.state.direccion} onSelect={this.onSelectDireccion}/>
        <InputChoiceTable
          id="tipoConsorcio"
          name="Tipo de Consorcio"
          mandatory={true}
          value={this.state.tipo}
          onChange={this.onChangeTipo}
          choices={
            [
              {
                value: 'condominio',
                icon: 'condominio',
                color: '#3c8dbc'
              },
              {
                value: 'barrio',
                icon: 'barrio',
                color: '#3c8dbc'
              },
              {
                value: 'edificio',
                icon: 'edificio',
                color: '#3c8dbc'
              }
            ]
          }
        />
      </FormWrap>
    );
  }

}
