import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import Layout from 'components/Layout';
import SimpleFormWrap from 'components/shared/SimpleFormWrap';
import InputText from 'components/shared/InputText';
import InputLocation from 'components/shared/InputLocation';
import InputHidden from 'components/shared/InputHidden';
import SelectPeriodo from 'components/shared/SelectPeriodo';
import InputMoney from 'components/shared/InputMoney';
import InputReactSelect from 'components/shared/InputReactSelect';

import { actionCreators } from 'actions/api';

@connect(
  (store) => ({
    actualizarConsorcioState: store.get('api').actualizarConsorcio || {},
    listarBancosState: store.get('api').listarBancos,
    layout: store.get('layout') || {}
  }),
  (dispatch) => ({
    actualizarConsorcio: bindActionCreators(actionCreators, dispatch).actualizarConsorcio,
    actualizarConsorcioReset: bindActionCreators(actionCreators, dispatch).actualizarConsorcioReset,
    listarBancos: bindActionCreators(actionCreators, dispatch).listarBancos,
  })
)
export default class AddAgregarEstadoInicial extends React.Component {

  static propTypes = {
    idConsorcio: React.PropTypes.number.isRequired,
    actualizarConsorcio: React.PropTypes.func,
    onSuccess: React.PropTypes.func,
  }

  constructor(props) {
    super(props);
 
    const arr = Array.from(new Array(28), (x,i) => ++i);
    const diaCierreOptions = arr.map((arr, b) => ({ label: arr, value: arr }));

    // set initial state
    this.state = {
      inputValues:
      {
        numeroCuenta: "",
        cuit: "",
        saldoInicial: 0,
        diaCierre: 28,
        codigoPeriodo: "",
        saldoCajaInicial: 0
      },
      bancos: [{label:'cargando'}],
      diaCierreOptions: diaCierreOptions
    };

    this.onComponentValueChange = this.onComponentValueChange.bind(this)
    this.onSubmit = this.onSubmit.bind(this)
    this.onChangePeriodo = this.onChangePeriodo.bind(this)
    this.setItemCombo = this.setItemCombo.bind(this);
  }

  componentWillMount() {
    this.props.listarBancos();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.listarBancosState && nextProps.listarBancosState.data && nextProps.listarBancosState !== this.props.listarBancosState) {
      let _bancos = nextProps.listarBancosState.data.map((item) => ({value: item.idBanco, label: item.banco}))
      _bancos.unshift({value: '', laben:'seleccione un banco'});
      this.setState({
        bancos: _bancos
      })
    }

    if (!nextProps.actualizarConsorcioState.loading && nextProps.actualizarConsorcioState.success) {
      var inputValues = this.state.inputValues;
      for (var i in inputValues) {
        inputValues[i] = '';
      };
      this.setState({ "inputValues": inputValues });

      setTimeout(() => {
        let nombreUrl = this.props.layout.consorcio ? this.props.layout.consorcio.nombreUrl : nextProps.actualizarConsorcioState.data.nombreUrl
        this.props.onSuccess(nombreUrl)
        this.props.actualizarConsorcioReset();
      }, 1000);
    }
  }

  onComponentValueChange(event) {
    let val = {};

    // capture the input value
    if (event.target.type === "number") {
      val[event.target.id] = parseInt(event.target.value);
    } else {
      val[event.target.id] = event.target.value;
    }
    // remove input error if there was any as the input value changed
    val[`${event.target.id}Error`] = '';

    // save in state
    this.setState({ "inputValues": _.extend(this.state.inputValues, val) });
  }

  setItemCombo(index, value) {
    var val = [];
    val[index] = value;
    val[`${index}Error`] = '';
    this.setState({ "inputValues": _.extend(this.state.inputValues, val) });
  }

  onChangePeriodo(value) {
    let val = {};
    // capture the input value
    val['codigoPeriodo'] = value
    // remove input error if there was any as the input value changed
    val['codigoPeriodoError'] = '';
    this.setState({ "inputValues": _.extend(this.state.inputValues, val) });
  }

  onSubmit(missingFields) {
    if (missingFields) {
      var val = [];
      missingFields.map((value) => {
        val[`${value.id}Error`] = value.errorMessage;
      });
      this.setState({ "inputValues": _.extend(this.state.inputValues, val) });
    } else {
      this.props.actualizarConsorcio({
        idConsorcio: this.props.idConsorcio,
        idBanco: this.state.inputValues.idBanco,
        numeroCuenta: this.state.inputValues.numeroCuenta,
        cuit: this.state.inputValues.cuit,
        diaCierre: this.state.inputValues.diaCierre,
        ultimoPeriodo: this.state.inputValues.codigoPeriodo,
        saldoInicial: this.state.inputValues.saldoInicial,
        saldoCajaInicial: this.state.inputValues.saldoCajaInicial
      });
    }
  }

  render() {
    return (
      <SimpleFormWrap
        titulo="Estado contable inicial"
        onSubmit={this.onSubmit}
        loading={this.props.actualizarConsorcioState.loading}
        errorMessage={this.props.actualizarConsorcioState.error}
        successMessage="Estado inicial asentado"
        success={this.props.actualizarConsorcioState.success}
        disabled={this.props.actualizarConsorcioState.loading}
      >
        <InputHidden id="idConsorcio" name="consorcio" value={this.props.idConsorcio} />
        
        <InputReactSelect
          id="idBanco"
          name="Banco"
          mandatory={true}
          onChange={this.setItemCombo}
          value={this.state.inputValues.idBanco}
          options={this.state.bancos}
          errorMessage={this.state.inputValues.idBancoError}
        />

        <InputText id="numeroCuenta" name="Número cuenta" mandatory={false} value={this.state.inputValues.numeroCuenta} onChange={this.onComponentValueChange} errorMessage={this.state.inputValues.numeroCuentaError} />
        <InputText id="cuit" name="CUIT" mandatory={false} value={this.state.inputValues.cuit} onChange={this.onComponentValueChange} errorMessage={this.state.inputValues.cuitError}/>
        
        <InputReactSelect
          id="diaCierre"
          name="Dia Cierre"
          mandatory={true}
          onChange={this.setItemCombo}
          value={this.state.inputValues.diaCierre}
          options={this.state.diaCierreOptions}
          errorMessage={this.state.inputValues.diaCierreError}
        />                

        <div className="box box-primary form-unidades h-center col-sm-12">
          <div className="box-header">
            <h3 className="box-title">Ultimo periodo Liquidado</h3>
          </div>
          <div className="box-body">
            <div className={`form-group col-sm-4 ${(this.state.inputValues.codigoPeriodoError ? 'has-error' : '')}`}>
              <label htmlFor="codigoPeriodo" className="control-label">Periodo</label>
              <SelectPeriodo id="codigoPeriodo" onChange={this.onChangePeriodo} diaCierre={String(this.state.inputValues.diaCierre)} rangoPeriodos={[-9,3]}/>
            </div>
            <InputMoney id="saldoCajaInicial" name="Saldo caja" mandatory={true} value={this.state.inputValues.saldoCajaInicial} onChange={this.onComponentValueChange} errorMessage={this.state.inputValues.saldoCajaInicialError} width="4" />
            <InputMoney id="saldoInicial" name="Saldo final" mandatory={true} value={this.state.inputValues.saldoInicial} onChange={this.onComponentValueChange} errorMessage={this.state.inputValues.saldoInicialError} width="4" />
          </div>
        </div>
      </SimpleFormWrap>
    );
  }
}
