import React from 'react';

import Layout from 'components/Layout';
import SteppedProcess from 'components/shared/SteppedProcess';
import Step from 'components/shared/Step';

import AddConsorcio from './AddConsorcio';
import AddUnidades from './AddUnidades';
import AddEstadoInicial from './AddEstadoInicial';

export default class CargarConsorcio extends React.Component {

  static propTypes = {
    params: React.PropTypes.object
  }

  constructor(props) {
    super(props);

    // set initial state
    this.state = {};
    this.state.currentStep = props.location.query.currentStep ? parseInt(props.location.query.currentStep, 10) : 1;
    this.onSuccess = this.onSuccess.bind(this)
  }

 onSuccess(params = {}) {

    if (this.state.currentStep === 3) {
      this.props.router.push({
        pathname: '/' + params
      })
    } else {

      params = Object.assign(params, {currentStep: this.state.currentStep + 1})
      this.props.router.push({
        pathname: this.props.location.pathname,
        query: params
      })

      this.setState({
        currentStep: this.state.currentStep + 1
      })  
    }
  }

  render() {
    return (
      <Layout
        classBody="create-consorcio"
        head={(
          <section className="content-header">
            <h1>
              <i className="fa fa-dashboard"></i> Consorcios &gt; Cargar
            </h1>
          </section>)}
      >
        <SteppedProcess currentStep={this.state.currentStep}>
          <Step stepTitle="Alta de Consorcio" key="1">
            <AddConsorcio onSuccess={this.onSuccess} />
          </Step>
          <Step stepTitle="Alta de Unidades" key="2">
            <AddUnidades idConsorcio={parseInt(this.props.location.query.idConsorcio, 10)} onNext={this.onSuccess} />
          </Step>
          <Step stepTitle="Estado Inicial de finanzas" key="3">
            <AddEstadoInicial idConsorcio={parseInt(this.props.location.query.idConsorcio, 10)} onSuccess={this.onSuccess} />
          </Step>
        </SteppedProcess>
      </Layout>
    );
  }

}
