import React from 'react';
import Rodal from 'rodal';
import JsonTable from 'react-json-table';
import dateFormat from 'dateformat';

import LoadingOverlay from 'components/shared/LoadingOverlay';


let settings = {
  "noRowsMessage": <LoadingOverlay style={{bottom: 0, top: 0, left: 0, right: 0, width: 1000, maxWidth: "100%", marginBottom: 20}} />,
  "cellClass": (currentClass, columnKey, rowData )=>( columnKey === "actions" ? "action-column" : "" ),
  "headerClass": ( currentClass, columnKey )=>( "ttt" )
}

export default class RecaudacionListItem extends React.Component {

	static propTypes = {
		item: React.PropTypes.object,
		onClick: React.PropTypes.func,
    recaudacionesEnMemoria: React.PropTypes.array
	}

	constructor(props) {
		super(props)
		this.state = {
      items: [],
      columns: [],
      loading: false,
      action: null,
      shouldUpdate: false
    }
		this.onClickVerDetalle = this.onClickVerDetalle.bind(this);
    this.buildTable = this.buildTable.bind(this);
	}

	show() {
    this.setState({ visible: true });
  }

  hide() {
    this.setState({ visible: false });
  }

  onClickVerDetalle() {
    if (typeof this.props.recaudacionesEnMemoria === 'undefined') {
      this.setState({ action: "verDetalle", loading: true, shouldUpdate: true });
      this.props.onClick(this.props.item);
    } else {
      this.show()
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.state.shouldUpdate) {
      if (typeof this.props.recaudacionesEnMemoria === 'undefined') {
        this.setState({ loading: false, shouldUpdate: false });
        this.buildTable(nextProps.recaudacionesEnMemoria)
        this.show();
      }
    }
  }

  buildTable(data) {

    let totalMonto = 0;

    let value = [];
    _.map( data, (val, key) => {
      totalMonto += parseFloat(val.monto);
      value[key] = {
        "periodoCodigo": val.periodoCodigo,
        "cuota": `${val.cuotaActual}/${val.cuotas}`,
        "monto": `$${parseFloat(val.monto).formatMoney(2, ',', '.')}`,
        "estado": val.procesado == true ? "procesado" : "sin procesar"
        };
    });

    this.setState({
      "items": value,
      "columns": [
        {key: 'periodoCodigo', label: 'Periodo'},
        {key: 'cuota', label: 'Cuota'},
        {key: 'monto', label: 'Monto'},
        {key: 'estado', label: 'Estado'},
      ],
      "totalMonto": totalMonto
    });
  }

	render() {
    let items = {
      ...this.state
    }
		return (
			<div>
				<ul className="action-buttons">
          <li>
  					<button onClick={this.onClickVerDetalle} className="btn btn-default-toro flat">
  						{
  						  this.state.loading == true && this.state.action === "verDetalle"
  						?
                <i className="fa fa-refresh fa-spin"></i>
  						:
                <i className="glyphicon glyphicon-eye-open"></i>
  						}
  						<span>Ver Detalle</span>
            </button>
					</li>
				</ul>

      	<Rodal visible={this.state.visible} onClose={this.hide.bind(this)} width={90} height={80} measure="%" customStyles={{height:"auto", "overflow":"scroll","margin":"20px auto"}}>
          {
            this.state.visible
          ?
            (
              <div className="col-md-12">
                <div className="box box-primary form-horizontal">
                  <div className="box-header with-border">
                    <h3 className="box-title">Detalle de cuotas de la Recaudacion</h3>
                  </div>

                  <div className="box-body">
                    <div className="form-group">
                      <label className="col-sm-2">Monto</label>
                      <div className="col-sm-10">
                        ${parseFloat(this.state.totalMonto).formatMoney(2, ',', '.')}
                      </div>
                    </div>
                    <div className="form-group">
                      <label className="col-sm-2">Cuotas</label>
                      <div className="col-sm-10">
                        {this.props.item.detail.cuotas}
                      </div>
                    </div>
                    <div className="form-group">
                      <label className="col-sm-2">Descripcion</label>
                      <div className="col-sm-10">
                        {this.props.item.detail.descripcion}
                      </div>
                    </div>
                    <div className="form-group">
                      <label className="col-sm-2">Uuid</label>
                      <div className="col-sm-10">
                        {this.props.item.detail.uuid}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            )
          :
            ""
          }
        <div className="row">
          <JsonTable rows={items.items} columns={items.columns} className="table table-bordered table-hover dataTable" settings={settings} classPrefix="" />
        </div>
      	</Rodal>
      </div>
		);
	}
}
