import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Rodal from 'rodal';

import { actionCreators } from 'actions/api';
import SimpleFormWrap from 'components/shared/SimpleFormWrap';
import InputText from 'components/shared/InputText';
import InputChoice from 'components/shared/InputChoice';
import InputReactSelect from 'components/shared/InputReactSelect';

@connect(
  (store) => ({
    crearRecaudacionState: store.get('api').crearRecaudacion || {},
    layout: store.get('layout') || {}
  }),
  (dispatch) => ({
    crearRecaudacion: bindActionCreators(actionCreators, dispatch).crearRecaudacion,
    crearRecaudacionReset: bindActionCreators(actionCreators, dispatch).crearRecaudacionReset
  })
)
export default class CreaRecaudacion extends React.Component {

  static propTypes = {
    data: React.PropTypes.object,
    textBtn: React.PropTypes.string.isRequired,
    onSuccess: React.PropTypes.func
  }

  constructor(props) {
    super(props);
    this.onSubmit = this.onSubmit.bind(this);
    this.onComponentValueChange = this.onComponentValueChange.bind(this);
    this.setItemCombo = this.setItemCombo.bind(this);

    const arr = Array.from(new Array(12), (x,i) => ++i);
    const cuotasOptions = arr.map((arr, b) => ({ label: arr, value: arr }));

    this.state = {
      visible: false,
      error: null,
      reset: false,
      categorias: null,
      inputValues: [],
      cuotasOptions: cuotasOptions
    };
  }

  show() {
    this.setState({ visible: true });
  }

  hide() {
    this.setState({ visible: false });
  }

  onSubmit(missingFields) {
    if (missingFields) {
      var val = [];
      missingFields.map((value) => {
        val[`${value.id}Error`] = value.errorMessage;
      });
      this.setState({ "inputValues": _.extend(this.state.inputValues, val) });
    } else {
      this.props.crearRecaudacion({
        idConsorcio: this.props.layout.consorcio.idConsorcio,
        monto: this.state.inputValues.monto,
        cuotas: this.state.inputValues.cuotas,
        periodoCodigo: this.props.layout.consorcioSelected.idPeriodoSelected,
        descripcion: this.state.inputValues.descripcion
      });
    }    
  }

  componentWillReceiveProps(nextProps) {

    if (nextProps.crearRecaudacionState.success  && nextProps.crearRecaudacionState.data && nextProps.crearRecaudacionState.data !== this.props.crearRecaudacionState.data) {

      this.props.onSuccess();
      this.setState({ error: null, reset: true  });

      var inputValues = this.state.inputValues;
      for (var i in inputValues) {
        inputValues[i] = '';
      };
      this.setState({ "inputValues":inputValues });

      setTimeout(() => {
        this.setState({ visible: false, reset: false});
        this.props.crearRecaudacionReset();
      }, 2000);

    } else if (nextProps.crearRecaudacionState.error) {
      this.setState({ error: this.props.crearRecaudacionState.error });
    }
  }

  onComponentValueChange(e) {
    let val = [];    
    val[e.target.id] = e.target.value;
    val[`${e.target.id}Error`] = '';

    if (e.target.id === "monto") {
      // e.target.value = e.target.value.replace(/\D/g, '');
      if (!_.isEmpty(e.target.value) && Math.sign(e.target.value) >= 0) {
        val["monto"] = String(-e.target.value);
      }
    }
    this.setState({ "inputValues": _.extend(this.state.inputValues, val) });
  }

  setItemCombo(index, value) {
    var val = [];
    val[index] = value;
    val[`${index}Error`] = '';
    this.setState({ "inputValues": _.extend(this.state.inputValues, val) });
  }

  render() {
    // const disabled = this.props.layout.consorcioSelected.idPeriodoSelected !== this.props.layout.consorcioSelected.idPeriodoActual ? true : false;
    const disabled = false;
    return (
      <div>
        <button disabled={disabled} className="btn btn-flat btn-default-toro file-input pull-right" onClick={this.show.bind(this)}>
          {this.props.textBtn}
        </button>

        <Rodal visible={this.state.visible} onClose={this.hide.bind(this)} customStyles={{height:"auto", "overflow":"scroll","margin":"20px auto"}}>

          <SimpleFormWrap
            titulo="Nueva Recaudacion"
            onSubmit={this.onSubmit}
            loading={this.props.crearRecaudacionState.loading}
            errorMessage={this.state.error}
            successMessage="nuevo recaudacion cargada"
            success={this.props.crearRecaudacionState.success}
            disabled={this.props.crearRecaudacionState.loading}
            >

            <InputText id="descripcion" name="Descripcion" onChange={this.onComponentValueChange} value={this.state.inputValues.descripcion} errorMessage={this.state.inputValues.descripcionError} />
            <InputText id="monto" name="Monto" mandatory={true} onChange={this.onComponentValueChange} value={this.state.inputValues.monto} errorMessage={this.state.inputValues.montoError} />

            <InputReactSelect
              id="cuotas"
              name="Cuotas"
              mandatory={true}
              onChange={this.setItemCombo}
              value={this.state.inputValues.cuotas}
              options={this.state.cuotasOptions}
              errorMessage={this.state.inputValues.cuotasError}
              widthLabel="3"
            />

          </SimpleFormWrap>

        </Rodal>
      </div>
    );
  }
}
