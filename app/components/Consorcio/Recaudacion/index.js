import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import JsonTable from 'react-json-table';
import dateFormat from 'dateformat';

import { actionCreators } from 'actions/api';
import CreateRecaudacion from './CreateRecaudacion';
import RecaudacionListItem from './RecaudacionListItem';
import LoadingOverlay from 'components/shared/LoadingOverlay';

let settings = {
  "noRowsMessage": <LoadingOverlay style={{bottom: 0, top: 0, left: 0, right: 0, width: 1000, maxWidth: "100%", marginBottom: 20}} />,
  "cellClass": (currentClass, columnKey, rowData )=>( columnKey === "actions" ? "action-column" : "" ),
  "headerClass": ( currentClass, columnKey )=>( "ttt" )
}

@connect(
  (store) => ({
    obtenerRecaudacionesState: store.get('api').obtenerRecaudaciones || {loading: true},
    obtenerRecaudacionesPorUuidState: store.get('api').obtenerRecaudacionesPorUuid || {},
    listarCategoriasState: store.get('api').listarCategorias || {},
    layout: store.get('layout') || {}
  }),
  (dispatch) => ({
    obtenerRecaudaciones: bindActionCreators(actionCreators, dispatch).obtenerRecaudaciones,
    obtenerRecaudacionesPorUuid: bindActionCreators(actionCreators, dispatch).obtenerRecaudacionesPorUuid,
    listarCategorias: bindActionCreators(actionCreators, dispatch).listarCategorias
  })
)
export default class Recaudacion extends React.Component {

  static propTypes = {
    layout: React.PropTypes.object,
    obtenerRecaudaciones: React.PropTypes.func
  }

  constructor(props) {
    super(props);
    this.state = {
      items: [],
      columns: [],
      recaudacionesEnMemoria: [],
      uuidRetrieved: null
    };
    this.actionsRow = this.actionsRow.bind(this);
    this.buildTable = this.buildTable.bind(this);
    this.guardarRecaudacionesPorUuid = this.guardarRecaudacionesPorUuid.bind(this);
    this.onSuccess = this.onSuccess.bind(this);
    this.onClickAction = this.onClickAction.bind(this);
  }

  componentWillMount() {
    if (this.props.obtenerRecaudacionesState.data) {
      this.buildTable(this.props.obtenerRecaudacionesState.data)
    }
  }

  componentWillReceiveProps(nextProps) {

    if (nextProps.obtenerRecaudacionesState.loading && !nextProps.obtenerRecaudacionesState.success) {
      this.setState({ items: [], columns: [] });

    }else if (nextProps.obtenerRecaudacionesState.loading === false && nextProps.obtenerRecaudacionesState.data) {
      this.buildTable(nextProps.obtenerRecaudacionesState.data);
    }

    if (typeof this.state.recaudacionesEnMemoria[this.state.uuidRetrieved] === 'undefined') {
      if (typeof nextProps.obtenerRecaudacionesPorUuidState.data != 'undefined') {
        this.guardarRecaudacionesPorUuid(this.state.uuidRetrieved, nextProps.obtenerRecaudacionesPorUuidState.data);
      }
    }

  }

  onSuccess() {
    this.props.obtenerRecaudaciones({ idConsorcio: this.props.layout.consorcio.idConsorcio,  idPeriodo: this.props.layout.consorcioSelected.idPeriodoSelected })
  }

  guardarRecaudacionesPorUuid(uuid, data) {
    var values = this.state.recaudacionesEnMemoria ;
    values[uuid] = data;
    this.setState({ recaudacionesEnMemoria: values });
  }

  onClickAction(item){
    this.setState({ uuidRetrieved: item.detail.uuid })
    this.props.obtenerRecaudacionesPorUuid({ idConsorcio: this.props.layout.consorcio.idConsorcio, "uuid": item.detail.uuid });
  }

  buildTable(data) {

    if (_.isEmpty(data)) {
      data = [{ "idRecaudacion":null,"idConsorcio":null,"monto":null,"cuotas":null,"cuotaActual":null,"uuid":null,"descripcion":null,"extraordinaria":null,"periodoCodigo":null,"createdAt":null,"updatedAt":null,"deleted":null, "procesado": null }];
    }

    let value = [];
    _.map( data, (val, key) => {
      value[key] = {
        "periodoCodigo": val.periodoCodigo,
        "descripcion": val.descripcion ? val.descripcion.slice(0, 15) : "",
        "cuota": val.cuotas ? `${val.cuotaActual}/${val.cuotas}` : "",
        "monto": val.monto ? `$${parseFloat(val.monto).formatMoney(2, ',', '.')}` : "",
        "estado": val.procesado ? (val.procesado == true ? "procesado" : "sin procesar") : "",
        "detail": {
          "uuid": val.uuid,
          "cuotas": val.cuotas,
          "descripcion": val.descripcion,
          "extraordinaria": val.extraordinaria
          },
        };
    });

    this.setState({
      "items": value,
      "columns": [
        {key: 'periodoCodigo', label: 'Periodo'},
        {key: 'descripcion', label: 'Descripcion'},
        {key: 'cuota', label: 'Cuota'},
        {key: 'monto', label: 'Monto'},
        {key: 'estado', label: 'Estado'},
        {key: 'actions', label: 'Acciones'}
      ]
    });
  }

  actionsRow(item) {
    return <RecaudacionListItem item={item} recaudacionesEnMemoria={this.state.recaudacionesEnMemoria[item.detail.uuid]} onClick={this.onClickAction}/>
  }

  render() {

    let state = {
      ...this.state
    }
    if (!_.isEmpty(state.items)) {
      state.columns[5].cell = ( item, columnKey ) => {
        return item.descripcion ? this.actionsRow(item) : null;
      }
    }
    return (
      <div>
        <div className="buscador_filtros">
          <div className="row">
            <div className="col-md-8">
            {
              this.props.layout.consorcio
            ?
              <div className="col-sm-2">
                {this.props.children}
              </div>
            :
              ""
            }
            </div>
            <div className="col-md-4">
              <CreateRecaudacion onSuccess={this.onSuccess} data={this.props} textBtn="Crear Recaudacion"/>
            </div>
          </div>
        </div>  

        <div className="box-header">
          <h3 className="box-title">Recaudaciones del período</h3>
        </div>    

        <div className="row">
          <JsonTable rows={state.items} columns={state.columns} className="table table-bordered table-hover dataTable" settings={settings} classPrefix="" />
        </div>

      </div>
    );
  }
}