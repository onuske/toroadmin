import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import JsonTable from 'react-json-table';
import dateFormat from 'dateformat';

import { actionCreators } from 'actions/api';
import CreateMovimiento from './CreateMovimiento';
import LoadingOverlay from 'components/shared/LoadingOverlay';
import SelectCategoria from 'components/shared/SelectCategoria';

let settings = {
  "noRowsMessage": <LoadingOverlay style={{bottom: 0, top: 0, left: 0, right: 0, width: 1000, maxWidth: "100%", marginBottom: 20}} />,
  "cellClass": (currentClass, columnKey, rowData )=>( columnKey === "actions" ? "action-column" : "" ),
  "headerClass": ( currentClass, columnKey )=>( "ttt" )
}

@connect(
  (store) => ({
    obtenerMovimientosCajaState: store.get('api').obtenerMovimientosCaja || {loading: true},
    listarCategoriasState: store.get('api').listarCategorias || {},
    listarUnidadesState: store.get('api').listarUnidades || {},
    setearMovimientoCajaState: store.get('api').setearMovimientoCaja || {},
    layout: store.get('layout') || {}
  }),
  (dispatch) => ({
    obtenerMovimientosCaja: bindActionCreators(actionCreators, dispatch).obtenerMovimientosCaja,
    listarCategorias: bindActionCreators(actionCreators, dispatch).listarCategorias,
    listarUnidades: bindActionCreators(actionCreators, dispatch).listarUnidades,
    setearMovimientoCaja: bindActionCreators(actionCreators, dispatch).setearMovimientoCaja
  })
)
export default class Caja extends React.Component {

  static propTypes = {
    layout: React.PropTypes.object,
    obtenerMovimientosCaja: React.PropTypes.func
  }

  constructor(props) {
    super(props);
    this.state = {
      items: [],
      columns: [],
      inputValues: [],
      categorias: null,
      subCategorias: null,
      unidades: null
    };
    this.onSuccess = this.onSuccess.bind(this);
    this.actionsRow = this.actionsRow.bind(this);
    this.buildTable = this.buildTable.bind(this);
    this.fillCategoriasOptions = this.fillCategoriasOptions.bind(this);
    this.fillSubCategoriasOptions = this.fillSubCategoriasOptions.bind(this);
    this.fillUnidadesOptions = this.fillUnidadesOptions.bind(this);
    this.setearCaja = this.setearCaja.bind(this);
  }

  componentWillMount() {
    if (this.props.obtenerMovimientosCajaState.data) {
      this.buildTable(this.props.obtenerMovimientosCajaState.data)
    }

    if (!this.props.layout.categorias) {
      this.props.listarCategorias();
    } else {
      this.fillCategoriasOptions(this.props.layout.categorias);
      let subCat = _.filter(this.props.layout.categorias, (val) => (typeof val.subCategorias !== 'undefined'));
      this.fillSubCategoriasOptions(subCat[0].subCategorias);
    }

    if (this.props.layout.unidades) {
      this.fillUnidadesOptions(this.props.layout.unidades);
    }
  }

  componentWillReceiveProps(nextProps) {

    if (nextProps.layout) {

      if (_.isEmpty(this.state.categorias) && nextProps.layout.categorias) {
        this.fillCategoriasOptions(nextProps.layout.categorias);
        var subCat = _.filter(nextProps.layout.categorias, (val) => (typeof val.subCategorias !== 'undefined'));
        this.fillSubCategoriasOptions(subCat[0].subCategorias);
      }

      if (_.isEmpty(this.state.unidades) && nextProps.layout.unidades) {
        this.fillUnidadesOptions(nextProps.layout.unidades);
      }
    }

    if (nextProps.obtenerMovimientosCajaState.loading && !nextProps.obtenerMovimientosCajaState.success) {
      this.setState({ items: [], columns: [] });
    } else if (nextProps.obtenerMovimientosCajaState.success && nextProps.obtenerMovimientosCajaState.data) {
      this.buildTable(nextProps.obtenerMovimientosCajaState.data);
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (!this.props.layout.unidades && !prevProps.layout.consorcio && this.props.layout.consorcio ) {
      this.props.listarUnidades({ idConsorcio: this.props.layout.consorcio.idConsorcio });
    }
  }

  setearCaja(id, value) {
    value["idMovimientoCaja"] = id
    this.props.setearMovimientoCaja(value)
  }

  fillCategoriasOptions(data) {
    this.setState({
      categorias: data.map((cat) => ({ label: cat.nombre, value: cat.idCategoria }))
    })
  }

  fillSubCategoriasOptions(data) {
    this.setState({
      subCategorias: data.map((cat) => ({ label: cat.nombre, value: cat.idSubCategoria }))
    })
  }

  fillUnidadesOptions(data) {
    this.setState({
      unidades: data.map((unidad) => ({ label: unidad.unidad, value: unidad.idUnidad }))
    })
  }

  onSuccess(params = {}) {
    this.props.obtenerMovimientosCaja({ idConsorcio: this.props.layout.consorcio.idConsorcio,  idPeriodo: this.props.layout.consorcioSelected.idPeriodoSelected })
  }

  buildTable(data) {
    // {"idMovimientoCaja":222,"idConsorcio":196,"fecha":"2017-06-05T03:00:00.000Z","monto":"-2875.00","saldo":"-11986.17","nroComprobante":null,"idCategoria":5,"idUnidad":null,"observaciones":"Honorarios Administración Ravazzola. MAYO 2017.","procesado":false,"createdAt":"2017-09-21T09:39:56.286Z","updatedAt":"2017-09-21T09:39:56.286Z","deleted":null,"idSubCategoria":null}
    if (_.isEmpty(data)) {
      data = [{"idMovimientoCaja": null, "idConsorcio": null, "fecha": null, "monto": null, "saldo": null, "nroComprobante": null, "idCategoria": null, "idUnidad": null ,"observaciones": null, "procesado": null, "createdAt": null, "updatedAt": null, "deleted": null, "idSubCategoria": null }];
    }

    let value = [];
    _.map( data, (val, key) => {
      value[key] = {
        "fecha": !_.isEmpty(val.fecha) ? dateFormat(val.fecha, "dd-mm-yyyy") : null,
        "monto": val.monto ? `$${parseFloat(val.monto).formatMoney(2, ',', '.')}` : "",
        "saldo": val.saldo ? `$${parseFloat(val.saldo).formatMoney(2, ',', '.')}` : "",
        "descripcion": val.observaciones ? val.observaciones.slice(0, 15) : "",
        "estado": val.procesado !== null ? (val.procesado == true ? "procesado" : "sin procesar") : "",
        "detail": {
          "idMovimientoCaja": val.idMovimientoCaja,
          "idUnidad": val.idUnidad,
          "idCategoria": val.idCategoria,
          "idSubCategoria": val.idSubCategoria,
          "procesado": val.procesado
          },
        };
    });

    this.setState({
      "items": value,
      "columns": [
        {key: 'fecha', label: 'Fecha'},
        {key: 'descripcion', label: 'Descripcion'},
        {key: 'monto', label: 'Monto'},
        {key: 'saldo', label: 'Saldo'},
        {key: 'estado', label: 'Estado'},
        {key: 'actions', label: 'Categoría'}
      ]
    });
  }

  actionsRow(item) {
    return <SelectCategoria
            item={item}
            idMovimiento={item.detail.idMovimientoCaja}
            categorias={this.state.categorias}
            ubCategorias={this.state.subCategorias}
            unidades={this.state.unidades}
            onChange={this.setearCaja}
           />
  }

  render() {

    let state = {
      ...this.state
    }
    if (!_.isEmpty(state.items)) {
      state.columns[5].cell = ( item, columnKey ) => {
        return item.descripcion ? this.actionsRow(item) : null;
      }
    }

    return (
      <div>
        <div className="buscador_filtros">
          <div className="row">
            <div className="col-md-8">
              <div className="col-sm-2">
                {this.props.children}
              </div>
            </div>
            <div className="col-md-4">
              <CreateMovimiento onSuccess={this.onSuccess} data={this.props} textBtn="Crear Movimiento Caja"/>
            </div>
          </div>
        </div>

        <div className="box-header">
          <h3 className="box-title">Movimientos de caja del período</h3>
        </div>    

        <div className="row">
          <JsonTable rows={state.items} columns={state.columns} className="table table-bordered table-hover dataTable" settings={settings} classPrefix="" />
        </div>

      </div>
    );
  }
}