import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Rodal from 'rodal';

import { actionCreators } from 'actions/api';
import SimpleFormWrap from 'components/shared/SimpleFormWrap';
import InputText from 'components/shared/InputText';
import InputChoice from 'components/shared/InputChoice';
import InputReactSelect from 'components/shared/InputReactSelect';
import InputDate from 'components/shared/InputDate';

const CATEGORIA = {
  PAGO_DE_EXPENSAS : 1,
  INGRESO_A_CUENTA : 2,
  GASTO: 3,
  MOVIMIENTO_YA_LIQUIDADO : 4,
  COMPENSACION : 5,
}

@connect(
  (store) => ({
    crearMovimientosCajaState: store.get('api').crearMovimientosCaja || {}
  }),
  (dispatch) => ({
    crearMovimientosCaja: bindActionCreators(actionCreators, dispatch).crearMovimientosCaja
  })
)
export default class CreateMovimiento extends React.Component {

  static propTypes = {
    data: React.PropTypes.object,
    textBtn: React.PropTypes.string.isRequired,
    onSuccess: React.PropTypes.func
  }

  constructor(props) {
    super(props);
    this.onSubmit = this.onSubmit.bind(this);
    this.onComponentValueChange = this.onComponentValueChange.bind(this);
    this.setItemCombo = this.setItemCombo.bind(this);
    this.fillCategoriasOptions = this.fillCategoriasOptions.bind(this);
    this.fillSubCategoriasOptions = this.fillSubCategoriasOptions.bind(this);
    this.fillUnidadesOptions = this.fillUnidadesOptions.bind(this);
    this.state = {
      visible: false,
      success: false,
      error: null,
      categorias: null,
      subCategorias: null,
      unidades: null,
      inputValues: [] 
    };
  }

  show() {
    this.setState({ visible: true,  success: false });
  }

  hide() {
    this.setState({ visible: false });
  }


  componentWillMount() {

    if (this.props.data.layout.categorias) {
      this.fillCategoriasOptions(this.props.data.layout.categorias);
      let subCat = _.filter(this.props.data.layout.categorias, (val) => (typeof val.subCategorias !== 'undefined'));
      this.fillSubCategoriasOptions(subCat[0].subCategorias);
    }

    if (this.props.data.layout.unidades) {
      this.fillUnidadesOptions(this.props.data.layout.unidades);
    }
  }

  componentWillReceiveProps(nextProps) {

    if (nextProps.crearMovimientosCajaState.error) {
      this.setState({ error: nextProps.crearMovimientosCajaState.error });
    }

    if (this.state.success) {
      var inputValues = this.state.inputValues;
      for (var i in inputValues) {
        inputValues[i] = '';
      };
      this.setState({ "inputValues":inputValues });
    }

    if (nextProps.data.layout) {

      if (_.isEmpty(this.state.categorias) && nextProps.data.layout.categorias) {
        this.fillCategoriasOptions(nextProps.data.layout.categorias);
        var subCat = _.filter(nextProps.data.layout.categorias, (val) => (typeof val.subCategorias !== 'undefined'));
        this.fillSubCategoriasOptions(subCat[0].subCategorias);
      }

      if (_.isEmpty(this.state.unidades) && nextProps.data.layout.unidades) {
        this.fillUnidadesOptions(nextProps.data.layout.unidades);
      }
    }

    if (nextProps.crearMovimientosCajaState.success) {

      this.props.onSuccess();
      nextProps.crearMovimientosCajaState.success = false;
      this.setState({ error: null, success: true });

      setTimeout(() => {
        this.setState({ visible: false });
      }, 2000);
    }
  }

  onSubmit(missingFields) {
    if (this.state.inputValues.idCategoria === CATEGORIA.GASTO && !this.state.inputValues.idSubCategoria) {
      let missingInput = [{ id: "idSubCategoria", name: "SubCategoria" ,errorMessage: "falta completar Sub-categoria" }]
      !missingFields ? missingFields = missingInput : missingFields.push(missingInput)
    }

    if (missingFields) {
      var val = [];
      missingFields.map((value) => {
        val[`${value.id}Error`] = value.errorMessage;
      });
      this.setState({ "inputValues": _.extend(this.state.inputValues, val) });
    } else {

      let name = null
      let value = null
      let data = {
        idConsorcio: this.props.data.layout.consorcio.idConsorcio,
        fecha: this.state.inputValues.fecha,
        monto: this.state.inputValues.monto,
        nroComprobante: this.state.inputValues.nroComprobante,
        idCategoria: this.state.inputValues.idCategoria,
        observaciones: this.state.inputValues.observaciones
      }
      if (this.state.inputValues.idCategoria === CATEGORIA.PAGO_DE_EXPENSAS && this.state.inputValues.idUnidad) {
        name = "idUnidad"
        value = this.state.inputValues.idUnidad
      } else if (this.state.inputValues.idCategoria === CATEGORIA.GASTO) {
        name = "idSubCategoria"
        value = this.state.inputValues.idSubCategoria
      }
      data[name] = value
      this.props.crearMovimientosCaja(data);
    }
  }

  fillCategoriasOptions(data) {
    this.setState({
      categorias: data.map((cat) => ({ label: cat.nombre, value: cat.idCategoria }))
    })
  }

  fillSubCategoriasOptions(data) {
    this.setState({
      subCategorias: data.map((cat) => ({ label: cat.nombre, value: cat.idSubCategoria }))
    })
  }

  fillUnidadesOptions(data) {
    this.setState({
      unidades: data.map((unidad) => ({ label: "UF " + unidad.numero + " - " +  unidad.unidad, value: unidad.idUnidad }))
    })
  }

  onComponentValueChange(e) {
    let val = [];

    if (e.target.id === "monto" && Math.sign(e.target.value) == 1 && this.state.inputValues.tipoMovimiento === "egreso") {
      val["monto"] = String(-e.target.value);
      this.setState({ "inputValues": _.extend(this.state.inputValues, val) });
    } else {

      if (e.target.id === "tipoMovimiento" && !_.isEmpty(this.state.inputValues.monto)) {
        if (e.target.value === "ingreso") {  
          val["monto"] = String(Math.abs(this.state.inputValues.monto));
        } else if (Math.sign(this.state.inputValues.monto) == 1) {
          val["monto"] = String(-this.state.inputValues.monto);
        }
      }

      val[e.target.id] = e.target.value;
      val[`${e.target.id}Error`] = '';
      this.setState({ "inputValues": _.extend(this.state.inputValues, val) });
    }
  }

  setItemCombo(index, value) {
    if (value === CATEGORIA.PAGO_DE_EXPENSAS) {
      this.props.data.listarUnidades({ idConsorcio: this.props.data.layout.consorcio.idConsorcio });
    }
    var val = [];
    val[index] = value;
    val[`${index}Error`] = '';
    this.setState({ "inputValues": _.extend(this.state.inputValues, val) });
  }

  render() {
    return (
      <div>
        <button className="btn btn-flat btn-default-toro file-input pull-right" onClick={this.show.bind(this)}>
          {this.props.textBtn}
        </button>

        <Rodal visible={this.state.visible} onClose={this.hide.bind(this)} customStyles={{height:"auto", "overflow":"scroll","margin":"20px auto"}}>

          <SimpleFormWrap
            titulo="Nuevo movimiento"
            onSubmit={this.onSubmit}
            loading={this.props.crearMovimientosCajaState.loading}
            errorMessage={this.state.error}
            successMessage="nuevo movimiento cargado"
            success={this.state.success}
            >

            <InputDate id="fecha" name="Fecha" mandatory={true} onChange={this.onComponentValueChange} value={this.state.inputValues.fecha} errorMessage={this.state.inputValues.fechaError}/>
            <InputText id="observaciones" name="Descripcion" onChange={this.onComponentValueChange} value={this.state.inputValues.observaciones} errorMessage={this.state.inputValues.observacionesError} />

            <InputChoice
              id="tipoMovimiento"
              name="Tipo de movimiento"
              onChange={this.onComponentValueChange}
              mandatory={true}
              value={this.state.inputValues.tipoMovimiento}
              choices={
                [
                  {
                    value: 'ingreso'
                  },
                  {
                    value: 'egreso'
                  }
                ]
              }
              errorMessage={this.state.inputValues.tipoMovimientoError}
            />

            <InputText id="monto" name="Monto" mandatory={true} onChange={this.onComponentValueChange} value={this.state.inputValues.monto} errorMessage={this.state.inputValues.montoError} />
            <InputText id="nroComprobante" name="Nro. de Comprobante" onChange={this.onComponentValueChange} value={this.state.inputValues.nroComprobante} errorMessage={this.state.inputValues.nroComprobanteError} />
            
            <InputReactSelect
              id="idCategoria"
              name="Categoria"
              mandatory={true}
              onChange={this.setItemCombo}
              value={this.state.inputValues.idCategoria}
              options={this.state.categorias}
              errorMessage={this.state.inputValues.idCategoriaError}
            />
            {
              this.state.inputValues.idCategoria === CATEGORIA.GASTO
            ?
              <InputReactSelect
                id="idSubCategoria"
                name="SubCategoria"
                onChange={this.setItemCombo}
                value={this.state.inputValues.idSubCategoria}
                options={this.state.subCategorias}
                errorMessage={this.state.inputValues.idSubCategoriaError}
              />
            :
              ""
            }

            {
              this.state.inputValues.idCategoria === CATEGORIA.PAGO_DE_EXPENSAS
            ?
              <InputReactSelect
                id="idUnidad"
                name="unidad"
                onChange={this.setItemCombo}
                value={this.state.inputValues.idUnidad}
                options={this.state.unidades}
                errorMessage={this.state.inputValues.idUnidadError}
              />
            :
              ""
            }

          </SimpleFormWrap>

        </Rodal>
      </div>
    );
  }
}
