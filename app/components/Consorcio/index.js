// npm imports
import React from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { actionCreators } from 'actions/api';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';

// toro imports
import Layout from 'components/Layout';
import Banco from './Banco';
import Caja from './Caja';
import Recaudacion from './Recaudacion';
import Ajuste from './Ajuste';
import Mensajes from './Mensajes';
import Publicacion from './Publicacion';
import Periodos from './Periodos';
import SelectPeriodo from 'components/shared/SelectPeriodo';


@connect(
  (store) => ({
    obtenerMovimientosBancoState: store.get('api').obtenerMovimientosBanco || {loading: true},
    obtenerMovimientosCajaState: store.get('api').obtenerMovimientosCaja || {loading: true},
    obtenerRecaudacionesState: store.get('api').obtenerRecaudaciones || {loading: true},
    obtenerMensajesState: store.get('api').obtenerMensajes || {loading: true},
    layout: store.get('layout')
  }),
  (dispatch) => ({
    obtenerMovimientosBanco: bindActionCreators(actionCreators, dispatch).obtenerMovimientosBanco,
    obtenerMovimientosCaja: bindActionCreators(actionCreators, dispatch).obtenerMovimientosCaja,
    obtenerRecaudaciones: bindActionCreators(actionCreators, dispatch).obtenerRecaudaciones,
    obtenerMensajes: bindActionCreators(actionCreators, dispatch).obtenerMensajes
  })
)
export default class PanelConsorcio extends React.PureComponent {
  static propTypes = {
    layout: React.PropTypes.object,
    params: React.PropTypes.object
  }

  constructor(props) {
    super(props);
    this.handleSelect = this.handleSelect.bind(this);
    this.state = { selectedTab : 0, firstLoad: true };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.layout && nextProps.layout.consorcio) {

      if (nextProps.layout.consorcioSelected && nextProps.layout.consorcioSelected.idPeriodoSelected) {
        let idConsorcio = nextProps.layout.consorcio.idConsorcio;
        let idPeriodo = nextProps.layout.consorcioSelected.idPeriodoSelected

        if (this.state.firstLoad && !this.props.layout.consorcioSelected.idPeriodoSelected) {

          this.setState({ firstLoad: false })
          this.props.obtenerMovimientosBanco({ idConsorcio: idConsorcio, idPeriodo: idPeriodo })
          this.props.obtenerMovimientosCaja({ idConsorcio: idConsorcio, idPeriodo: idPeriodo })
          this.props.obtenerRecaudaciones({ idConsorcio: idConsorcio, idPeriodo: idPeriodo })

        } else if (this.props.layout.consorcioSelected && (this.props.layout.consorcioSelected.idPeriodoSelected !== nextProps.layout.consorcioSelected.idPeriodoSelected)) {

          this.props.obtenerMovimientosBanco({ idConsorcio: idConsorcio, idPeriodo: idPeriodo })
          this.props.obtenerMovimientosCaja({ idConsorcio: idConsorcio, idPeriodo: idPeriodo })
          this.props.obtenerRecaudaciones({ idConsorcio: idConsorcio, idPeriodo: idPeriodo })
        }
      }

    }
  }

  handleSelect(index, last) {
    this.setState({ selectedTab: index });
  }

  render() {

    let selectPeriodo = this.props.layout.consorcio ? <SelectPeriodo diaCierre={this.props.layout.consorcio.diaCierre}/> : ""

    return (
      <Layout classBody="consorcio">
        <div className="box-header z-index-1">
          <h3 className="box-title">
            <Link to="/" className="box-volver">
              <i className="fa fa-arrow-circle-left" aria-hidden="true"></i>
            </Link>
            <span><i className="fa fa-building"></i>Consorcio - {this.props.layout.consorcio && this.props.layout.consorcio.nombre ? this.props.layout.consorcio.nombre : ''}</span>
          </h3>
        </div>

        <div className="row">
        <div className="col-md-12">
          <Tabs
              onSelect={this.handleSelect}
              selectedIndex={this.state.selectedTab}
            >
            <TabList>
              <Tab>Banco</Tab>
              <Tab>Caja</Tab>
              <Tab>Recaudacion</Tab>
              <Tab>Ajuste</Tab>
              <Tab>Períodos</Tab>
            </TabList>
            <TabPanel>
              <Banco>
                {selectPeriodo}
              </Banco>
            </TabPanel>
            <TabPanel>
              <Caja>
                {selectPeriodo}
              </Caja>
            </TabPanel>
            <TabPanel>
              <Recaudacion>
                {selectPeriodo}
              </Recaudacion>
            </TabPanel>
            <TabPanel>
              <Ajuste>
                {selectPeriodo}
              </Ajuste>
            </TabPanel>
            <TabPanel>
              <Periodos />
            </TabPanel>
          </Tabs>
        </div>
      </div>
      </Layout>
    );
  }
}
