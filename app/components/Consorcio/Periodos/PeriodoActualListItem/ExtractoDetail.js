import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import Button from 'components/shared/Button';
import dateFormat from 'dateformat';

import { actionCreators as actionCreatorsApi } from 'actions/api';
import { actionCreators as actionCreatorsExpensas} from 'actions/expensas';

import InputReactSelect from 'components/shared/InputReactSelect';
import LoadingOverlay from 'components/shared/LoadingOverlay';
import SuccessOverlay from 'components/shared/SuccessOverlay';
import SelectCategoria from 'components/shared/SelectCategoria';
import EditTextInline from 'components/shared/EditTextInline';

const CATEGORIA = {
  PAGO_DE_EXPENSAS : 1,
  INGRESO_A_CUENTA : 2,
  GASTO: 3, // GASTOS_BANCARIOS : 6
  MOVIMIENTO_YA_LIQUIDADO : 4,
  COMPENSACION : 5,
}

@connect(
  (store) => ({
    setearMovimientoBancoState: store.get('api').setearMovimientoBanco || {},
    setearMovimientoCajaState: store.get('api').setearMovimientoCaja || {},
    setearRecaudacionState: store.get('api').setearRecaudacion || {},
    layout: store.get('layout'),
    expensas: store.get('expensas')
  }),
  (dispatch) => ({
    setearMovimientoBanco: bindActionCreators(actionCreatorsApi, dispatch).setearMovimientoBanco,
    setearMovimientoBancoReset: bindActionCreators(actionCreatorsApi, dispatch).setearMovimientoBancoReset,
    setearMovimientoCaja: bindActionCreators(actionCreatorsApi, dispatch).setearMovimientoCaja,
    setearRecaudacion: bindActionCreators(actionCreatorsApi, dispatch).setearRecaudacion,
    setConciliacion: bindActionCreators(actionCreatorsExpensas, dispatch).setConciliacion
  })
)
export default class ExtractoDetail extends React.Component {

  static propTypes = {
    visible: React.PropTypes.bool.isRequired,
    idConsorcio: React.PropTypes.number.isRequired,
    idPeriodo: React.PropTypes.string.isRequired,
    nextStep: React.PropTypes.func,
    generarPreview: React.PropTypes.func,
    generarPeriodoState: React.PropTypes.object,
    generarPeriodoReset: React.PropTypes.func,
    pregenerarPeriodo: React.PropTypes.func,
    pregenerarPeriodoState: React.PropTypes.object,
    pregenerarPeriodoReset: React.PropTypes.func,
    reset: React.PropTypes.func
  }

  constructor(props) {
    super(props);
    this.getPreview = this.getPreview.bind(this);
    this.setItemCombo = this.setItemCombo.bind(this);
    this.setearBanco = this.setearBanco.bind(this);
    this.setearCaja = this.setearCaja.bind(this);
    this.setearRecaudacion = this.setearRecaudacion.bind(this);
    this.state = { "reset": false }
  }

  componentWillMount() {
    if (!this.props.layout.categorias) {
      this.props.listarCategorias();
    }
    if (!this.props.pregenerarPeriodoState.data || _.isEmpty(this.props.pregenerarPeriodoState.data)) {
      this.props.pregenerarPeriodo({ idConsorcio: this.props.idConsorcio, idPeriodo: this.props.idPeriodo })
    } else {
      this.setState({ reset: true })
      this.props.pregenerarPeriodoReset()
      this.props.reset();
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.visible) {

      if (this.props.expensas.conciliacion && this.props.layout) {
        const conciliacion = {};

        if (nextProps.layout.categorias && _.isEmpty(nextProps.expensas.conciliacion.categorias)) {
          conciliacion.categorias = nextProps.layout.categorias.map((cat) => ({label:cat.nombre, value:cat.idCategoria}))
          let subCat = _.filter(nextProps.layout.categorias, (val) => (typeof val.subCategorias !== 'undefined'));
          conciliacion.subCategorias = subCat[0].subCategorias.map((cat) => ({label:cat.nombre, value:cat.idSubCategoria}))
        }
        if (nextProps.layout.unidades && _.isEmpty(nextProps.expensas.conciliacion.unidadesSinAsignar)) {
          conciliacion.unidadesSinAsignar = nextProps.layout.unidades.map((unidad) => ( {label: unidad.unidad, value: unidad.idUnidad} ))
        }
        if (nextProps.pregenerarPeriodoState.data && !this.props.pregenerarPeriodoState.data || nextProps.pregenerarPeriodoState.data !== this.props.pregenerarPeriodoState.data) {
          conciliacion.banco = _.filter(nextProps.pregenerarPeriodoState.data, (item) => {return item.tipo === 'banco'});
          conciliacion.caja = _.filter(nextProps.pregenerarPeriodoState.data, (item) => {return item.tipo === 'caja'});
          conciliacion.recaudacion = _.filter(nextProps.pregenerarPeriodoState.data, (item) => {return item.tipo === 'recaudacion'});
        }
        
        if (!_.isEmpty(conciliacion)) {
          this.props.setConciliacion(conciliacion)
        }

        if (!nextProps.generarPeriodoState.loading && nextProps.generarPeriodoState.success) {
          this.props.nextStep();
          this.props.generarPeriodoReset();
        }
      }

    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.visible && this.state.reset) {
      this.setState({ reset: false })
      if (prevProps.pregenerarPeriodoState.data && _.isEmpty(prevProps.pregenerarPeriodoState.data && !prevProps.pregenerarPeriodoState.loading )) {
        this.props.pregenerarPeriodo({ idConsorcio: this.props.idConsorcio, idPeriodo: this.props.idPeriodo })
      }
    }
  }

  getPreview(e) {
    let banco = {};
    this.props.expensas.conciliacion.banco.forEach( (val, index) => {
      if (typeof val.idCategoria === 'undefined') {
        banco[index] = { "_categoriaError": 'falta seleccionar la categoria' };
      } else if (val.idCategoria == CATEGORIA.GASTO && typeof val.idSubCategoria === 'undefined') {
        banco[index] = { "_subCategoriaError": 'falta seleccionar la sub-categoria' };
      }
    })

    let caja = {};
    this.props.expensas.conciliacion.caja.forEach( (val, index) => {
      if (typeof val.idCategoria === 'undefined') {
        caja[index] = { "_categoriaError": 'falta seleccionar la categoria' }
      } else if (val.idCategoria == CATEGORIA.GASTO && typeof val.idSubCategoria === 'undefined') {
        caja[index] = { "_subCategoriaError": 'falta seleccionar la sub-categoria' };
      }
    })

    let recaudacion = {};
    this.props.expensas.conciliacion.recaudacion.forEach( (val, index) => {
      if (typeof val.idCategoria === 'undefined') {
        recaudacion[index] = { "_categoriaError": 'falta seleccionar la categoria' }
      } else if (val.idCategoria == CATEGORIA.GASTO && typeof val.idSubCategoria === 'undefined') {
        recaudacion[index] = { "_subCategoriaError": 'falta seleccionar la sub-categoria' };
      }
    })

    if (!_.isEmpty(banco) || !_.isEmpty(caja) || !_.isEmpty(recaudacion)) {
      let state = { "banco": banco, "caja": caja, "recaudacion": recaudacion }
      state.notValidate = true;
      this.props.setConciliacion(state);
    } else {
      this.props.setConciliacion({ notValidate: false })
      let data = [...this.props.expensas.conciliacion.banco, ...this.props.expensas.conciliacion.caja, ...this.props.expensas.conciliacion.recaudacion];
      this.props.generarPreview(data);
    }
  }


  setearBanco(id, values, which, index) {
    values["idMovimientoBanco"] = id
    this.props.setearMovimientoBanco(values)
    delete values.idMovimientoBanco
    this.setItemCombo(which, index, values)
  }

  setearCaja(id, values, which, index) {
    values["idMovimientoCaja"] = id
    this.props.setearMovimientoCaja(values)
    delete values.idMovimientoCaja
    this.setItemCombo(which, index, values)
  }

  setearRecaudacion(id, values, which, index) {
    values["idRecaudacion"] = id
    this.props.setearRecaudacion(values)
    delete values.idRecaudacion
    this.setItemCombo(which, index, values)
  }

  setItemCombo(which, index, values) {
    let state = '{"'+which+'":[]}';
    state = JSON.parse(state)
    state[which][index] = values
    this.props.setConciliacion(state);
  }

  render() {
    let getTemplate = () => {
      return (
        <div>
          <div className="col-md-12">

            <div className="box">

              <div className="box-header">
                <h3 className="box-title">Transacciones bancarias del período</h3>
              </div>
              <div className="box-body no-padding">
                <table className="table table-condensed">
                  <tbody>
                    <tr>
                      <th style={{ width: '10px' }}>fecha</th>
                      <th style={{ minWidth: '380px'}}>concepto</th>
                      <th style={{ minWidth: '80px' }}>monto</th>
                      <th style={{ minWidth: '120px' }}>comprobante Nº</th>
                      <th style={{ minWidth: '270px' }}>categoría</th>
                    </tr>
                    {this.props.expensas.conciliacion.banco.map((item, index) => ((
                      <tr key={index}>

                        <td>{dateFormat(item.data.fecha, 'dd/mm/yy')}</td>
                        <td><EditTextInline
                              id={index}
                              index={index}
                              which="banco"
                              idMovimiento={item.data.idMovimientoBanco}
                              onChange={this.setearBanco}
                              onSuccess={this.props.setearMovimientoBancoReset}
                              value={item.data.observaciones ? item.data.observaciones : item.data.concepto}
                              errorMessage={item.descBancoError}
                              loading={this.props.setearMovimientoBancoState.loading}
                              success={this.props.setearMovimientoBancoState.success}
                              error={this.props.setearMovimientoBancoState.error}
                            /></td>
                        <td>{item.data.monto}</td>
                        <td>{item.data.nroComprobante}</td>
                        <td>
                          <SelectCategoria
                            item={item}
                            index={index}
                            which="banco"
                            idMovimiento={item.data.idMovimientoBanco}
                            categorias={this.props.expensas.conciliacion.categorias}
                            subCategorias={this.props.expensas.conciliacion.subCategorias}
                            unidades={this.props.expensas.conciliacion.unidadesSinAsignar}
                            onChange={this.setearBanco}/>
                        </td>
                      </tr>
                    )))}
                  </tbody>
                </table>
              </div>
            </div>
          </div>

          <div className="col-md-12">
            <div className="box">
              <div className="box-header">
                <h3 className="box-title">Transacciones de caja</h3>
              </div>
              <div className="box-body no-padding">
                <table className="table table-condensed">
                  <tbody>
                  <tr>
                    <th style={{ width: '10px' }}>fecha</th>
                    <th style={{ minWidth: '380px'}}>concepto</th>
                    <th style={{ minWidth: '80px' }}>monto</th>
                    <th style={{ minWidth: '120px' }}>comprobante Nº</th>
                    <th style={{ minWidth: '270px' }}>categoría</th>
                  </tr>
                    {this.props.expensas.conciliacion.caja.map((item, index) => ((
                      <tr key={index}>
                        <td>{dateFormat(item.data.fecha, 'dd/mm/yy')}</td>
                        <td>{item.data.observaciones}</td>
                        <td>{item.data.monto}</td>
                        <td>{item.data.nroComprobante}</td>
                        <td>
                          <SelectCategoria
                            item={item}
                            index={index}
                            which="caja"
                            idMovimiento={item.data.idMovimientoCaja}
                            categorias={this.props.expensas.conciliacion.categorias}
                            subCategorias={this.props.expensas.conciliacion.subCategorias}
                            unidades={this.props.expensas.conciliacion.unidadesSinAsignar}
                            onChange={this.setearCaja}
                          />
                        </td>
                      </tr>
                    )))}
                  </tbody>
                </table>
              </div>
            </div>
          </div>


          <div className="col-md-12">
            <div className="box">
              <div className="box-header">
                <h3 className="box-title">Recaudaciones</h3>
              </div>
              <div className="box-body no-padding">
                <table className="table table-condensed">
                  <tbody>
                  <tr>
                    <th style={{ width: '10px' }}>fecha</th>
                    <th style={{ minWidth: '380px'}}>descripcion</th>
                    <th style={{ minWidth: '80px'}}>monto</th>
                    <th style={{ minWidth: '120px' }}>notas</th>
                    <th style={{ minWidth: '270px' }}>categoría</th>
                  </tr>
                    {this.props.expensas.conciliacion.recaudacion.map((item, index) => ((
                      <tr key={index}>
                        <td>{dateFormat(item.data.createdAt, 'dd/mm/yy')}</td>
                        <td>{item.data.descripcion}</td>
                        <td>{item.data.monto}</td>
                        <td>{item.data.cuotas > 1 ? `(cuota ${item.data.cuotaActual} de ${item.data.cuotas})` : ''}</td>
                        <td>
                          <SelectCategoria
                            item={item}
                            index={index}
                            which="recaudacion"
                            idMovimiento={item.data.idRecaudacion}
                            categorias={this.props.expensas.conciliacion.categorias}
                            subCategorias={this.props.expensas.conciliacion.subCategorias}
                            onChange={this.setearRecaudacion}
                          />
                        </td>
                      </tr>
                    )))}
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
      )
    }

    return (
      <div>
        {
          this.props.visible && this.props.expensas.conciliacion && this.props.expensas.conciliacion.recaudacion && this.props.expensas.conciliacion.unidadesSinAsignar && this.props.expensas.conciliacion.categorias
        ?  
          <div>
            { 
              this.props.pregenerarPeriodoState.loading
              ?  
                <LoadingOverlay />
              :
                getTemplate()
            }
            <div className="col-md-12">
              <div className="box box-default">
                { this.props.generarPeriodoState.success ? <SuccessOverlay /> : ''}
                {
                  this.props.expensas.conciliacion
                ?
                  <div className="box-footer">
                    <span style={{color: 'red'}}>{this.props.generarPeriodoState.error} {this.props.expensas.conciliacion.notValidate ? "falta seleccionar alguna categoria o unidad" : ""}</span>
                    <Button className="btn btn-flat btn-default-toro pull-right" loading={this.props.generarPeriodoState.loading} onClick={this.getPreview}>Generar Previsualizacion</Button>
                  </div>
                :
                  ""
                }
              </div>

            </div>
          </div>
        :
          ''
        }
      </div>
    );
  }

}