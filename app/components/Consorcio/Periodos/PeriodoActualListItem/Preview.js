import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { actionCreators } from 'actions/expensas';
import Button from 'components/shared/Button';

import LoadingOverlay from 'components/shared/LoadingOverlay';
import SuccessOverlay from 'components/shared/SuccessOverlay';

@connect(
  (store) => ({
    expensas: store.get('expensas')
  }),
  (dispatch) => ({
    setPreview: bindActionCreators(actionCreators, dispatch).setPreview,
    setTotal: bindActionCreators(actionCreators, dispatch).setTotal
  })
)
export default class Preview extends React.Component {

  static propTypes = {
    visible: React.PropTypes.bool.isRequired,
    idConsorcio: React.PropTypes.number.isRequired,
    idPeriodo: React.PropTypes.string.isRequired,
    onSuccess: React.PropTypes.func,
    generarPeriodo: React.PropTypes.func,
    generarPeriodoState: React.PropTypes.object,
    generarPeriodoReset: React.PropTypes.func,
    generarPreview: React.PropTypes.func,
    backStep: React.PropTypes.func,
    extractoData: React.PropTypes.array,
    previewData: React.PropTypes.object
  }

  constructor(props) {
    super(props);

    this.state = { 
      inputValues: { periodoTotal: ''},
      helpText: null,
      lastIndex: null,
      needUpdatePreview: false
    };
    this.saveAndProceed = this.saveAndProceed.bind(this);
    this.getPreview = this.getPreview.bind(this);
    this.onComponentValueChange = this.onComponentValueChange.bind(this);
    this.getExtractoData = this.getExtractoData.bind(this);
  }


  componentDidMount() {
    if (this.props.previewData && this.props.previewData.JSON && this.props.expensas.preview && this.props.expensas.total) {
      let periodoTotal = this.props.previewData.JSON.periodo.totalFinal;
      let diferencia = Math.round((parseFloat(periodoTotal) - parseFloat(this.props.expensas.total)) * 100) / 100;
      let val = {periodoTotal: periodoTotal, diferencia: diferencia, montoFinalCalculado: this.props.expensas.total}
      this.setState({ inputValues: _.extend(this.state.inputValues, val), helpText: '' });
    }
  }


  componentWillReceiveProps(nextProps) {
    if (nextProps.visible) {
      if (nextProps.generarPeriodoState.loading) {
        this.setState({ helpText: !nextProps.expensas.preview ? "Generando Expensa..." :  "Generando Preview..." });

      } else if (nextProps.generarPeriodoState.success) {
        this.setState({ helpText: "success"});
        if (!nextProps.expensas.preview) {
          this.props.onSuccess();
        }
      }

      if (nextProps.previewData && nextProps.expensas.preview && nextProps.previewData.JSON && !nextProps.generarPeriodoState.loading) {
        let periodoTotal = nextProps.previewData.JSON.periodo.totalFinal;
        let diferencia = 0;
        let montoFinalCalculado = 0;

        if (!this.props.visible) {
          this.props.setTotal(periodoTotal);
          montoFinalCalculado = nextProps.previewData.JSON.periodo.totalFinal;
        } else {
          montoFinalCalculado = nextProps.expensas.total;
          diferencia = Math.round((parseFloat(periodoTotal) - parseFloat(nextProps.expensas.total)) * 100) / 100;
        }

        let val = {periodoTotal: periodoTotal, diferencia: diferencia, montoFinalCalculado: montoFinalCalculado}
        this.setState({ inputValues: _.extend(this.state.inputValues, val), helpText: '' });
      }
    }
  }

  saveAndProceed(e) {
    this.props.setPreview(false)
    this.props.generarPeriodo({ idConsorcio: this.props.idConsorcio, idPeriodo: this.props.idPeriodo, data: this.props.extractoData });
  }

  getPreview(e) {
    let data = this.getExtractoData();
    this.props.generarPreview(data);
  }

  onComponentValueChange(e) {

    let val = [];
    val[e.target.id] = e.target.value;

    if (e.target.id === "diferencia") {
      val["periodoTotal"] = Math.round((parseFloat(this.props.expensas.total) + parseFloat(e.target.value)) * 100) / 100;
    } else if (e.target.id === "periodoTotal") {
      val["diferencia"] = Math.round((parseFloat(e.target.value) - parseFloat(this.props.expensas.total)) * 100) / 100;
    }

    this.setState({
     "inputValues": _.extend(this.state.inputValues, val)
    });

  }

  getExtractoData() {
    if (this.state.inputValues.diferencia !== 0) {

      let lastIndex = null;
      if (!this.state.lastIndex) {
        let lastItem = this.props.extractoData[this.props.extractoData.length-1];
        lastIndex = lastItem.index;
        lastIndex++
        this.setState({ lastIndex: lastIndex })
      } else {
        lastIndex = this.state.lastIndex;
      }

      let item =  {
        "index": lastIndex,
        "tipo": "ajuste",
        "data": {
          "idConsorcio": this.props.idConsorcio,
          "monto": parseFloat(this.state.inputValues.diferencia),
          "descripcion": "ajuste",
          "procesado": false,
          "createdAt": null,
          "updatedAt": null,
          "deleted": null
        }
      }
      let data = this.props.extractoData;
      data[lastIndex] = item;
      return data;
    } else {
      return this.props.extractoData
    }
  }

  render() {
    let needUpdatePreview = this.props.previewData.JSON && (this.props.previewData.JSON.periodo.totalFinal != this.state.inputValues.periodoTotal) ? true : false;
    return (
      <div>
      {
        this.props.visible
      ?  
        <div className="col-md-12">
          <div className="box">
          { this.props.generarPeriodoState.loading && this.props.expensas.preview ? <LoadingOverlay /> : '' }
            <div className="box-header">
              <h3 className="box-title">Preview de la expensa</h3>
            </div>
            <div className="box-body no-padding">

            <iframe name="expensa" title="expensa" id="expensa" srcDoc={this.props.previewData.HTML} style={{"width":"100%", "height":"360px"}}></iframe>                

            </div>
          </div>


          <div className="box">
            <div className="box-header">
              <h3 className="box-title">Montos Finales</h3>
            </div>
            <div className="box-body">

              <table className="table table-condensed">
                <tbody>
                  <tr>
                    <div className={`form-group ${(this.state.errorMessage ? 'has-error' : '')} col-sm-12`}>
                      <label className="col-sm-3 control-label" htmlFor="montoFinalCalculado">Monto final calculado</label>                                     
                      <div className="col-sm-9">
                        <input
                          id="montoFinalCalculado"
                          type="text"
                          name="Monto final calculado"
                          value={this.state.inputValues.montoFinalCalculado}
                          className="form-control"
                          disabled
                        />
                      </div>
                      <span className="help-block">{this.state.errorMessage}</span>
                    </div>
                  </tr>
                  <tr>
                    <div className={`form-group ${(this.state.errorMessage ? 'has-error' : '')} col-sm-12`}>
                      <label className="col-sm-3 control-label" htmlFor="diferencia">Ajuste Manual</label>                                     
                      <div className="col-sm-9">
                        <input
                          id="diferencia"
                          type="text"
                          name="Ajuste Manual"
                          onChange={this.onComponentValueChange}
                          value={this.state.inputValues.diferencia}
                          style={{color: "grey"}}
                          className="form-control"
                        />
                      </div>
                      <span className="help-block">{this.state.errorMessage}</span>
                    </div>
                  </tr>
                  <tr>
                    <div className={`form-group ${(this.state.errorMessage ? 'has-error' : '')} col-sm-12`}>
                      <label className="col-sm-3 control-label" htmlFor="periodoTotal">Monto Final</label>                                     
                      <div className="col-sm-9">
                        <input
                          id="periodoTotal"
                          type="text"
                          name="Monto Final"
                          onChange={this.onComponentValueChange}
                          className="form-control"
                          value={this.state.inputValues.periodoTotal}
                        />
                      </div>
                      <span className="help-block">{this.state.errorMessage}</span>
                    </div>
                  </tr>
                </tbody>
              </table>

            </div>    
          </div>


          <div className="box">
            <div className="col-md-12">
              <div className="box box-default">
                { this.props.generarPeriodoState.success && !this.props.expensas.preview ? <SuccessOverlay /> : '' }

                <div className="box-footer">
                {
                  this.props.generarPeriodoState.error
                  ?
                    <span style={{color: 'red'}}>{this.props.generarPeriodoState.error}</span>
                  :
                    <span>{this.state.helpText}</span>
                  
                }
                {
                  !needUpdatePreview
                ?
                  <Button className="btn btn-danger pull-right" loading={this.props.generarPeriodoState.loading && !this.props.expensas.preview} onClick={this.saveAndProceed}>Generar Expensa</Button>
                :
                  ""
                }  
                  <Button className="btn btn-flat btn-default-toro pull-right" loading={this.props.generarPeriodoState.loading && this.props.expensas.preview} onClick={this.getPreview}>Actualizar</Button>
                  

                  <Button className="btn btn-flat btn-default-toro pull-left" onClick={this.props.backStep}>
                    <span className="glyphicon glyphicon-arrow-left"></span>
                    &nbsp; &nbsp; Atras
                  </Button>
                </div>
              </div>
            </div>
          </div>

        </div>
      :
        ""
      }
      </div>
    );
  }

}