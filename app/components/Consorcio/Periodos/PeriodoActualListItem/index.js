import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Rodal from 'rodal';

import { actionCreators } from 'actions/api';
import { actionCreators as actionCreatorsExpensas} from 'actions/expensas';
import ExtractoDetail from 'components/Consorcio/Periodos/PeriodoActualListItem/ExtractoDetail';
import Preview from 'components/Consorcio/Periodos/PeriodoActualListItem/Preview';

@connect(
  (store) => ({
    pregenerarPeriodoState: store.get('api').pregenerarPeriodo || {},
    generarPeriodoState: store.get('api').generarPeriodo || {},
    listarCategoriasState: store.get('api').listarCategorias || {},
    listarUnidadesState: store.get('api').listarUnidades || {},
    layout: store.get('layout'),
    expensas: store.get('expensas')
  }),
  (dispatch) => ({
    pregenerarPeriodo: bindActionCreators(actionCreators, dispatch).pregenerarPeriodo,
    pregenerarPeriodoReset: bindActionCreators(actionCreators, dispatch).pregenerarPeriodoReset,
    generarPeriodo: bindActionCreators(actionCreators, dispatch).generarPeriodo,
    generarPeriodoReset: bindActionCreators(actionCreators, dispatch).generarPeriodoReset,
    listarCategorias: bindActionCreators(actionCreators, dispatch).listarCategorias,
    listarUnidades: bindActionCreators(actionCreators, dispatch).listarUnidades,
    setPeriodo: bindActionCreators(actionCreatorsExpensas, dispatch).setPeriodo,
    setStep: bindActionCreators(actionCreatorsExpensas, dispatch).setStep,
    setPreview: bindActionCreators(actionCreatorsExpensas, dispatch).setPreview,
    reset: bindActionCreators(actionCreatorsExpensas, dispatch).reset
  })
)
export default class PeriodoActualListItem extends React.Component {

  static propTypes = {
    item: React.PropTypes.object,
    data: React.PropTypes.object
  }

  constructor(props) {
    super(props);

    this.idConsorcio = this.props.layout.consorcio.idConsorcio

    this.state = {
      visible: false,
      loading: false,
      extractoData: [],
      previewData: {},
      step: 1
    };
    this.onSuccess = this.onSuccess.bind(this);
    this.generarPreview = this.generarPreview.bind(this);
    this.backStep = this.backStep.bind(this);
    this.nextStep = this.nextStep.bind(this);
  }

  show() {
    this.setState({ visible: true });
  }

  hide() {
    this.setState({ visible: false });
  }

  componentWillMount() {
    if (!this.props.layout.categorias) {
      this.props.listarCategorias();
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.state.visible) {
      if (nextProps.generarPeriodoState.success && nextProps.generarPeriodoState.data && nextProps.expensas.preview && (_.isEmpty(this.state.previewData) || nextProps.generarPeriodoState.data !== this.props.generarPeriodoState.data)) {
        this.setState({ previewData: nextProps.generarPeriodoState.data });
      }
      if (nextProps.expensas.step && nextProps.expensas.step !== this.state.step) {
        this.setState({ step: nextProps.expensas.step });
      }
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.visible) {

      if (!this.props.expensas.idPeriodo) {
        this.props.setPeriodo(prevProps.item.idPeriodo);
      } else if (!prevProps.expensas.idPeriodo && this.props.expensas.idPeriodo || prevProps.expensas.idPeriodo !== this.props.expensas.idPeriodo) {
        if (!this.props.layout.categorias) {
          this.props.listarCategorias();
        }
        if (!this.props.layout.unidades) {
          this.props.listarUnidades({ idConsorcio: this.idConsorcio });
        }
      }

    }
  }


  componentWillUpdate(nextProps, nextState) {
    if (!this.state.visible && nextState.visible) {
      if (!this.props.expensas.idPeriodo) {
        this.props.setPeriodo(this.props.item.idPeriodo);
      } else if (this.props.expensas.idPeriodo !== this.props.item.idPeriodo) {
        this.props.reset();
      }
    }
  }

  generarPreview(data) {
    this.setState({ extractoData: data });
    this.props.generarPeriodo({ idConsorcio: this.idConsorcio, idPeriodo: this.props.item.idPeriodo, preview: true, data: data});
  }

  backStep() {
    this.props.setStep(1);
  }

  nextStep() {
    this.props.setStep(2);
  }

  onSuccess() {
    setTimeout(() => {
      this.hide();
      // this.props.reset()
      this.props.generarPeriodoReset();
      this.props.onSuccess();
    }, 2000);    
  }


  render() {
    // let reset = this.props.generarPeriodoState && this.props.generarPeriodoState.success && !this.props.expensas.preview ? true : false;
    return (
      <div>

        <ul className="action-buttons">
          <li>
            <button onClick={this.show.bind(this)} className="btn btn-default-toro flat">
              {
              this.state.loading == true
              ?<i className="fa fa-refresh fa-spin"></i>
              :<i className="fa fa-calculator"></i>
              }
              <span>Generar Expensa</span>
            </button>
          </li>
        </ul>

        <Rodal visible={this.state.visible} onClose={this.hide.bind(this)} width={90} measure="%" customStyles={{height:"auto", "overflow":"scroll",
    margin: "20px auto", "maxWidth": "1100px"}}>
          {
            this.state.visible
          ?
            <div>
              <br />
              <div>
                <ExtractoDetail
                  visible={this.state.step == 1}
                  idConsorcio={this.idConsorcio}
                  idPeriodo={this.props.item.idPeriodo}
                  reset={this.props.reset}
                  nextStep={this.nextStep}
                  pregenerarPeriodo={this.props.pregenerarPeriodo}
                  pregenerarPeriodoReset={this.props.pregenerarPeriodoReset}
                  pregenerarPeriodoState={this.props.pregenerarPeriodoState}
                  generarPreview={this.generarPreview}
                  generarPeriodoState={this.props.generarPeriodoState}
                  generarPeriodoReset={this.props.generarPeriodoReset}
                />

                <Preview
                  visible={this.state.step == 2}
                  idConsorcio={this.idConsorcio }
                  idPeriodo={this.props.item.idPeriodo}
                  onSuccess={this.onSuccess}
                  backStep={this.backStep}
                  generarPeriodo={this.props.generarPeriodo}
                  generarPeriodoState={this.props.generarPeriodoState}
                  generarPeriodoReset={this.props.generarPeriodoReset}
                  generarPreview={this.generarPreview}
                  extractoData={this.state.extractoData}
                  previewData={this.state.previewData}
                />

              </div>
            </div>  
          :
            ""
          }
        </Rodal>
      </div>

    );
  }
}
