import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import JsonTable from 'react-json-table';

import { actionCreators } from 'actions/api';
import PeriodoActualListItem from 'components/Consorcio/Periodos/PeriodoActualListItem';
import PeriodoListItem from 'components/Consorcio/Periodos/PeriodoListItem';
import LoadingOverlay from 'components/shared/LoadingOverlay';

const PERIODO_ESTADO = {
  PENDIENTE : "PENDIENTE",
  GENERADO: "GENERADO"
}

let settings = {
  "noRowsMessage": <LoadingOverlay style={{bottom: 0, top: 0, left: 0, right: 0, width: 1000, maxWidth: "100%", marginBottom: 20}} />,
  "cellClass": (currentClass, columnKey, rowData )=>( columnKey === "actions" ? "action-column" : "" ),
  "headerClass": ( currentClass, columnKey )=>( "ttt" )
}

@connect(
  (store) => ({
    obtenerPeriodosState: store.get('api').obtenerPeriodos || {loading: true},
    descargarExpensaState: store.get('api').descargarArchivo || {},
    layout: store.get('layout') || {}
  }),
  (dispatch) => ({
    obtenerPeriodos: bindActionCreators(actionCreators, dispatch).obtenerPeriodos,
    descargarExpensa: bindActionCreators(actionCreators, dispatch).descargarArchivo
  })
)
export default class Periodos extends React.Component {
  
  static propTypes = {
    layout: React.PropTypes.object,
    obtenerPeriodos: React.PropTypes.func,
    descargarExpensa: React.PropTypes.func
  }

  constructor(props) {
    super(props);
    this.state = {
      items : [],
      columns : [],
      archivosEnMemoria : [],
      uuidRetrieved: null
    };
    this.periodoRow = this.periodoRow.bind(this);
    this.buildTable = this.buildTable.bind(this);
    this.guardarBlobURL = this.guardarBlobURL.bind(this);
    this.onSuccess = this.onSuccess.bind(this);
    this.onClickAction = this.onClickAction.bind(this);
  }

  componentWillMount() {
    if (!_.isEmpty(this.props.obtenerPeriodosState.data)) {
      this.buildTable(this.props.obtenerPeriodosState.data)
    }
    else if (this.props.layout.consorcio) {
      this.props.obtenerPeriodos({ idConsorcio: this.props.layout.consorcio.idConsorcio });
    }
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.obtenerPeriodosState.data && _.isEmpty(this.props.obtenerPeriodosState.data) || (nextProps.obtenerPeriodosState.data !== this.props.obtenerPeriodosState.data) &&  !_.isEmpty(nextProps.obtenerPeriodosState.data) ) {
      var sortedData = nextProps.obtenerPeriodosState.data.reverse();
      this.buildTable(sortedData);
    }

    if(typeof this.state.archivosEnMemoria[this.state.uuidRetrieved] === 'undefined') {
      if(typeof nextProps.descargarExpensaState.data != 'undefined') {
        var blobURL = window.URL.createObjectURL(nextProps.descargarExpensaState.data);
        this.guardarBlobURL(this.state.uuidRetrieved, blobURL);
      }
    }
  }

  onSuccess() {
    this.props.obtenerPeriodos({ idConsorcio: this.props.layout.consorcio.idConsorcio});
  }

  guardarBlobURL(uuid, blobURL) {
    var values = this.state.archivosEnMemoria ;
    values[uuid] = blobURL;
    this.setState({ archivosEnMemoria: values });
  }

  onClickAction(item){
    this.setState({ uuidRetrieved: item.archivo.info.uuid })
    this.props.descargarExpensa({"uuid": item.archivo.info.uuid});
  }

  periodoRow(item) {
    if (item.estado === PERIODO_ESTADO.GENERADO && typeof item.archivo.info == 'undefined' ) {
      // console.error("item.archivo debe tener uuid", item)
    }
    if (item.estado === PERIODO_ESTADO.PENDIENTE) {
      return <PeriodoActualListItem item={item} onSuccess={this.onSuccess}/>
    } else if (item.estado === PERIODO_ESTADO.GENERADO && item.archivo.info) {
      let data = {...this.props}
      return <PeriodoListItem item={item} archivosEnMemoria={this.state.archivosEnMemoria[item.archivo.info.uuid]} onClick={this.onClickAction} />
    }
  }

  buildTable(data) {

    let value = [];
    _.map( data, (val, key) => {
      value[key] = {
        "idPeriodo": val.idPeriodo,
        "estado": val.estado,
        "archivo": {
          "info": val.archivo,
          "data": ""
        },
        "actions": "" };
    });

    this.setState({
      "items": value,
      "columns": [
        {key: 'idPeriodo', label: 'Periodo'},
        {
          key: 'actions',
          label: 'Acciones'
        }
      ]}
    );
  }

  render() {
    let items = {
      ...this.state
    }
    if (items.columns.length > 1) {
      items.columns[1].cell = ( item, columnKey ) => {
        return this.periodoRow(item);
      }
    }

    
    return (
      <div>
        <div className="box">
          <JsonTable rows={ items.items } columns={ items.columns} className="table table-bordered table-hover dataTable" settings={settings} classPrefix="" />
        </div>
      </div>
    );
  }
}
