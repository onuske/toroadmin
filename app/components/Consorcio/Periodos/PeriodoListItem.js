import React from 'react';
import Rodal from 'rodal';

export default class PeriodoListItem extends React.Component {

	static propTypes = {
		item: React.PropTypes.object,
		onClick: React.PropTypes.func,
		archivosEnMemoria: React.PropTypes.string
	}

	constructor(props) {
		super(props)
		this.state = {loading: false, action: null, shouldUpdate: false}
		this.onClickDescargar = this.onClickDescargar.bind(this);
		this.onClickVerExpensa = this.onClickVerExpensa.bind(this);
		this.forzarDescarga = this.forzarDescarga.bind(this);
	}

	show() {
    this.setState({ visible: true });
  }

  hide() {
    this.setState({ visible: false });
  }

	onClickDescargar() {
		if(typeof this.props.archivosEnMemoria === 'undefined'){
			this.setState({action: "descargar", loading: true, shouldUpdate: true});
			this.props.onClick(this.props.item);
		}else{
			this.forzarDescarga(this.props.archivosEnMemoria, this.props.item.archivo.info.nombre);
		}
  }

  onClickVerExpensa() {
		if(typeof this.props.archivosEnMemoria === 'undefined'){
	  	this.setState({action: "verExpensa", loading: true, shouldUpdate: true})
			this.props.onClick(this.props.item);
		}else{
	  	this.show()
		}
  }

  forzarDescarga(blobURL, fileName) {
    var tempLink = document.createElement('a');
    tempLink.href = blobURL;
    tempLink.setAttribute('download', fileName);
    tempLink.setAttribute('target', '_blank');
    document.body.appendChild(tempLink);
    tempLink.click();
    document.body.removeChild(tempLink);
  }

  componentWillReceiveProps(nextProps) {
		if(this.state.shouldUpdate){
  		if(typeof this.props.archivosEnMemoria === 'undefined'){
  			this.setState({loading: false, shouldUpdate: false});
	    	if(this.state.action === "verExpensa"){
			  	this.show()
	    	}else if(this.state.action === "descargar"){
					this.forzarDescarga(nextProps.archivosEnMemoria, this.props.item.archivo.info.nombre);
		    }
  		}
	  }
  }

	render() {
		return (
			<div>
				<ul className="action-buttons">
          <li>
					<button onClick={this.onClickVerExpensa} className="btn btn-default-toro flat">
						{
						 this.state.loading == true && this.state.action === "verExpensa"
						?
              <i className="fa fa-refresh fa-spin"></i>
						:
              <i className="glyphicon glyphicon-eye-open"></i>
						}
						<span>Ver Expensa</span>
          </button>
					</li>

          <li>
					<button onClick={this.onClickDescargar} className="btn btn-default-toro flat">
						{
						  this.state.loading == true && this.state.action === "descargar"
						?
              <i className="fa fa-refresh fa-spin"></i>
						:
              <i className="glyphicon glyphicon-download-alt"></i>
						}
						<span>Descargar</span>
          </button>
					</li>

				</ul>

      	<Rodal visible={this.state.visible} onClose={this.hide.bind(this)} width={820} measure="px" customStyles={{height:"auto", "overflow":"scroll", "margin":"20px auto"}}>
          {
            this.state.visible
          ?
            (
              <div>

                <object data={this.props.archivosEnMemoria} type='application/pdf' style={{"width":"780px", "height":"600px"}} ></object>

              </div>
            )
          :
            ""
          }
      	</Rodal>
      </div>
		);
	}
}
