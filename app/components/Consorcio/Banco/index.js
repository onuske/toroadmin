import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import JsonTable from 'react-json-table';
import dateFormat from 'dateformat';

import { actionCreators } from 'actions/api';
import UploadFile from './UploadFile';
import ToroBrand from 'components/shared/ToroBrand';
import LoadingOverlay from 'components/shared/LoadingOverlay';
import SelectCategoria from 'components/shared/SelectCategoria';

let settings = {
  "noRowsMessage": <LoadingOverlay style={{bottom: 0, top: 0, left: 0, right: 0, width: 1000, maxWidth: "100%", marginBottom: 20}} />,
  "cellClass": (currentClass, columnKey, rowData )=>( columnKey === "actions" ? "action-column" : "" ),
  "headerClass": ( currentClass, columnKey )=>( "ttt" )
}

@connect(
  (store) => ({
    obtenerMovimientosBancoState: store.get('api').obtenerMovimientosBanco || {loading: true},
    listarCuentasState: store.get('api').listarCuentas || {},
    listarCategoriasState: store.get('api').listarCategorias || {},
    listarUnidadesState: store.get('api').listarUnidades || {},
    setearMovimientoBancoState: store.get('api').setearMovimientoBanco || {},
    layout: store.get('layout') || {}
  }),
  (dispatch) => ({
    obtenerMovimientosBanco: bindActionCreators(actionCreators, dispatch).obtenerMovimientosBanco,
    listarCuentas: bindActionCreators(actionCreators, dispatch).listarCuentas,
    listarCategorias: bindActionCreators(actionCreators, dispatch).listarCategorias,
    listarUnidades: bindActionCreators(actionCreators, dispatch).listarUnidades,
    setearMovimientoBanco: bindActionCreators(actionCreators, dispatch).setearMovimientoBanco
  })
)
export default class Banco extends React.Component {

  static propTypes = {
    layout: React.PropTypes.object,
    obtenerMovimientosBanco: React.PropTypes.func
  }

  constructor(props) {
    super(props);
    this.state = {
      items: [],
      columns: [],
      inputValues: [],
      categorias: null,
      subCategorias: null,
      unidades: null
    };
    this.onSuccess = this.onSuccess.bind(this);
    this.handleOnChange = this.handleOnChange.bind(this);
    this.actionsRow = this.actionsRow.bind(this);
    this.buildTable = this.buildTable.bind(this);
    this.fillCategoriasOptions = this.fillCategoriasOptions.bind(this);
    this.fillSubCategoriasOptions = this.fillSubCategoriasOptions.bind(this);
    this.fillUnidadesOptions = this.fillUnidadesOptions.bind(this);
    this.setearBanco = this.setearBanco.bind(this);
  }

  componentWillMount() {
    if (this.props.obtenerMovimientosBancoState.data) {
      this.buildTable(this.props.obtenerMovimientosBancoState.data)
    }

    if (!this.props.listarCuentasState.data && !this.props.listarCuentasState.loading && this.state.layout && this.state.layout.consorcio) {
      this.props.listarCuentas({ idConsorcio: this.props.layout.consorcio.idConsorcio })
    } else if (this.props.listarCuentasState.success && this.props.listarCuentasState.data) {
      let choices = []
      this.props.listarCuentasState.data.map((cuenta, index) => {
        choices[cuenta.nombreBanco] = true;
      })
      this.setState({ inputValues: choices })
    }

    if (!this.props.layout.categorias) {
      this.props.listarCategorias();
    } else {
      this.fillCategoriasOptions(this.props.layout.categorias);
      let subCat = _.filter(this.props.layout.categorias, (val) => (typeof val.subCategorias !== 'undefined'));
      this.fillSubCategoriasOptions(subCat[0].subCategorias);
    }

    if (this.props.layout.unidades) {
      this.fillUnidadesOptions(this.props.layout.unidades);
    }
  }

  componentWillReceiveProps(nextProps) {

    if (nextProps.layout) {

      if (_.isEmpty(this.state.inputValues) && nextProps.layout.consorcio) {
        if (nextProps.listarCuentasState.error) {
          console.error("ERROR", nextProps.listarCuentasState.error)
        } else if (!nextProps.listarCuentasState.data && !nextProps.listarCuentasState.loading) {
          this.props.listarCuentas({ idConsorcio: nextProps.layout.consorcio.idConsorcio })
        } else if (nextProps.listarCuentasState.success && nextProps.listarCuentasState.data) {
          let choices = []
          nextProps.listarCuentasState.data.map((cuenta, index) => {
            choices[cuenta.nombreBanco] = true;
          })
          this.setState({ inputValues: choices })
        }
      }

      if (_.isEmpty(this.state.categorias) && nextProps.layout.categorias) {
        this.fillCategoriasOptions(nextProps.layout.categorias);
        var subCat = _.filter(nextProps.layout.categorias, (val) => (typeof val.subCategorias !== 'undefined'));
        this.fillSubCategoriasOptions(subCat[0].subCategorias);
      }

      if (_.isEmpty(this.state.unidades) && nextProps.layout.unidades) {
        this.fillUnidadesOptions(nextProps.layout.unidades);
      }
    }

    if (nextProps.obtenerMovimientosBancoState.loading && !nextProps.obtenerMovimientosBancoState.success) {
      this.setState({ items: [], columns: [] });
    } else if (nextProps.obtenerMovimientosBancoState.success && nextProps.obtenerMovimientosBancoState.data) {
      this.buildTable(nextProps.obtenerMovimientosBancoState.data);
    }

  }

  componentDidUpdate(prevProps, prevState) {
    if (!this.props.layout.unidades && !prevProps.layout.consorcio && this.props.layout.consorcio ) {
      this.props.listarUnidades({ idConsorcio: this.props.layout.consorcio.idConsorcio });
    }
  }

  setearBanco(id, value) {
    value["idMovimientoBanco"] = id
    this.props.setearMovimientoBanco(value)
  }

  onSuccess() {
    this.props.obtenerMovimientosBanco({ idConsorcio: this.props.layout.consorcio.idConsorcio, idPeriodo: this.props.layout.consorcioSelected.idPeriodoSelected })
  }

  fillCategoriasOptions(data) {
    this.setState({
      categorias: data.map((cat) => ({ label: cat.nombre, value: cat.idCategoria }))
    })
  }

  fillSubCategoriasOptions(data) {
    this.setState({
      subCategorias: data.map((cat) => ({ label: cat.nombre, value: cat.idSubCategoria }))
    })
  }

  fillUnidadesOptions(data) {
    this.setState({
      unidades: data.map((unidad) => ({ label: unidad.unidad, value: unidad.idUnidad }))
    })
  }

  handleOnChange(e) {
    let target = e.target;
    let value = [];

    if (target.type === undefined) {
      value[target.firstChild.id] = target.firstChild.checked ? false : true;
    } else if (target.type === 'checkbox') {
      value[target.id] = target.checked;
    }

    this.setState({
     "inputValues": _.extend(this.state.inputValues, value)
    });
  }

  buildTable(data) {
    // { "idMovimientoBanco": 4480, "idBancosCuenta": 1, "idExtracto": 299, "fecha": "2017-06-19T03:00:00.000Z", "monto": "1900.00", "saldo": "23192.92", "nroComprobante": null, "concepto": "DEP. EFECTIVO ATM LINK   ", "codigo": null, "observaciones": null, "rawData": ",19/06/2017,DEP. EFECTIVO ATM LINK   ,,0,1900,23192.92,,", "procesado": false, "createdAt": "2017-11-17T00:54:00.775Z", "updatedAt": "2017-11-17T00:54:00.775Z", "deleted": null, "idUnidad": null, "idCategoria": null, "idSubCategoria": null }
    if (_.isEmpty(data)) {
      data = [{ "idMovimientoBanco": null, "idBancosCuenta": null, "idExtracto": null, "fecha": null, "monto": null, "saldo": null, "nroComprobante": null, "concepto": null, "codigo": null, "observaciones": null, "rawData": null, "procesado": null, "createdAt": null, "updatedAt": null, "deleted": null, "idUnidad": null, "idCategoria": null, "idSubCategoria": null }];
    }

    let value = [];
    _.map( data, (val, key) => {
      value[key] = {
        "fecha": !_.isEmpty(val.fecha) ? dateFormat(val.fecha, "yyyy-mm-dd") : null,
        "monto": val.monto ? `$${parseFloat(val.monto).formatMoney(2, ',', '.')}` : "",
        "saldo": val.saldo ? `$${parseFloat(val.saldo).formatMoney(2, ',', '.')}` : "",
        "descripcion": val.concepto ? val.concepto.slice(0, 15) : "",
        "estado": val.procesado !== null ? (val.procesado == true ? "procesado" : "sin procesar") : "",
        "detail": {
          "idMovimiento": val.idMovimientoBanco,
          "banco": val.idBancosCuenta,
          "categoria": val.idCategoria,
          "subCategoria": val.idSubCategoria,
          "unidad": val.idUnidad,
          "procesado": val.procesado
          },
        };
    });

    this.setState({
      "items": value,
      "columns": [
        {key: 'fecha', label: 'Fecha'},
        {key: 'descripcion', label: 'Descripcion'},
        {key: 'monto', label: 'Monto'},
        {key: 'saldo', label: 'Saldo'},
        {key: 'estado', label: 'Estado'},
        {key: 'actions', label: 'Categoría'}
      ]
    });
  }

  actionsRow(item) {
    return <SelectCategoria item={item} categorias={this.state.categorias} subCategorias={this.state.subCategorias} unidades={this.state.unidades} onChange={this.setearBanco}/>
  }

  render() {
    let state = {
      ...this.state
    }

    let bancos = []
    if (this.props.listarCuentasState.data ) {

      bancos = this.props.listarCuentasState.data.map((val, key) => ( { nombre: val.nombreBanco, cuenta: val.idBancosCuenta }))
      
      if (this.props.listarCuentasState.data.length > 1) {
        if (state.items.length > 1) {
          let filters = this.props.listarCuentasState.data.map((val, key) => {
            if (this.state.inputValues[val.nombreBanco]) {
              return val.idBancosCuenta
            }
          })

          filters = _.compact(filters)
          if (_.isEmpty(filters)) {
            state.items = [{ "idMovimientoBanco": null, "idBancosCuenta": null, "idExtracto": null, "fecha": null, "monto": null, "saldo": null, "nroComprobante": null, "concepto": null, "codigo": null, "observaciones": null, "rawData": null, "procesado": null, "createdAt": null, "updatedAt": null, "deleted": null, "idUnidad": null, "idCategoria": null, "idSubCategoria": null }];
          } else {
            state.items = _.filter(state.items, (mov) => { return filters.includes(mov.detail.banco) });
          }
        }
      }
    }

    if (!_.isEmpty(state.items)) {
      state.columns[5].cell = ( item, columnKey ) => {
        return item.descripcion ? this.actionsRow(item) : null;
      }
    }
    return (
      <div>
        <div className="buscador_filtros">
          <div className="row">
            <div className="col-md-8">
              <div className="col-md-5">
                {this.props.children}
              </div>
              {
                bancos.length > 1
              ?
                <div className="col-md-7">
                  {bancos.map((item, index) => ((
                    <div className="input-group checkbox-inline" style={{"color": "black"}} key={item.nombre} onClick={this.handleOnChange}>
                      <input type="checkbox" id={item.nombre} onChange={this.handleOnChange} checked={this.state.inputValues[item.nombre]} />
                      <label className="control-label">{item.nombre}</label>
                    </div>
                  )))}
                </div>
              :
                <div className="col-md-7">&nbsp;</div>
              }
            </div>
            <div className="col-md-4">
              <UploadFile onSuccess={this.onSuccess} textBtn="Subir Extracto Bancario" cuentasBancos={bancos} />
            </div>
          </div>
        </div>

        <div className="box-header">
          <h3 className="box-title">Movimientos bancarios del período</h3>
        </div>

        <div className="row">
          <JsonTable rows={state.items} columns={state.columns} className="table table-bordered table-hover dataTable" settings={settings} classPrefix="" />
        </div>
      </div>
    );
  }
}

