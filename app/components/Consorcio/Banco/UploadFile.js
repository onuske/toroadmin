import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Rodal from 'rodal';

import { actionCreators } from 'actions/api';
import LoadingOverlay from 'components/shared/LoadingOverlay';
import SuccessOverlay from 'components/shared/SuccessOverlay';
import InputFile from 'components/shared/InputFile';
import InputReactSelect from 'components/shared/InputReactSelect';

@connect(
  (store) => ({
    subirArchivoState: store.get('api').subirArchivo || {},
    generarExtractoState: store.get('api').generarExtracto || {},
    layout: store.get('layout') || {}
  }),
  (dispatch) => ({
    subirArchivo: bindActionCreators(actionCreators, dispatch).subirArchivo,
    generarExtracto: bindActionCreators(actionCreators, dispatch).generarExtracto,
    subirArchivoReset: bindActionCreators(actionCreators, dispatch).subirArchivoReset,
    generarExtractoReset: bindActionCreators(actionCreators, dispatch).generarExtractoReset
  })
)
export default class UploadFile extends Component {

  static propTypes = {
    textBtn: React.PropTypes.string.isRequired,
    cuentasBancos: React.PropTypes.array.isRequired,
    generarExtracto: React.PropTypes.func,
    generarExtractoState: React.PropTypes.object
  }

  constructor(props, context) {
    super(props, context);
    this.handleChange = this.handleChange.bind(this);
    this.validateBanco = this.validateBanco.bind(this);
    this.setItemCombo = this.setItemCombo.bind(this);
    this.getInitialValues = this.getInitialValues.bind(this);
    this.state = this.getInitialValues();
  }

  getInitialValues() {
    return  {
     success: false,
     error: null,
     reset: false,
     loading: false,
     helpText: "haga click en el boton para subir un archivo",
     cuentasBancos: this.props.cuentasBancos.map((banco) => ({label:banco.nombre, value:banco.nombre})),
     cuentaBanco: this.props.cuentasBancos.length == 1 ? this.props.cuentasBancos[0].nombre : null,
     buttonDisabled: this.props.cuentasBancos.length > 1,
     hideSelect: this.props.cuentasBancos.length == 1 ? true : false

    }
  }

  show() {
    this.setState({ visible: true });
    this.setState(this.getInitialValues());
  }

  hide() {
    this.setState({ visible: false });
    if(this.props.generarExtractoState.data) {
      this.props.onSuccess();
    }
  }

  handleChange(e) {
    this.props.subirArchivo(e.target.files[0])
    this.setState({ loading: true });
  }

  validateBanco(e) {
    if (this.state.cuentaBanco == null) {
      var val = [];
      val["cuentaBancoError"] = "seleccione el banco antes de subir el extracto";
      this.setState(_.extend(this.state, val));
    }
    return false
  }

  setItemCombo(index, value) {
    var val = [];
    val[index] = value;
    val[`${index}Error`] = '';
    if (!_.isEmpty(value)) val["buttonDisabled"] = false
    this.setState(_.extend(this.state, val));
  }

  componentWillReceiveProps(nextProps) {
    if (!this.state.cuentaBanco) {
      if (this.props.cuentasBancos.length == 1) {
        var val = [];
        val["cuentaBanco"] = this.props.cuentasBancos[0].nombre;
        val["cuentaBancoError"] = '';
        this.setState(_.extend(this.state, val));
      } else {
        this.setState(_.extend(this.state, { buttonDisabled: false }));
      }
    }

    if (nextProps.subirArchivoState.loading && !nextProps.subirArchivoState.success) {
      this.setState({ helpText: "subiendo archivo..." });
    }
    else if (nextProps.subirArchivoState.success && nextProps.subirArchivoState.data && nextProps.subirArchivoState.data !== this.props.subirArchivoState.data) {
      this.setState({ helpText: "archivo subido" })
      this.props.generarExtracto({ idConsorcio: this.props.layout.consorcio ? this.props.layout.consorcio.idConsorcio : nextProps.layout.consorcio.idConsorcio, idArchivo: nextProps.subirArchivoState.data.data.idArchivo, nombreBanco: this.state.cuentaBanco })
      this.props.subirArchivoReset();
    }
    else if(nextProps.subirArchivoState.error) {
      this.setState({ reset: true, loading: false });
    }

    if (nextProps.generarExtractoState.loading && !nextProps.generarExtractoState.success) {
      this.setState({ helpText: "generando extracto.." });
    }
    else if (nextProps.generarExtractoState.success &&  nextProps.generarExtractoState.data) {
      this.setState({ success: true, reset: true, loading: false, helpText: `se generaron ${nextProps.generarExtractoState.data.registrosInsertados} registros` });
      setTimeout(() => {
        this.hide()
        this.props.generarExtractoReset();
      }, 5000)
    }
    else if(nextProps.generarExtractoState.error) {
      this.setState({ reset: true, loading: false });
    }
  }

  render() {
    return (
      <div>
        <button className="btn btn-flat btn-default-toro file-input pull-right" onClick={this.show.bind(this)}>
          {this.props.textBtn}
        </button>

        <Rodal visible={this.state.visible} onClose={this.hide.bind(this)} customStyles={{"margin":"auto"}}>
          <div className="box box-primary" style={{padding: "20px"}}>
            { this.state.loading ? <LoadingOverlay /> : ''}
            { this.state.success ? <SuccessOverlay /> : ''}
            <div className="box-header with-border">
              <h3 className="box-title">Subir Extracto Bancario</h3>
            </div>

            {
              !this.state.hideSelect
            ?
              <InputReactSelect
                id="cuentaBanco"
                name="Banco"
                onChange={this.setItemCombo}
                value={this.state.cuentaBanco}
                options={this.state.cuentasBancos}
                errorMessage={this.state.cuentaBancoError}
                clearable={false}
                mandatory={this.state.cuentasBancos.length > 1}
                inputStyle="inline"
                widthLabel="4"
              />
            :
              ""  
            }
            <br />
            <div className={`form-group ${(this.props.errorMessage ? 'has-error' : '')}`}>

              <button className="btn btn-flat btn-default-toro file-input" onClick={this.validateBanco}>
                <InputFile
                  ref="inputFile"
                  name="file"
                  accept=".xls"
                  placeholder="seleccionar archivo"
                  className="input-class"
                  onChange={this.handleChange}
                  reset={this.state.reset}
                  disabled={this.state.buttonDisabled}
                />
              </button>
            </div>

          </div>
          <br />
          <p className="help-block">{this.state.helpText}</p>

        </Rodal>  
      </div>
    );
  }

}
