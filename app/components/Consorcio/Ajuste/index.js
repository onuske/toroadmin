import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import JsonTable from 'react-json-table';
import dateFormat from 'dateformat';

import { actionCreators } from 'actions/api';
import CreateAjuste from './CreateAjuste';
import LoadingOverlay from 'components/shared/LoadingOverlay';

let settings = {
  "noRowsMessage": <LoadingOverlay style={{bottom: 0, top: 0, left: 0, right: 0, width: 1000, maxWidth: "100%", marginBottom: 20}} />,
  "cellClass": (currentClass, columnKey, rowData )=>( columnKey === "actions" ? "action-column" : "" ),
  "headerClass": ( currentClass, columnKey )=>( "ttt" )
}

@connect(
  (store) => ({
    obtenerAjustesState: store.get('api').obtenerAjustes || {},
    layout: store.get('layout') || {}
  }),
  (dispatch) => ({
    obtenerAjustes: bindActionCreators(actionCreators, dispatch).obtenerAjustes,
    listarCategorias: bindActionCreators(actionCreators, dispatch).listarCategorias
  })
)
export default class Ajuste extends React.Component {

  static propTypes = {
    layout: React.PropTypes.object,
    obtenerAjustes: React.PropTypes.func
  }

  constructor(props) {
    super(props);
    this.state = {
      items: [],
      columns: []
    };
    this.actionsRow = this.actionsRow.bind(this);
    this.buildTable = this.buildTable.bind(this);
    this.onSuccess = this.onSuccess.bind(this);
  }

  componentDidMount() {
    if (!this.props.obtenerAjustesState.data) {
      this.props.obtenerAjustes({ idConsorcio: this.props.layout.consorcio.idConsorcio })
    } else if (this.props.obtenerAjustesState.data) {
      this.buildTable(this.props.obtenerAjustesState.data)
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.layout.consorcio) {
      if (nextProps.obtenerAjustesState.loading && !nextProps.obtenerAjustesState.success) {
        this.setState({ items: [], columns: [] });
      } else if (nextProps.obtenerAjustesState.data && nextProps.obtenerAjustesState.success) {
        this.buildTable(nextProps.obtenerAjustesState.data);
      }
    }
  }

  onSuccess() {
    this.props.obtenerAjustes({ idConsorcio: this.props.layout.consorcio.idConsorcio,  idPeriodo: this.props.layout.consorcioSelected.idPeriodoSelected })
  }

  buildTable(data) {
    if (data.length < 1) {
      data = [[{"idAjuste":null,"idUnidad":null,"descripcion":null,"monto":null,"fecha":null,"procesado":null,"createdAt":null,"updatedAt":null,"deleted":null}]]
    }

    let value = [];
    _.map( data, (val, key) => {
      value[key] = {
        "idUnidad": val.idUnidad,
        "fecha": !_.isEmpty(val.fecha) ? dateFormat(val.fecha, "dd-mm-yyyy") : null,
        "descripcion": val.descripcion ? val.descripcion.slice(0, 15) : "",
        "monto": val.monto ? `$${parseFloat(val.monto).formatMoney(2, ',', '.')}` : "",
        "estado": val.procesado ? (val.procesado == true ? "procesado" : "sin procesar") : "",
        "detail": {
          "idAjuste": val.idAjuste
          },
        };
    });

    this.setState({
      "items": value,
      "columns": [
        {key: 'idUnidad', label: 'Unidad'},
        {key: 'fecha', label: 'Fecha'},
        {key: 'descripcion', label: 'Descripcion'},
        {key: 'monto', label: 'Monto'},
        {key: 'estado', label: 'Estado'},
        {key: 'actions', label: 'Acciones'}
      ]
    });
  }

  actionsRow(item) {
    // return <RecaudacionListItem item={item} AjustesEnMemoria={this.state.AjustesEnMemoria[item.detail.uuid]} onClick={this.onClickAction}/>
    return "";
  }

  render() {
    let state = {
      ...this.state
    }
    if (!_.isEmpty(state.items)) {
      state.columns[5].cell = ( item, columnKey ) => {
        return item.descripcion ? this.actionsRow(item) : null;
      }
    }
    return (
      <div>
        <div className="buscador_filtros">
          <div className="row">
            <div className="col-md-8">
            {
              this.props.layout.consorcio
            ?
              <div className="col-sm-3">
                {this.props.children}
              </div>
            :
              ""
            }
            </div>
            <div className="col-md-3">
              <CreateAjuste onSuccess={this.onSuccess} textBtn="Crear Ajuste"/>
            </div>
          </div>
        </div>

        <div className="box-header">
          <h3 className="box-title">Ajustes del período</h3>
        </div>

        <div className="row">
          <JsonTable rows={state.items} columns={state.columns} className="table table-bordered table-hover dataTable" settings={settings} classPrefix="" />
        </div>

      </div>
    );
  }
}