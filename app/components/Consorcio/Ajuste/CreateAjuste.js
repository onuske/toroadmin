import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Rodal from 'rodal';

import { actionCreators } from 'actions/api';
import SimpleFormWrap from 'components/shared/SimpleFormWrap';
import InputText from 'components/shared/InputText';
import InputChoice from 'components/shared/InputChoice';
import InputReactSelect from 'components/shared/InputReactSelect';
import InputDate from 'components/shared/InputDate';

const CATEGORIA = {
  PAGO_DE_EXPENSAS : 1
}

@connect(
  (store) => ({
    crearAjusteState: store.get('api').crearAjuste || {},
    listarUnidadesState: store.get('api').listarUnidades || {},
    layout: store.get('layout') || {}
  }),
  (dispatch) => ({
    crearAjuste: bindActionCreators(actionCreators, dispatch).crearAjuste,
    crearAjusteReset: bindActionCreators(actionCreators, dispatch).crearAjusteReset,
    listarUnidades: bindActionCreators(actionCreators, dispatch).listarUnidades
  })
)
export default class CreateAjuste extends React.Component {

  static propTypes = {
    unidades: React.PropTypes.array,
    textBtn: React.PropTypes.string.isRequired,
    onSuccess: React.PropTypes.func
  }

  constructor(props) {
    super(props);
    this.onSubmit = this.onSubmit.bind(this);
    this.onComponentValueChange = this.onComponentValueChange.bind(this);
    this.setItemCombo = this.setItemCombo.bind(this);
    this.fillUnidadesOptions = this.fillUnidadesOptions.bind(this);

    this.state = {
      visible: false,
      error: null,
      reset: false,
      inputValues: [],
      unidades: props.layout.unidades ? this.fillUnidadesOptions(props.layout.unidades) : null
    };
  }

  show() {
    this.setState({ visible: true });
  }

  hide() {
    this.setState({ visible: false });
  }

  onSubmit(missingFields) {
    if (missingFields) {
      var val = [];
      missingFields.map((value) => {
        val[`${value.id}Error`] = value.errorMessage;
      });
      this.setState({ "inputValues": _.extend(this.state.inputValues, val) });
    } else {
      this.props.crearAjuste({
        idConsorcio: this.props.layout.consorcio.idConsorcio,
        idUnidad: this.state.inputValues.idUnidad,
        monto: this.state.inputValues.monto,
        fecha: this.state.inputValues.fecha,
        descripcion: this.state.inputValues.descripcion
      });
    }    
  }

  componentWillReceiveProps(nextProps) {
    if (this.state.visible) {
      if (!nextProps.layout.unidades) {
        this.props.listarUnidades({ idConsorcio: nextProps.layout.consorcio.idConsorcio });
      } else if (_.isEmpty(this.state.unidades) && nextProps.layout.unidades) {
        this.setState({ unidades: this.fillUnidadesOptions(nextProps.layout.unidades) })
      }

      if ((!nextProps.crearAjusteState.loading && nextProps.crearAjusteState.success) && (nextProps.crearAjusteState.data !== this.props.crearAjusteState.data)) {
        this.setState({ error: null, reset: true  });
        this.props.onSuccess();

        var inputValues = this.state.inputValues;
        for (var i in inputValues) {
          inputValues[i] = '';
        };
        this.setState({ "inputValues":inputValues });


        setTimeout(() => {
          this.setState({ visible: false, reset: false});
          this.props.crearAjusteReset();
        }, 2000);

      } else if (nextProps.crearAjusteState.error) {
        this.setState({ error: nextProps.crearAjusteState.error });
      }
    }
  }

  fillUnidadesOptions(data) {
    return data.map((unidad) => ({ label: unidad.unidad, value: unidad.idUnidad }))
  }

  onComponentValueChange(e) {
    let val = [];
    if (e.target.id === "monto" && Math.sign(e.target.value) == 1 && this.state.inputValues.tipoMovimiento === "egreso") {
      val["monto"] = String(-e.target.value);
      this.setState({ "inputValues": _.extend(this.state.inputValues, val) });
    } else {

      if (e.target.id === "tipoMovimiento" && !_.isEmpty(this.state.inputValues.monto)) {
        if (e.target.value === "ingreso") {  
          val["monto"] = String(Math.abs(this.state.inputValues.monto));
        } else if (Math.sign(this.state.inputValues.monto) == 1) {
          val["monto"] = String(-this.state.inputValues.monto);
        }
      }

      val[e.target.id] = e.target.value;
      val[`${e.target.id}Error`] = '';
      this.setState({ "inputValues": _.extend(this.state.inputValues, val) });
    }
  }

  setItemCombo(index, value) {
    var val = [];
    val[index] = value;
    val[`${index}Error`] = '';
    this.setState({ "inputValues": _.extend(this.state.inputValues, val) });
  }

  render() {

    return (
      <div>
        <button className="btn btn-flat btn-default-toro file-input pull-right" onClick={this.show.bind(this)}>
          {this.props.textBtn}
        </button>

        <Rodal visible={this.state.visible} onClose={this.hide.bind(this)} customStyles={{height:"auto", "overflow":"scroll","margin":"20px auto"}}>

          <SimpleFormWrap
            titulo="Nueva Ajuste"
            onSubmit={this.onSubmit}
            loading={this.props.crearAjusteState.loading}
            errorMessage={this.state.error}
            successMessage="nuevo Ajuste cargada"
            success={this.props.crearAjusteState.success}
            disabled={this.props.crearAjusteState.loading}
            >

            <InputDate id="fecha" name="Fecha" mandatory={true} onChange={this.onComponentValueChange} value={this.state.inputValues.fecha} errorMessage={this.state.inputValues.fechaError}/>
            <InputText id="descripcion" name="Descripcion" onChange={this.onComponentValueChange} value={this.state.inputValues.descripcion} errorMessage={this.state.inputValues.descripcionError} />

            <InputReactSelect
              id="idUnidad"
              name="unidad"
              onChange={this.setItemCombo}
              value={this.state.inputValues.idUnidad}
              options={this.state.unidades}
              errorMessage={this.state.inputValues.idUnidadError}
            />

            <InputChoice
              id="tipoMovimiento"
              name="Tipo de movimiento"
              onChange={this.onComponentValueChange}
              mandatory={true}
              value={this.state.inputValues.tipoMovimiento}
              choices={
                [
                  {
                    value: 'a favor de la UF'
                  },
                  {
                    value: 'en contra de la UF'
                  }
                ]
              }
              errorMessage={this.state.inputValues.tipoMovimientoError}
            />
            <InputText id="monto" name="Monto" mandatory={true} onChange={this.onComponentValueChange} value={this.state.inputValues.monto} errorMessage={this.state.inputValues.montoError} />
            <p className="text-center">Un ajuste a favor de la UF otorga saldo a la cuenta corriente de la misma.</p>
          </SimpleFormWrap>

        </Rodal>
      </div>
    );
  }
}
