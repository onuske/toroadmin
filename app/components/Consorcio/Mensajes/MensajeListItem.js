import React from 'react';
import Rodal from 'rodal';
import dateFormat from 'dateformat';

export default class MensajeListItem extends React.Component {

	static propTypes = {
		item: React.PropTypes.object,
		onClick: React.PropTypes.func
	}

	constructor(props) {
		super(props)
		this.state = { loading: false, action: null }
		this.onClickVerMensaje = this.onClickVerMensaje.bind(this);
	}

	show() {
    this.setState({ visible: true });
  }

  hide() {
    this.setState({ visible: false });
  }

  onClickVerMensaje() {
  	this.show()
	}

	render() {
		return (
			<div>
				<ul className="action-buttons">
          <li>
  					<button onClick={this.onClickVerMensaje} className="btn btn-default-toro flat">
  						{
  						this.state.loading == true && this.state.action === "verMensaje"
  						?<i className="fa fa-refresh fa-spin"></i>
  						:<i className="glyphicon glyphicon-eye-open"></i>
  						}
  						<span>Ver</span>
            </button>
					</li>
				</ul>

      	<Rodal visible={this.state.visible} onClose={this.hide.bind(this)} width={90} height={80} measure="%" customStyles={{height:"auto", "overflow":"scroll","margin":"20px auto"}}>
          {
            this.state.visible
          ?
            (
              <div style={{margin: "50px"}}> <span>{dateFormat(this.props.item.fecha, "dd/mm/yyyy")}</span> 
                <br />
                <span> {this.props.item.detail.email} </span>                
                <br />
                <span> {this.props.item.detail.tel} </span>                
                <br />
                <span> {this.props.item.detail.descripcion} </span>                

              </div>
            )
          :
            ""
          }
      	</Rodal>
      </div>
		);
	}
}
