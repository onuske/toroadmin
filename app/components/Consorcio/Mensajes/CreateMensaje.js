import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Rodal from 'rodal';

import { actionCreators } from 'actions/api';
import SimpleFormWrap from 'components/shared/SimpleFormWrap';
import InputText from 'components/shared/InputText';
import InputReactSelect from 'components/shared/InputReactSelect';
import InputCKEditor from 'components/shared/InputCKEditor';

const toolbar = [
   ['Styles','Format','Font','FontSize'],
   '/',
   ['Bold','Italic','Underline','StrikeThrough','-','Undo','Redo','-','Cut','Copy','Paste','Find','Replace','-','Outdent','Indent','-','Print'],
   '/',
   ['NumberedList','BulletedList','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
   ['Image','Table','-','Link','Flash','Smiley','TextColor','BGColor','Source']
  ]

@connect(
  (store) => ({
    crearMensajeState: store.get('api').crearMensaje || {},
    listarUnidadesState: store.get('api').listarUnidades || {},
    layout: store.get('layout') || {}
  }),
  (dispatch) => ({
    crearMensaje: bindActionCreators(actionCreators, dispatch).crearMensaje,
    crearMensajeReset: bindActionCreators(actionCreators, dispatch).crearMensajeReset,
    listarUnidades: bindActionCreators(actionCreators, dispatch).listarUnidades
  })
)
export default class CreateMovimiento extends React.Component {

  static propTypes = {
    data: React.PropTypes.object,
    textBtn: React.PropTypes.string.isRequired,
    onSuccess: React.PropTypes.func
  }

  constructor(props) {
    super(props);
    this.onSubmit = this.onSubmit.bind(this);
    this.onComponentValueChange = this.onComponentValueChange.bind(this);
    this.setItemCombo = this.setItemCombo.bind(this);
    this.updateContent = this.updateContent.bind(this);
    this.state = {
      visible: false,
      reset: false,
      error: null,
      categorias: null,
      unidades: !_.isEmpty(props.listarUnidadesState) ? props.listarUnidadesState.data.map((unidad) => ({ label: unidad.unidad, value: String(unidad.idUnidad) })) : [],
      inputValues: []
    };
  }

  show() {
    this.setState({ visible: true });
  }

  hide() {
    this.setState({ visible: false });
  }

  onSubmit(missingFields) {
    if (missingFields) {
      var val = [];
      missingFields.map((value) => {
        val[`${value.id}Error`] = value.errorMessage;
      });
      this.setState({ "inputValues": _.extend(this.state.inputValues, val) });
    } else {
      this.props.crearMensaje({
        idConsorcio: this.props.layout.consorcio.idConsorcio,
        idUnidad: this.state.inputValues.unidad,
        email: this.state.inputValues.email,
        telefono: this.state.inputValues.telefono,
        descripcion: this.state.inputValues.descripcion
      });
    }
  }

  componentWillMount() {
    if (!this.props.listarUnidadesState.data) {
      this.props.listarUnidades({idConsorcio: this.props.layout.consorcio.idConsorcio});
    }
  }

  componentWillReceiveProps(nextProps) {

    if (_.isEmpty(this.state.unidades) && nextProps.listarUnidadesState.data) {
      this.setState({
        unidades: nextProps.listarUnidadesState.data.map((unidad) => ({ label: unidad.unidad, value: String(unidad.idUnidad) }))
      })
    }

    if (nextProps.crearMensajeState.success && nextProps.crearMensajeState.data && nextProps.crearMensajeState.data !== this.props.crearMensajeState.data) {

      this.props.onSuccess();
      this.setState({ error: null, reset: true });

      var inputValues = this.state.inputValues;
      for(var i in inputValues){
        inputValues[i] = '';
      };
      this.setState({ "inputValues":inputValues });


      setTimeout(() => {
        this.setState({ visible: false,  reset: false });
        this.props.crearMensajeReset();
      }, 2000);

    } else if (nextProps.crearMensajeState.error) {
      this.setState({error: this.props.crearMensajeState.error});
    }

  }

  onComponentValueChange(e) {
    let val = [];    
    val[e.target.id] = e.target.value;
    val[`${e.target.id}Error`] = '';
    this.setState({ "inputValues": _.extend(this.state.inputValues, val) });
  }

  setItemCombo(index, value) {
    var val = [];
    val[index] = value;
    val[`${index}Error`] = '';
    this.setState({ "inputValues": _.extend(this.state.inputValues, val) });
  }

  updateContent(newContent) {
    let val = [];    
    val["descripcion"] = newContent;
    val["descripcionError"] = '';
    this.setState({ "inputValues": _.extend(this.state.inputValues, val) });
  }

  render() {
    return (
      <div>
        <button className="btn btn-flat btn-default-toro file-input pull-right" onClick={this.show.bind(this)}>
          {this.props.textBtn}
        </button>

        <Rodal visible={this.state.visible} onClose={this.hide.bind(this)} customStyles={{height:"auto", width: "500px", "overflow":"scroll","margin":"auto"}}>

          <SimpleFormWrap
            titulo="Nuevo mensaje"
            onSubmit={this.onSubmit}
            loading={this.props.crearMensajeState.loading}
            errorMessage={this.state.error}
            successMessage="nuevo mensaje creado"
            success={this.props.crearMensajeState.success}
            formStyle="inline"
            >

            <InputReactSelect
              id="origen"
              name="Origen"
              onChange={this.setItemCombo}
              value={this.state.inputValues.origen}
              options={[{ label: "TELEFONO", value: "TELEFONO" }, { label: "EMAIL", value: "EMAIL" }]}
              errorMessage={this.state.inputValues.origenError}
              inputStyle="inline"
              widthLabel="3"
            />

            <InputReactSelect
              id="estado"
              name="Estado"
              onChange={this.setItemCombo}
              value={this.state.inputValues.estado}
              options={[{ label: "PENDIENTE", value: "PENDIENTE" }]}
              errorMessage={this.state.inputValues.estadoError}
              inputStyle="inline"
              widthLabel="3"
            />

            <InputReactSelect
              id="unidad"
              name="Unidad"
              onChange={this.setItemCombo}
              value={this.state.inputValues.unidad}
              options={this.state.unidades}
              errorMessage={this.state.inputValues.unidadError}
              inputStyle="inline"
              widthLabel="3"
            />

            <InputText id="email" name="Email" onChange={this.onComponentValueChange} value={this.state.inputValues.email} errorMessage={this.state.inputValues.emailError} inputStyle="inline" widthLabel="3" />
            <InputText id="telefono" name="telefono" onChange={this.onComponentValueChange} value={this.state.inputValues.telefono} errorMessage={this.state.inputValues.telefonoError} inputStyle="inline" widthLabel="3" />
            <InputCKEditor 
              id="descripcion"
              name="Descripcion"
              mandatory={true}
              onChange={this.updateContent}
              value={this.state.inputValues.descripcion}
              errorMessage={this.state.inputValues.descripcionError}
              inputStyle="inline"
              widthLabel="3"
              config={'ckeditor', { toolbar : toolbar, uiColor : '#9AB8F3'}}
              activeClass="p10"
              reset={this.state.reset}
            />
            
          </SimpleFormWrap>

        </Rodal>
      </div>
    );
  }
}
