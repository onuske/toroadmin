import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import JsonTable from 'react-json-table';
import dateFormat from 'dateformat';

import { actionCreators } from 'actions/api';
import CreateMensaje from './CreateMensaje';
import MensajeListItem from './MensajeListItem';
import SelectPeriodo from 'components/shared/SelectPeriodo';
import LoadingOverlay from 'components/shared/LoadingOverlay';


let settings = {
  "noRowsMessage": <LoadingOverlay style={{bottom: 0, top: 0, left: 0, right: 0, width: 1000, maxWidth: "100%", marginBottom: 20}} />,
  "cellClass": (currentClass, columnKey, rowData )=>( columnKey === "actions" ? "action-column" : "" ),
  "headerClass": ( currentClass, columnKey )=>( "ttt" )
}

@connect(
  (store) => ({
    obtenerMensajesState: store.get('api').obtenerMensajes || {loading: true},
    layout: store.get('layout') || {}
  }),
  (dispatch) => ({
    obtenerMensajes: bindActionCreators(actionCreators, dispatch).obtenerMensajes
  })
)
export default class Mensajes extends React.Component {

  static propTypes = {
    layout: React.PropTypes.object, // eslint-disable-line react/no-unused-prop-types
    obtenerMensajes: React.PropTypes.func
  }

  constructor(props) {
    super(props);
    this.state = {
      items : [],
      columns : [],
      shouldUpdate: false
    };
    this.buildTable = this.buildTable.bind(this);
    this.actionsRow = this.actionsRow.bind(this);
    this.onSuccess = this.onSuccess.bind(this);
  }

  onSuccess() {
    this.props.obtenerMensajes({ idConsorcio: this.props.layout.consorcio.idConsorcio,  idPeriodo: this.props.layout.consorcioSelected.idPeriodoSelected })
  }

  componentWillMount() {
    if (this.props.obtenerMensajesState.data) {
      this.buildTable(this.props.obtenerMensajesState.data)
    }
  }

  componentWillReceiveProps(nextProps) {
    
    if (nextProps.obtenerMensajesState.loading && !nextProps.obtenerMensajesState.success) {
      this.setState({ items: [], columns: [] });

    } else if (!nextProps.obtenerMensajesState.loading && nextProps.obtenerMensajesState.data) {
      this.buildTable(nextProps.obtenerMensajesState.data);
    }
    
  }

  buildTable(data) {
    
    if (_.isEmpty(data)) {
      data = [{ "idMensaje":null,"idConsorcio":null,"idUnidad":null,"descripcion":null,"email":null,"telefono":null,"createdAt":null,"updatedAt":null,"deleted":null,"nombreUnidad":null }];
    }

    let value = [];
    _.map( data, (val, key) => {
      value[key] = {
        "createdAt": val.createdAt !== null ? dateFormat(val.createdAt, "dd/mm/yyyy") : val.createdAt,
        "descripcion": val.descripcion !== null ? val.descripcion.slice(0, 15) : val.descripcion,
        "nombreUnidad": val.nombreUnidad,
        "detail": {
          "descripcion": val.descripcion,
          "email": val.email,
          "telefono": val.telefono
        },
        "actions": "" };
    });

    this.setState({
      "items": value,
      "columns": [
        {key: 'createdAt', label: 'Fecha'},
        {key: 'descripcion', label: 'Descripcion'},
        {key: 'nombreUnidad', label: 'Unidad'},
        {key: 'actions', label: 'Acciones'}
      ]
    });
  }

  actionsRow(item) {
    return <MensajeListItem item={item}/>
  }

  render() {
    let state = {
      ...this.state
    }
    if (!_.isEmpty(state.items)) {
      state.columns[3].cell = ( item, columnKey ) => {
        return item.descripcion ? this.actionsRow(item) : null;
      }
    }
    return (
      <div>
        <div className="buscador_filtros">
          <div className="row">
            <div className="col-md-6">
            {
              this.props.layout.consorcio
            ?
              <div className="col-sm-4">
                <SelectPeriodo diaCierre={this.props.layout.consorcio.diaCierre}/>
              </div>
            :
              ""
            }
            </div>
            <div className="col-md-6">
            <CreateMensaje onSuccess={this.onSuccess} data={this.props} textBtn="Crear Mensaje"/>
            </div>
          </div>
        </div>

        <div className="box-header">
          <h3 className="box-title">Mensajes</h3>
        </div>    


        <div className="row">
          <JsonTable rows={ state.items } columns={ state.columns} className="table table-bordered table-hover dataTable" settings={settings} classPrefix="" />
        </div>
      </div>
    );
  }
}
