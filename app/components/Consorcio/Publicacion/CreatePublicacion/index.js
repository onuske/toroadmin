import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Rodal from 'rodal';

import { actionCreators } from 'actions/api';
import LoadingOverlay from 'components/shared/LoadingOverlay';
import SuccessOverlay from 'components/shared/SuccessOverlay';
import UploadFile from 'components/shared/UploadFile';
import SimpleFormWrap from 'components/shared/SimpleFormWrap';
import InputText from 'components/shared/InputText';
import InputDate from 'components/shared/InputDate';

@connect(
  (store) => ({
    crearPublicacionState: store.get('api').crearPublicacion || {},
    layout: store.get('layout') || {}
  }),
  (dispatch) => ({
    crearPublicacion: bindActionCreators(actionCreators, dispatch).crearPublicacion,
    crearPublicacionReset: bindActionCreators(actionCreators, dispatch).crearPublicacionReset,
    borrarArchivo: bindActionCreators(actionCreators, dispatch).borrarArchivo
  })
)
export default class CreatePublicacion extends Component {

  static propTypes = {
    textBtn: React.PropTypes.string.isRequired,
    onSuccess: React.PropTypes.func
  }

  constructor(props, context) {
    super(props, context);
    this.getInitialState = this.getInitialState.bind(this);
    this.onSuccesUploadFile = this.onSuccesUploadFile.bind(this);
    this.deleteFile = this.deleteFile.bind(this);
    this.onComponentValueChange = this.onComponentValueChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.changeStatus = this.changeStatus.bind(this);
    this.state = this.getInitialState();
  }

  getInitialState() {
    return  {
     error: null,
     helpText: "haga click en Examinar para subir un archivo",
     archivosSubidos: [],
     inputValues: [],
     forceUpdate: null,
     uploadingFile: false
    }
  }

  show() {
    this.setState({ visible: true });
  }

  hide() {
    this.setState({ visible: false });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.crearPublicacionState.loading && !nextProps.crearPublicacionState.success) {
      this.setState({ helpText: "creando publicacion..." });
    } else if (nextProps.crearPublicacionState.success && nextProps.crearPublicacionState.data && nextProps.crearPublicacionState.data !== this.props.crearPublicacionState.data) {
      var inputValues = this.state.inputValues;
      for (var i in inputValues) {
        inputValues[i] = '';
      };
      this.setState({ inputValues: inputValues, archivosSubidos: [], error: null, helpText: "publicacion creada" });
      setTimeout(() => {
        this.props.onSuccess();
        this.setState({ visible: false, helpText: "haga click en Examinar para subir un archivo"});
        this.props.crearPublicacionReset();
      }, 2000);
    } else if (nextProps.crearPublicacionState.error) {
      this.setState({ error: this.props.crearPublicacionState.error, helpText: "hubo un error" });
    }
  }

  onComponentValueChange(e) {
    let val = [];    
    val[e.target.id] = e.target.value;
    val[`${e.target.id}Error`] = '';
    this.setState({ "inputValues": _.extend(this.state.inputValues, val) });
  }

  onSubmit(missingFields) {
    if (missingFields) {
      var val = [];
      missingFields.map((value) => {
        val[`${value.id}Error`] = value.errorMessage;
      });
      this.setState({ "inputValues": _.extend(this.state.inputValues, val) });
    } else {

      let idArchivos = [];
      for(var i in this.state.archivosSubidos){
        idArchivos.push(this.state.archivosSubidos[i].idArchivo)
      }
      this.props.crearPublicacion({
        idConsorcio: this.props.layout.consorcio.idConsorcio,
        titulo: this.state.inputValues.titulo,
        fecha: this.state.inputValues.fecha,
        idArchivos: idArchivos
      });
    }
  }

  onSuccesUploadFile(data){
    this.setState((prevState, props) => {
      var values = prevState.archivosSubidos;
      values[data.uuid] = data;
      return { archivosSubidos: values, uploadingFile: false, helpText: "haga click en Examinar para subir un archivo" }
    });
  }

  deleteFile(uuid) {
    this.props.borrarArchivo({ "idArchivo": this.state.archivosSubidos[uuid].idArchivo, "uuid": uuid });
    this.setState((prevState, props) => {
      var values = prevState.archivosSubidos;
      delete values[uuid];
      return { archivosSubidos: values, forceUpdate: uuid }
    });
  }

  changeStatus() {
    this.setState({ uploadingFile: true, helpText: "subiendo archivo..." })
  }

  render() {
    let archivosSubidos = this.state.archivosSubidos;
    return (
      <div>
        <button className="btn btn-flat btn-default-toro pull-right" onClick={this.show.bind(this)}>
          {this.props.textBtn}
        </button>

        <Rodal visible={this.state.visible} onClose={this.hide.bind(this)} customStyles={{"margin":"auto", "height":"auto", "width":"auto", "maxWidth":"60%"}}>
          <SimpleFormWrap
            titulo="Crear publicacion"
            onSubmit={this.onSubmit}
            loading={this.props.crearPublicacionState.loading}
            errorMessage={this.state.error}
            success={this.props.crearPublicacionState.success}
            formType="inline"
            disabled={this.state.uploadingFile}
            >

            <UploadFile onClick={this.changeStatus} onSuccess={this.onSuccesUploadFile} archivosSubidos={archivosSubidos} shouldUpdate={this.state.visible} forceUpdate={this.state.forceUpdate} onDelete={this.deleteFile}/>

            <InputText id="titulo" name="Titulo" mandatory={true} onChange={this.onComponentValueChange} value={this.state.inputValues.titulo} errorMessage={this.state.inputValues.tituloError}/>
            
            <InputDate id="fecha" name="Fecha" mandatory={true} onChange={this.onComponentValueChange} value={this.state.inputValues.fecha} errorMessage={this.state.inputValues.fechaError}/>

          </SimpleFormWrap>
          <br />
          <p className="help-block">{this.state.helpText}</p>

        </Rodal>  
      </div>
    );
  }
}




           
