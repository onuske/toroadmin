import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import JsonTable from 'react-json-table';
import dateFormat from 'dateformat';

import { actionCreators } from 'actions/api';
import CreatePublicacion from './CreatePublicacion';
import PublicacionListItem from './PublicacionListItem';
import LoadingOverlay from 'components/shared/LoadingOverlay';

let settings = {
  "noRowsMessage": <LoadingOverlay style={{bottom: 0, position: 'absolute', top: 0, left: 0, right: 0}} />,
  "cellClass": (currentClass, columnKey, rowData )=>( columnKey === "actions" ? "action-column" : "" ),
  "headerClass": ( currentClass, columnKey )=>( "ttt" )
}

@connect(
  (store) => ({
    obtenerPublicacionesState: store.get('api').obtenerPublicaciones || {loading: true},
    descargarArchivoState: store.get('api').descargarArchivo || {},
    layout: store.get('layout') || {}
  }),
  (dispatch) => ({
    obtenerPublicaciones: bindActionCreators(actionCreators, dispatch).obtenerPublicaciones,
    descargarArchivo: bindActionCreators(actionCreators, dispatch).descargarArchivo
  })
)
export default class Publicacion extends React.Component {

  static propTypes = {
    layout: React.PropTypes.object, // eslint-disable-line react/no-unused-prop-types
    obtenerPublicaciones: React.PropTypes.func
  }

  constructor(props) {
    super(props);
    this.state = {
      items: [],
      "columns": [
        {key: 'titulo', label: 'Titulo'},
        {key: 'fecha', label: 'Fecha'},
        {key: 'actions', label: 'Archivos'}
      ],
      publicacionesEnMemoria: [],
      retrieved: { index: null, uuid: null }
    };
    this.actionsRow = this.actionsRow.bind(this);
    this.buildTable = this.buildTable.bind(this);
    this.onSuccess = this.onSuccess.bind(this);
    this.descargarArchivo = this.descargarArchivo.bind(this);
    this.guardarBlobURL = this.guardarBlobURL.bind(this);
  }

  onSuccess() {
    this.setState({ items: [] })
    this.props.obtenerPublicaciones({ idConsorcio: this.props.layout.consorcio.idConsorcio,  idPeriodo: this.props.layout.consorcioSelected.idPeriodoSelected })
  }

  componentWillMount() {
    if (this.props.obtenerPublicacionesState.data) {
      this.buildTable(this.props.obtenerPublicacionesState.data)
    }
    else if (!this.props.obtenerPublicacionesState.data) {
      this.props.obtenerPublicaciones({ idConsorcio: this.props.layout.consorcio.idConsorcio })
    }
  }

  componentWillReceiveProps(nextProps) {

    if (nextProps.obtenerPublicacionesState.loading === false && nextProps.obtenerPublicacionesState.data) {
      this.buildTable(nextProps.obtenerPublicacionesState.data);
    }

    if(typeof this.state.retrieved.index === 'number' && typeof this.state.publicacionesEnMemoria[this.state.retrieved.index] === 'undefined') {

      if(typeof nextProps.descargarArchivoState.data != 'undefined') {
        var blobURL = window.URL.createObjectURL(nextProps.descargarArchivoState.data);
        this.guardarBlobURL(this.state.retrieved, blobURL);
      }
    } else if (typeof this.state.publicacionesEnMemoria[this.state.retrieved.index] === 'object') {

      if(typeof nextProps.descargarArchivoState.data != 'undefined') {
        var blobURL = window.URL.createObjectURL(nextProps.descargarArchivoState.data);

        this.guardarBlobURL(this.state.retrieved, blobURL);
      }

    }

  }

  guardarBlobURL(retrieved, blobURL) {
    var publicacion = this.state.publicacionesEnMemoria;
    var values = [];
    values[retrieved.uuid] = blobURL;

    if (typeof publicacion[retrieved.index] === 'object') {
      _.extend(publicacion[retrieved.index], values)
    } else {
      publicacion[retrieved.index] = values
    }

    this.setState({ publicacionesEnMemoria: publicacion });
  }

  guardarPublicacionesPorUuid(uuid, data) {
    var values = this.state.publicacionesEnMemoria ;
    values[uuid] = data;
    this.setState({ publicacionesEnMemoria: values });
  }
  
  descargarArchivo(index, uuid){
    this.setState({ retrieved: { index: index, uuid: uuid } })
    this.props.descargarArchivo({"uuid": uuid});
  }

  buildTable(data) {

    if (_.isEmpty(data)) {
      data = [{ "publicacion":{"idPublicacion":null,"idConsorcio":null,"titulo":null,"fecha":null,},"archivos":[] }]
    }
    
    let value = [];
    _.map( data, (val, key) => {
      let archivos = [];
      if (!_.isEmpty(val.archivos)) val.archivos.map((archivo)=>(archivos[archivo.uuid] = archivo))

      value[key] = {
        "idPublicacion": val.publicacion.idPublicacion,
        "titulo": val.publicacion.titulo,
        "fecha": !_.isEmpty(val.publicacion.fecha) ? dateFormat(val.publicacion.fecha, "yyyy-mm-dd") : null,
        "archivos": archivos
      }
    });

    this.setState({
      "items": value,
      "columns": [
        {key: 'titulo', label: 'Titulo'},
        {key: 'fecha', label: 'Fecha'},
        {key: 'actions', label: 'Archivos'}
      ]
    });
  }

  actionsRow(item) {
    return <PublicacionListItem item={item} archivosEnMemoria={this.state.publicacionesEnMemoria[this.state.items.indexOf(item)]} index={this.state.items.indexOf(item)} onClick={this.descargarArchivo} onSuccess={this.onSuccess}/>
  }


  render() {

    let state = {
      ...this.state
    }

    if (!_.isEmpty(state.items)) {
      state.columns[2].cell = ( item, columnKey ) => {
        return item.titulo !== null ? this.actionsRow(item) : ""
      }
    }

    return (
      <div>
        <div className="buscador_filtros">
          <div className="row">
            <div className="col-md-6">

            </div>
            <div className="col-md-6">
              <CreatePublicacion onSuccess={this.onSuccess} textBtn="Crear Publicacion"/>
            </div>
          </div>
        </div>  

        <div className="box-header">
          <h3 className="box-title">Publicaciones del período</h3>
        </div>    

        <div className="row">
          <div className="col-md-12">
            <div className="box">
              <div>

                <JsonTable rows={state.items} columns={state.columns} className="table table-bordered table-hover dataTable" settings={settings} classPrefix="" />
              
              </div>
            </div>
          </div>
        </div>

      </div>
    );
  }
}