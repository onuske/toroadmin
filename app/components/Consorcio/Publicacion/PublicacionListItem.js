import React from 'react';
import Rodal from 'rodal';
import EditPublicacion from './EditPublicacion';

export default class PublicacionListItem extends React.Component {

	static propTypes = {
		item: React.PropTypes.object,
		onClick: React.PropTypes.func,
		archivosEnMemoria: React.PropTypes.array,
    index: React.PropTypes.number,
    onSuccess: React.PropTypes.func
	}

	constructor(props) {
		super(props)
    this.onClickDescargar = this.onClickDescargar.bind(this);
    this.onClickVerArchivo = this.onClickVerArchivo.bind(this);
    this.forzarDescarga = this.forzarDescarga.bind(this);
    this.onClickEditar = this.onClickEditar.bind(this);
    this.onSuccess = this.onSuccess.bind(this);
		this.state = { loading: false, action: null, shouldUpdate: false, fileRetrieved: null }
	}

	show() {
    this.setState({ visible: true });
  }

  hide() {
    this.setState({ visible: false });
  }

	onClickDescargar(e) {

    let fileRetrieved = this.props.item.archivos[e.target.parentElement.parentElement.id];
		if (typeof this.props.archivosEnMemoria === 'undefined' || !this.props.archivosEnMemoria.includes(fileRetrieved.uuid)) {
			this.setState({ action: "descargar", loading: true, shouldUpdate: true, fileRetrieved: fileRetrieved });
			this.props.onClick(this.props.index, fileRetrieved.uuid);
		} else {
      this.setState({fileRetrieved: fileRetrieved})
			this.forzarDescarga(this.props.archivosEnMemoria[fileRetrieved.uuid], fileRetrieved.nombre);
		}
  }

  onClickVerArchivo(e) {
    let fileRetrieved = this.props.item.archivos[e.target.parentElement.parentElement.id];
    if (typeof this.props.archivosEnMemoria === 'undefined' || !this.props.archivosEnMemoria.includes(fileRetrieved.uuid)) {
      this.setState({ action: "ver", loading: true, shouldUpdate: true, fileRetrieved: fileRetrieved });
      this.props.onClick(this.props.index, fileRetrieved.uuid);
    } else {
      this.setState({fileRetrieved: fileRetrieved})
      this.show()
    }
  }

  onClickEditar(e) {
      this.setState({action: "editar"})
      this.show()
  }

  forzarDescarga(blobURL, fileName) {
    var tempLink = document.createElement('a');
    tempLink.href = blobURL;
    tempLink.setAttribute('download', fileName);
    tempLink.setAttribute('target', '_blank');
    document.body.appendChild(tempLink);
    tempLink.click();
    document.body.removeChild(tempLink);
  }

  componentWillReceiveProps(nextProps) {
		if (this.state.shouldUpdate) {
  		if (typeof nextProps.archivosEnMemoria !== 'undefined' && nextProps.archivosEnMemoria[this.state.fileRetrieved.uuid]) {
  			this.setState({ loading: false, shouldUpdate: false });
	    	if (this.state.action === "ver") {
			  	this.show()
	    	} else if (this.state.action === "descargar") {
					this.forzarDescarga(nextProps.archivosEnMemoria[this.state.fileRetrieved.uuid], this.state.fileRetrieved.nombre);
		    }
  		}
	  }
  }

  onSuccess() {
    this.props.onSuccess()
    this.hide();
  }

	render() {
    let archivos = this.props.item.archivos;
    let content = null;
    if (this.state.action === "ver" && this.props.archivosEnMemoria && this.state.fileRetrieved.uuid) {
      content = <object data={this.props.archivosEnMemoria[this.state.fileRetrieved.uuid]} type='application/pdf' style={{"width":"780px", "height":"600px"}} ></object>
    } else if ( this.state.action === "editar") {
      content = <EditPublicacion onSuccess={this.onSuccess} item={this.props.item} visible={this.state.visible}/>
    }

    return (
			<div>

        {
          typeof archivos === 'object'
        ?
          <ul className="action-buttons">
            {Object.entries(archivos).map(([key, val]) => (
              <div key={val.uuid} id={key} style={{"clear": "both"}}>
                <li><label style={{"verticalAlign": "center"}} >{val.nombre.slice(0, 15)}</label></li>

                <li>
                  <button onClick={this.onClickVerArchivo} className="btn btn-default-toro button-li flat">
                    {
                      this.state.fileRetrieved && this.state.fileRetrieved.uuid === val.uuid && this.state.loading == true && this.state.action === "ver"
                    ?
                      <i className="fa fa-refresh fa-spin"></i>
                    :
                      <i className="glyphicon glyphicon-eye-open"></i>
                    }
                    <span>Ver</span>
                  </button>
                </li>

                <li>
                  <button onClick={this.onClickDescargar} className="btn btn-default-toro button-li flat">
                    {
                      this.state.fileRetrieved && this.state.fileRetrieved.uuid === val.uuid && this.state.loading == true && this.state.action === "descargar"
                    ?
                      <i className="fa fa-refresh fa-spin"></i>
                    :
                      <i className="glyphicon glyphicon-download-alt"></i>
                    }
                    <span>Descargar</span>
                  </button>
                </li>
              </div>
            ))}

          </ul>
        :
          ""
        }
        <ul className="action-buttons">

          <li>
            <button onClick={this.onClickEditar} className="btn btn-default-toro button-li flat">
              {
                this.state.loading == true && this.state.action === "editar"
              ?
                <i className="fa fa-refresh fa-spin"></i>
              :
                <i className="glyphicon glyphicon-edit"></i>
              }
              <span>Editar</span>
            </button>
          </li>
        </ul>

      	<Rodal visible={this.state.visible} onClose={this.hide.bind(this)} width={820} measure="px" customStyles={{height:"auto", "overflow":"scroll", "margin":"20px auto"}}>
          {
            this.state.visible
          ?
            (
              <div>
                {content}
              </div>
            )
          :
            ""
          }
      	</Rodal>
      </div>
		);
	}
}
