import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { actionCreators } from 'actions/api';
import LoadingOverlay from 'components/shared/LoadingOverlay';
import SuccessOverlay from 'components/shared/SuccessOverlay';
import UploadFile from 'components/shared/UploadFile';
import SimpleFormWrap from 'components/shared/SimpleFormWrap';
import InputText from 'components/shared/InputText';
import InputDate from 'components/shared/InputDate';

@connect(
  (store) => ({
    editarPublicacionState: store.get('api').editarPublicacion || {},
    eliminarPublicacionState: store.get('api').eliminarPublicacion || {},
    layout: store.get('layout') || {}
  }),
  (dispatch) => ({
    editarPublicacion: bindActionCreators(actionCreators, dispatch).editarPublicacion,
    editarPublicacionReset: bindActionCreators(actionCreators, dispatch).editarPublicacionReset,
    eliminarPublicacion: bindActionCreators(actionCreators, dispatch).eliminarPublicacion,
    eliminarPublicacionReset: bindActionCreators(actionCreators, dispatch).eliminarPublicacionReset
  })
)
export default class EditPublicacion extends Component {

  static propTypes = {
    item: React.PropTypes.object.isRequired,
    onSuccess: React.PropTypes.func,
    visible: React.PropTypes.bool
  }

  constructor(props, context) {
    super(props, context);
    this.getInitialState = this.getInitialState.bind(this);
    this.onSuccesUploadFile = this.onSuccesUploadFile.bind(this);
    this.deleteFile = this.deleteFile.bind(this);
    this.deletePublicacion = this.deletePublicacion.bind(this);
    this.onComponentValueChange = this.onComponentValueChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.changeStatus = this.changeStatus.bind(this);

    this.state = this.getInitialState();
  }

  getInitialState() {
    return  {
     success: false,
     error: null,
     loading: false,
     helpText: "haga click en Examinar para subir un archivo",
     archivosSubidos: this.props.item.archivos,
     inputValues: { fecha: this.props.item.fecha, titulo: this.props.item.titulo },
     forceUpdate: null,
     uploadingFile: false
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.editarPublicacionState.loading && !nextProps.editarPublicacionState.success) {
      this.setState({ loading: true, helpText: "actualizando publicacion..." });
    } else if (nextProps.editarPublicacionState.success && nextProps.editarPublicacionState.data) {
      var inputValues = this.state.inputValues;
      for(var i in inputValues){
        inputValues[i] = '';
      };
      this.setState({ inputValues: inputValues, error: null, success: true, loading: false, helpText: "haga click en Examinar para subir un archivo" });
      setTimeout(() => {
        this.props.onSuccess();
        this.setState({ success: false });
        this.props.editarPublicacionReset();
      }, 2000);
    } else if (nextProps.editarPublicacionState.error) {
      this.setState({ error: this.props.editarPublicacionState.error, helpText: "hubo un error al editar la publicacion" });
    }


    if (nextProps.eliminarPublicacionState.loading && !nextProps.eliminarPublicacionState.success) {
      this.setState({ loading: true, helpText: "eliminando publicacion.." });
    } else if (nextProps.eliminarPublicacionState.success && nextProps.eliminarPublicacionState.data) {
      var inputValues = this.state.inputValues;
      for(var i in inputValues){
        inputValues[i] = '';
      };
      this.setState({ inputValues: inputValues, error: null, success: true, loading: false, helpText: "haga click en Examinar para subir un archivo" });
      setTimeout(() => {
        this.props.onSuccess();
        this.setState({ success: false });
  
        this.props.eliminarPublicacionReset();
      }, 2000);
    } else if (nextProps.eliminarPublicacionState.error) {
      this.setState({ error: this.props.eliminarPublicacionState.error,  helpText: "hubo un error al eliminar la publicacion"  });
    }

  }

  onComponentValueChange(e) {
    let val = [];    
    val[e.target.id] = e.target.value;
    val[`${e.target.id}Error`] = '';
    this.setState({ "inputValues": _.extend(this.state.inputValues, val) });
  }

  onSubmit(missingFields) {
    if (missingFields) {
      var val = [];
      missingFields.map((value) => {
        val[`${value.id}Error`] = value.errorMessage;
      });
      this.setState({ "inputValues": _.extend(this.state.inputValues, val) });
    } else {

      let idArchivos = [];
      for(var i in this.state.archivosSubidos){
        idArchivos.push(this.state.archivosSubidos[i].idArchivo)
      }                                                  

      this.props.editarPublicacion({
        idConsorcio: this.props.layout.consorcio.idConsorcio,
        idPublicacion: this.props.item.idPublicacion,
        titulo: this.state.inputValues.titulo,
        fecha: this.state.inputValues.fecha,
        idArchivos: idArchivos
      });
    }
  }

  onSuccesUploadFile(data){
    this.setState((prevState, props) => {
      var values = prevState.archivosSubidos;
      values[data.uuid] = data;
      return { archivosSubidos: values, uploadingFile: false, helpText: "haga click en Examinar para subir un archivo" }
    });
  }

  deleteFile(uuid) {
    this.setState((prevState, props) => {
      var values = prevState.archivosSubidos;
      delete values[uuid];
      return { archivosSubidos: values, forceUpdate: uuid }
    });
  }

  deletePublicacion() {
    this.props.eliminarPublicacion({ 
      idConsorcio: this.props.layout.consorcio.idConsorcio,
      idPublicacion: this.props.item.idPublicacion
    })
  }

  changeStatus() {
    this.setState({ uploadingFile: true, helpText: "subiendo archivo..." })
  }

  render() {
    let archivosSubidos = this.state.archivosSubidos;
    let inputValues = this.state.inputValues;
    let changed = false

    _.map( inputValues, (val, key) => {
      if (val !== this.props.item[key]) {
        changed = true;
      }
    });

    if (archivosSubidos !== this.props.item.archivos) {
      changed = true;
    }
    return (
      <div>
        <SimpleFormWrap
          titulo="Crear publicacion"
          onSubmit={this.onSubmit}
          loading={this.state.loading}
          errorMessage={this.state.error}
          success={this.state.success}
          formType="inline"
          disabled={!changed || this.state.uploadingFile}
          >

          <UploadFile onClick={this.changeStatus} onSuccess={this.onSuccesUploadFile} archivosSubidos={archivosSubidos} shouldUpdate={this.props.visible} forceUpdate={this.state.forceUpdate} onDelete={this.deleteFile}/>

          <InputText id="titulo" name="Titulo" mandatory={true} onChange={this.onComponentValueChange} value={this.state.inputValues.titulo} errorMessage={this.state.inputValues.tituloError}/>

          <InputDate id="fecha" name="Fecha" mandatory={true} onChange={this.onComponentValueChange} value={this.state.inputValues.fecha} errorMessage={this.state.inputValues.fechaError}/>

          <button onClick={this.deletePublicacion} className="btn btn-danger pull-right"> Eliminar Publicacion </button>

        </SimpleFormWrap>
        <br />
        <p className="help-block">{this.state.helpText}</p>
      </div>
    );
  }
}




           
