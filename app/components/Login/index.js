import React, { Component } from 'react';

import logoImgVertical from '../../assets/img/logo-vertical-azul.png'; // eslint-disable-line no-unused-vars

import FormLogin from './FormLogin';

export default class Login extends Component {
  render() {
    return (
      <div>
      <div className="login-box">
        <div className="login-logo">

        <img src={logoImgVertical} width="160" height="187" alt="logo-toro" />

        </div>

          <FormLogin/>

          {/*
          <div className="social-auth-links text-center">
            <p>- OR -</p>
            <a href="#" className="btn btn-block btn-social btn-facebook btn-flat"><i className="fa fa-facebook"></i> Sign in using
              Facebook</Link>
            <a href="#" className="btn btn-block btn-social btn-google btn-flat"><i className="fa fa-google-plus"></i> Sign in using
              Google+</Link>
          </div>
          */}


      </div>

      </div>




    );
  }
}
