import axios from 'axios';
import fetch from 'isomorphic-fetch'
import config from 'config';

import { browserHistory } from 'react-router';

import ls from 'local-storage';

import jwtDecode from 'jwt-decode';

// Action Types

// Define types in the form of 'npm-module-or-myapp/feature-name/ACTION_TYPE_NAME'
const REQUEST_LOGIN = 'auth/REQUEST_LOGIN';
const RECEIVE_LOGIN = 'auth/RECEIVE_LOGIN';
const ERROR_LOGIN = 'auth/ERROR_LOGIN';
const REQUEST_LOGOUT = 'auth/REQUEST_LOGOUT';
const RECEIVE_LOGOUT = 'auth/RECEIVE_LOGOUT';

// initialize auth reducer from localStorage
const initialState = {};

// action creators
function requestLogin(user, pass) {
  return {
    type: REQUEST_LOGIN,
    user,
    pass
  }
}

function receiveLogin(token, userId, email, nombre, apellido, roleId, role) {
  return {
    type: RECEIVE_LOGIN,
    token,
    userId,
    email,
    nombre,
    apellido,
    roleId,
    role,
    receivedAt: Date.now()
  }
}

function errorLogin(error) {
  return {
    type: ERROR_LOGIN,
    errorMessage: error.message,
    errorMessageAlUsuario: 'Los datos ingresados son Incorrectos',
    error,
    receivedAt: Date.now()
  }
}

// export function fetchLogin(user, pass) {
//   return (dispatch) => {
//     dispatch(requestLogin(user, pass));
//     return fetch(`${config.api}/api/Usuarios/autenticarUsuario`, {
//       method: 'POST',
//       headers: {
//         Accept: 'application/json',
//         'Content-Type': 'application/json'
//       },
//       body: JSON.stringify({ email: user, password: pass })
//     })
//       .then((response) => (
//         Promise.all([Promise.resolve(response.status), response.json()])
//       ))
//       .then((values) => {
//         const status = values[0];
//         const json = values[1];
//         if (status === 200) {
//           // store the token in plain localStorage
//           ls('token', json.accessToken);
//
//           // decrypt the token
//           const decoded = jwtDecode(json.accessToken);
//           dispatch(receiveLogin(json.accessToken, decoded.public.id, decoded.public.email, decoded.public.nombre, decoded.public.apellido, decoded.public.roleId, decoded.public.role));
//           // redirect tot he dashboard
//           browserHistory.push('/');
//         } else {
//           dispatch(errorLogin(json.error));
//         }
//       });
//   }
// }

export function fetchLogin(user, pass) {
  console.log("AA");
  return (dispatch) => {
    ls('token', '9bLrkxIEr35mnO5yflr9w1pOJrrx0x6cKEJwEPwcgYs9ZECS4xnaEC8kR8nDjiar');

    // dispatch(receiveLogin(json.accessToken, decoded.public.id, decoded.public.email, decoded.public.nombre, decoded.public.apellido, decoded.public.roleId, decoded.public.role));
    dispatch(receiveLogin('9bLrkxIEr35mnO5yflr9w1pOJrrx0x6cKEJwEPwcgYs9ZECS4xnaEC8kR8nDjiar', "20", "test@test.com", "test", "lastName", "administrador", "1"));
    browserHistory.push('/');
  };
}

export function checkLogin() {
  return (dispatch) => {
    if (ls('token')) {
      // decrypt the token
      const decoded = jwtDecode(ls('token'));
      dispatch(receiveLogin(ls('token'), decoded.public.id, decoded.public.email, decoded.public.nombre, decoded.public.apellido, decoded.public.roleId, decoded.public.role));

      return axios({
        method: 'post',
        url: `${config.api}/api/Usuarios/verificarToken`,
        params: {
          access_token: ls('token')
        },
        headers: {
          Accept: 'application/json'
        }
      })
        .then((response) => {
          if (!response.data.success) {
            logout();
          }
        })
        .catch((error) => {
          logout();
        });

    }

  }
}

export function logout() {
  ls.remove('token')
  browserHistory.push('/login');
  return {
    type: REQUEST_LOGOUT
  }
}

// RAVASOFT-administrador REDUCERS
export default function reducer(state = initialState, action) {
  switch (action.type) {
    case REQUEST_LOGIN :
      return {
        ...state,
        loading: true
      };

    case RECEIVE_LOGIN :
      return {
        ...state,
        token: action.token,
        privateToken: action.privateToken,
        accessToken: action.accessToken,
        role: action.role,
        roleId: action.roleId,
        userId: action.userId,
        email: action.email,
        loading: false
      };

    case ERROR_LOGIN :
      return {
        ...state,
        loading: false,
        errorMessage: action.errorMessage,
        errorMessageAlUsuario: action.errorMessageAlUsuario
      };

    default:
      return state;

  }
}

export const actionCreators = {
  requestLogin,
  receiveLogin,
  fetchLogin,
  checkLogin
};
