// Action Types

function selectPeriodoAction(idPeriodoSelected, idPeriodoActual) {
  return {
    type: 'layout/selectPeriodo',
    timestamp: Date.now(),
    idPeriodoSelected: idPeriodoSelected,
    idPeriodoActual: idPeriodoActual
  }
}

// actions
function unselectConsorcio() {
  return {
    type: 'layout/unselectConsorcio',
    timestamp: Date.now(),
  }
}

function selectPeriodo(idPeriodoSelected, idPeriodoActual) {
  return (dispatch) => {
    dispatch(selectPeriodoAction(idPeriodoSelected, idPeriodoActual))
  }
}

// reducer
const initialState = {};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case 'api/Consorcios/listarConsorcios_SUCCESS' :
      return {
        ...state,
        consorcios: action.result.data,
      };
    case 'layout/selectConsorcio':
      let selectConsorcio = _.cloneDeep(state);
      selectConsorcio = _.pickBy(state, function(value, key) {
        return key !== "consorcioSelected" && key !== "consorcio" && key !== "unidades";
      });
      return {
        ...selectConsorcio,
        consorcioSelected: { ...state.consorcioSelected, consorcioNombreUrl: action.consorcioNombreUrl }
      };
    case 'layout/unselectConsorcio':
      let unselectConsorcio = _.pickBy(state, function(value, key) {
        return key !== "consorcioSelected" && key !== "consorcio" && key !== "unidades";
      });
      return unselectConsorcio
    case 'api/Consorcios/obtenerConsorcio_SUCCESS':
      return {
        ...state,
        consorcio: action.result.data,
      };
    case 'api/Unidades/listarUnidades_SUCCESS' :
      return {
        ...state,
        unidades: action.result.data,
      };
    case 'api/Categorias/listarCategorias_SUCCESS' :
      return {
        ...state,
        categorias: action.result.data,
      };
    case 'auth/RECEIVE_LOGIN':
      console.log("LLL", action);
      return {
        ...state,
        usuario: {nombre: action.nombre, apellido: action.apellido, email: action.email}
      };
    case 'layout/selectPeriodo':
      const result = {
        ...state
      }
      if (action.idPeriodoActual) {
        result.consorcioSelected = { ...state.consorcioSelected, idPeriodoActual: action.idPeriodoActual, idPeriodoSelected: action.idPeriodoSelected }
      } else {
        result.consorcioSelected = { ...state.consorcioSelected, idPeriodoSelected: action.idPeriodoSelected }
      }
      return result;
    case 'api/Unidades/crearUnidad_SUCCESS' :
      return {
        ...state,
        unidades: [...state.unidades, action.result.data],
      };
    default:
      return state;
  }
}

export const actionCreators = {
  unselectConsorcio,
  selectPeriodo
};
