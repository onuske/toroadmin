import axios from 'axios';
import ls from 'local-storage';
import config from 'config';

// Action Types
const initialState = {};

function requestInit(actionType, params) {
  return {
    type: `${actionType}_INIT`,
    params,
    timestamp: Date.now(),
  }
}

function requestSuccess(actionType, result) {
  return {
    type: `${actionType}_SUCCESS`,
    result,
    timestamp: Date.now(),
  }
}

function requestError(actionType, errorMessage) {
  return {
    type: `${actionType}_ERROR`,
    error: errorMessage,
    timestamp: Date.now(),
  }
}

function requestReset(actionType) {
  return {
    type: `${actionType}_RESET`,
    timestamp: Date.now(),
  }
}


function requestGet(method, params) {
  return (dispatch) => {
    dispatch(requestInit(method, params))
    return axios({
      method: 'get',
      url: `${config.api}/${method}`,
      params: {
        access_token: ls('token'),
        ...params
      },
      headers: {
        Accept: 'application/json'
      }
    })
      .then((response) => {
        dispatch(requestSuccess(method, response));
      })
      .catch((error) => {
        dispatch(requestError(method, error.response ? error.response.data.error.message : String(error)));
      });
  }
}

function requestGetBlob(method, params) {
  return (dispatch) => {
    dispatch(requestInit(method, params))
    return axios({
      method: 'get',
      url: `${config.api}/${method}`,
      responseType: 'blob',
      params: {
        access_token: ls('token'),
        ...params
      },
      headers: {
        Accept: 'application/json'
      }
    })
      .then((response) => {
        dispatch(requestSuccess(method, response));
      })
      .catch((error) => {
        dispatch(requestError(method, error.response ? error.response.data.error.message : String(error)));
      });
  }
}


export function requestPost(method, params) {
  return (dispatch) => {
    dispatch(requestInit(method, params))
    return axios({
      method: 'post',
      url: `${config.api}/${method}`,
      params: { access_token: ls('token') },
      data: params,
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      }
    })
      .then((response) => {
        dispatch(requestSuccess(method, response));
      })
      .catch((error) => {
        dispatch(requestError(method, error.response ? error.response.data.error.message : String(error)));
      });
  }
}

function subirArchivo(file) {
  return (dispatch) => {
    let method = 'api/Archivos/subirArchivo';
    dispatch(requestInit(method, {file: file}))
    const data = new FormData()
    data.append('mainFile', file)

    return axios.post(`${config.api}/${method}`, data, {
      params: { access_token: ls('token') }
    })
      .then((response) => {
        dispatch(requestSuccess(method, response));
      })
      .catch((error) => {
        dispatch(requestError(method, error.response ? error.response.data.error.message : String(error)));
      });
  }
}

function subirArchivoReset() {
  return (dispatch) => {
    dispatch(requestReset('api/Archivos/subirArchivo'))
  }
}

function borrarArchivo(params) {
  return requestPost('api/Archivos/borrarArchivo', params);
}

function crearUnidad(params) {
  return requestPost('api/Unidades/crearUnidad', params);
}

function crearUnidadReset() {
  return (dispatch) => {
    dispatch(requestReset('api/Unidades/crearUnidad'))
  }
}

function crearResponsable(params) {
  return requestPost('api/Responsables/crearResponsable', params)
}

function crearResponsableReset() {
  return (dispatch) => {
    dispatch(requestReset('api/Responsables/crearResponsable'))
  }
}

function crearConsorcio(params) {
  return requestPost('api/Consorcios/crearConsorcio', params);
}

function crearConsorcioReset() {
  return (dispatch) => {
    dispatch(requestReset('api/Consorcios/crearConsorcio'))
  }
}

function actualizarConsorcio(params) {
  return requestPost('api/Consorcios/actualizarConsorcio', params);
}

function actualizarConsorcioReset() {
  return (dispatch) => {
    dispatch(requestReset('api/Consorcios/actualizarConsorcio'))
  }
}

export function obtenerConsorcio(params) {
  return requestGet('api/Consorcios/obtenerConsorcio', params);
}

function listarUnidades(params) {
  return requestGet('api/Unidades/listarUnidades', params);
}

function listarConsorcios(params) {
  return requestGet('api/Consorcios/listarConsorcios', params);
}

function listarBancos() {
  return requestGet('api/Bancos/listarBancos');
}

function listarCuentas(params) {
  return requestGet('api/CuentasBancarias/listarCuentas', params);
}

function pregenerarPeriodo(params) {
  return requestPost('api/Periodos/pregenerarPeriodo', params);
}

function pregenerarPeriodoReset() {
  return (dispatch) => {
    dispatch(requestReset('api/Periodos/pregenerarPeriodo'))
  }
}

function generarPeriodo(params) {
    return requestPost('api/Periodos/generarPeriodo', params);
}

function generarPeriodoReset() {
  return (dispatch) => {
    dispatch(requestReset('api/Periodos/generarPeriodo'))
  }
}

function generarExtracto(params) {
  return requestPost('api/Extractos/generarExtracto', params);
}

function generarExtractoReset() {
  return (dispatch) => {
    dispatch(requestReset('api/Extractos/generarExtracto'))
  }
}

function listarCategorias(params) {
  return requestGet('api/Categorias/listarCategorias', params);
}

function obtenerMovimientosBanco(params) {
  return requestGet('api/MovimientosBancos/obtenerMovimientosBanco', params);
}

function setearMovimientoBanco(params) {
  return requestGet('api/MovimientosBancos/setearMovimientoBanco', params);
}

function setearMovimientoBancoReset() {
  return (dispatch) => {
    dispatch(requestReset('api/MovimientosBancos/setearMovimientoBanco'))
  }
}

function crearMovimientosCaja(params) {
  return requestPost('api/MovimientosCajas/crearMovimientosCaja', params);
}

function obtenerMovimientosCaja(params) {
  return requestGet('api/MovimientosCajas/obtenerMovimientosCaja', params);
}

function setearMovimientoCaja(params) {
  return requestGet('api/MovimientosCajas/setearMovimientoCaja', params);
}

function crearRecaudacion(params) {
  return requestPost('api/Recaudaciones/crearRecaudacion', params);
}

function crearRecaudacionReset() {
  return (dispatch) => {
    dispatch(requestReset('api/Recaudaciones/crearRecaudacion'))
  }
}

function obtenerRecaudaciones(params) {
  return requestGet('api/Recaudaciones/obtenerRecaudaciones', params);
}

function obtenerRecaudacionesPorUuid(params) {
  return requestGet('api/Recaudaciones/obtenerRecaudacionesPorUuid', params);
}

function setearRecaudacion(params) {
  return requestGet('api/Recaudaciones/setearRecaudacion', params);
}

function crearAjuste(params) {
  return requestPost('api/Ajustes/crearAjuste', params);
}

function crearAjusteReset() {
  return (dispatch) => {
    dispatch(requestReset('api/Ajustes/crearAjuste'))
  }
}

function obtenerAjustes(params) {
  return requestGet('api/Ajustes/obtenerAjustes', params);
}

function crearPublicacion(params) {
  return requestPost('api/Publicaciones/crearPublicacion', params);
}

function crearPublicacionReset() {
  return (dispatch) => {
    dispatch(requestReset('api/Publicaciones/crearPublicacion'))
  }
}

function obtenerPublicaciones(params) {
  return requestGet('api/Publicaciones/obtenerPublicaciones', params);
}

function editarPublicacion(params) {
  return requestPost('api/Publicaciones/editarPublicacion', params);
}

function editarPublicacionReset() {
  return (dispatch) => {
    dispatch(requestReset('api/Publicaciones/editarPublicacion'))
  }
}

function eliminarPublicacion(params) {
  return requestPost('api/Publicaciones/eliminarPublicacion', params);
}

function eliminarPublicacionReset() {
  return (dispatch) => {
    dispatch(requestReset('api/Publicaciones/eliminarPublicacion'))
  }
}

function obtenerPeriodos(params) {
  return requestGet('api/Periodos/obtenerPeriodos', params);
}

export function obtenerPeriodosReset() {
  return (dispatch) => {
    dispatch(requestReset('api/Periodos/obtenerPeriodos'))
  }
}

function descargarArchivo(params) {
  return requestGetBlob('api/Archivos/descargarArchivo', params);
}

function obtenerMensajes(params) {
  return requestGet('api/Mensajes/obtenerMensajes', params);
}

function crearMensaje(params) {
  return requestPost('api/Mensajes/crearMensaje', params);
}

function crearMensajeReset() {
  return (dispatch) => {
    dispatch(requestReset('api/Mensajes/crearMensaje'))
  }
}




//MovimientosCajas


// API REDUCERS
/*
Las acciones de api generan siempre un objeto igual, el mismo cambia segun el estado de la llamada http en cada momento
pero siempre manteniendo la misma forma. A saber:
- al iniciarse la llamada http:
 {
   loading: true
 }
 - al recibir la respuesta del servidor exitosamente:
  {
    loading: false,
    success: true,
    data: [...] // lo que devuelva el servidor, presumiblemente array u objeto.
  }
 - al ocurrir un error en la llamada http (ya sea respuesta del server o error de conexion):
   {
     loading: false,
     success: false,
     error: "mensaje de error"
   }
*/
export default function reducer(state = initialState, action) {

  if (/api\//.test(action.type) || /subirArchivo/.test(action.type)) {
    let result = {
      ...state
    }
    if (/_INIT/.test(action.type)) {
      result[action.type.split('/')[2].split('_INIT')[0]] = {loading: true};
      return result;
    } else if (/_SUCCESS/.test(action.type)) {
      if(/subirArchivo/.test(action.type)) {
        result[action.type.split('/')[2].split('_SUCCESS')[0]] = {
          loading: false,
          success: true,
          data: action.result
        }
      } else {
        result[action.type.split('/')[2].split('_SUCCESS')[0]] = {
          loading: false,
          success: true,
          data: action.result.data
        }
      }
      result.timestamp = action.timestamp
      return result;
    } else if (/_ERROR/.test(action.type)) {
      result[action.type.split('/')[2].split('_ERROR')[0]] = {
        loading: false,
        error: action.error
      }
      return result;
    } else if (/_RESET/.test(action.type)) {
      result[action.type.split('/')[2].split('_RESET')[0]] = {
        loading: false,
        data: {}
      }
      return result;
    }

  } else {
    switch (action.type) {
      default:
        return state;
    }
  }
}

export const actionCreators = {
  crearConsorcio,
  crearConsorcioReset,
  crearUnidad,
  crearUnidadReset,
  obtenerConsorcio,
  listarUnidades,
  crearResponsable,
  crearResponsableReset,
  listarBancos,
  listarCuentas,
  actualizarConsorcio,
  actualizarConsorcioReset,
  listarConsorcios,
  subirArchivo,
  subirArchivoReset,
  borrarArchivo,
  generarExtracto,
  generarExtractoReset,
  pregenerarPeriodo,
  pregenerarPeriodoReset,
  generarPeriodo,
  generarPeriodoReset,
  listarCategorias,
  obtenerMovimientosBanco,
  setearMovimientoBanco,
  setearMovimientoBancoReset,
  crearMovimientosCaja,
  obtenerMovimientosCaja,
  setearMovimientoCaja,
  obtenerPeriodos,
  obtenerPeriodosReset,
  descargarArchivo,
  obtenerMensajes,
  crearMensaje,
  crearMensajeReset,
  crearRecaudacion,
  crearRecaudacionReset,
  obtenerRecaudaciones,
  obtenerRecaudacionesPorUuid,
  setearRecaudacion,
  crearAjuste,
  crearAjusteReset,
  obtenerAjustes,
  crearPublicacion,
  crearPublicacionReset,
  obtenerPublicaciones,
  editarPublicacion,
  editarPublicacionReset,
  eliminarPublicacion,
  eliminarPublicacionReset
};
