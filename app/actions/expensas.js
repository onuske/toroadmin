// Action Types

const CATEGORIA = {
  PAGO_DE_EXPENSAS : 1,
  INGRESO_A_CUENTA : 2,
  GASTO: 3, // GASTOS_BANCARIOS : 6
  MOVIMIENTO_YA_LIQUIDADO : 4,
  COMPENSACION : 5,
}

function setStep(step) {
  return {
    type: 'expensas/setStep',
    step: step
  }
}

function setPreview(preview) {
  return {
    type: 'expensas/setPreview',
    preview: preview
  }
}

function setPeriodo(idPeriodo) {
  return {
    type: 'expensas/setPeriodo',
    idPeriodo: idPeriodo
  }
}

function setTotal(monto) {
  return {
    type: 'expensas/setTotal',
    monto: monto
  }
}

function reset() {
  return {
    type: 'expensas/reset'
  }
}

function setConciliacion(store) {
  return {
    type: 'expensas/setConciliacion',
    conciliacion: store
  }
}

// The initial state of Expensas
const initialState = {
  idPeriodo: null,
  preview: true,
  step: 1,
  total: 0,
  conciliacion: {
    banco: [],
    caja: [],
    recaudacion: [],
    categorias: [],
    unidadesSinAsignar: [],
    notValidate: false
  },
};


export default function reducer(state = initialState, action) {
  switch (action.type) {
    case 'expensas/setStep':
      return {
        ...state,
        step: action.step
      };
    case 'expensas/setPreview':
      return {
        ...state,
        preview: action.preview
      };
    case 'expensas/setPeriodo':
      return {
        ...state,
        idPeriodo: action.idPeriodo
      };
    case 'expensas/setTotal':
      return {
        ...state,
        total: action.monto
      };
    case 'expensas/reset':
      state = initialState
      return state
    case 'expensas/setConciliacion':
      let conciliacion = {}
      if (state.conciliacion) {
        conciliacion = _.cloneDeep(state.conciliacion);
        Object.entries(action.conciliacion).map(([key, val]) => {

          /*seteo de categoria y unidad y/o error de validacion para movimientos banco, caja y recaudacion*/
          if ( !_.isEmpty(conciliacion[key]) && (key === "banco" || key === "caja" || key === "recaudacion")) {
            
            Object.entries(val).map(([index, value]) => {
              if (typeof value === 'object') {

                if ( value.observaciones ) {
                  conciliacion[key][index].data.observaciones = value.observaciones
                } else {

                  if ( value.idCategoria !== CATEGORIA.PAGO_DE_EXPENSAS ) {
                    delete conciliacion[key][index].idUnidad;
                  }
                  if (value.idCategoria !== CATEGORIA.GASTO) {
                    delete conciliacion[key][index].idSubCategoria;
                  }

                  if (value.idCategoria) {
                    conciliacion[key][index].idCategoria = value.idCategoria;
                  }

                  if (value.idSubCategoria) {
                    conciliacion[key][index].idSubCategoria = value.idSubCategoria;
                  }

                  if (value.idUnidad) {
                    conciliacion[key][index].idUnidad = value.idUnidad;
                  }

                  /* manejo de error */
                  if (value._categoriaError) {
                    conciliacion[key][`${index}_categoriaError`] = value._categoriaError;
                  } else if (value._subCategoriaError) {
                    conciliacion[key][`${index}_subCategoriaError`] = value._subCategoriaError;
                  } else if (value._unidadError) {
                    conciliacion[key][`${index}_unidadError`] = value._unidadError;
                  } else {             
                    conciliacion[key][`${index}_categoriaError`] = '';
                    conciliacion[key][`${index}_subCategoriaError`] = '';
                    conciliacion[key][`${index}_unidadError`] = '';
                  }
                  
                }
              }

            })

          } else if (state.conciliacion[key] !==  val) {
            conciliacion[key] = val
          }
        })
      } else {
        conciliacion = action.conciliacion;
      }
      return {
        ...state,
        conciliacion
      };
    default:
      return state;
  }
}

export const actionCreators = {
  setStep,
  setPreview,
  setPeriodo,
  setTotal,
  reset,
  setConciliacion
};
