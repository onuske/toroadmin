// These are the pages you can go to.
// They are all wrapped in the App component, which should contain the navbar etc
// See http://blog.mxstbr.com/2016/01/react-apps-with-pages for more information
// about the code splitting business
import { getAsyncInjectors } from 'utils/asyncInjectors';
import ls from 'local-storage';

import { obtenerConsorcio, obtenerPeriodosReset } from 'actions/api';

const errorLoading = (err) => {
  console.error('Dynamic page loading failed', err); // eslint-disable-line no-console
};

const loadModule = (cb) => (componentModule) => {
  cb(null, componentModule.default);
};

export default function createRoutes(store) {
  // Create reusable async injectors using getAsyncInjectors factory
  const { injectReducer, injectSagas } = getAsyncInjectors(store); // eslint-disable-line no-unused-vars

  return [
    {
      path: '/login',
      name: 'login',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          System.import('components/Login'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([component]) => {
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    },
    {
      path: '/recuperarcontrasena',
      name: 'lostPassword',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          System.import('components/LostPassword'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([component]) => {
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    },
    {
      path: '/',
      name: 'home',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          System.import('components/Home'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([component]) => {
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
      onEnter(nextState, transition) {
        if (ls('token') == null) {
          transition('login');
        }
        store.dispatch({
          type: 'layout/unselectConsorcio'
        })
      }
    },
    {
      path: '/crearconsorcio',
      name: 'createConsorcio',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          System.import('components/CreateConsorcio'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([component]) => {
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
      onEnter(nextState, transition) {
        if (ls('token') == null) {
          transition('login');
        }
      }
    },
    {
      path: '/:consorcioNombreUrl',
      name: 'consorcio',
      childRoutes: [{
        path: 'messages/:id',
        onEnter: ({ params }, replace) => replace(`/messages/${params.id}`)
      }],
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          System.import('components/Consorcio'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([component]) => {
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
      onEnter(nextState, transition) {
        if (ls('token') == null) {
          transition('login');
        }
        store.dispatch({
          type: 'layout/selectConsorcio',
          consorcioNombreUrl: nextState.params.consorcioNombreUrl
        })

        store.dispatch({
          type: 'expensas/reset'
        })

        store.dispatch(obtenerPeriodosReset())
        
        store.dispatch(obtenerConsorcio({nombreUrl: nextState.params.consorcioNombreUrl}))
      }
    },
    // {
    //   path: '/:consorcioNombreUrl/extracto',
    //   name: 'extracto',
    //   getComponent(nextState, cb) {
    //     const importModules = Promise.all([
    //       System.import('components/Consorcio/Extracto'),
    //     ]);
    //
    //     const renderRoute = loadModule(cb);
    //
    //     importModules.then(([component]) => {
    //       renderRoute(component);
    //     });
    //
    //     importModules.catch(errorLoading);
    //   },
    //   onEnter(nextState, transition) {
    //     if (ls('token') == null) {
    //       transition('login');
    //     }
    //     store.dispatch({
    //       type: 'layout/selectConsorcio',
    //       consorcioNombreUrl: nextState.params.consorcioNombreUrl
    //     })
    //     obtenerConsorcio({nombreUrl: nextState.params.consorcioNombreUrl})(store.dispatch)
    //   }
    // },
    // {
    //   path: '/:consorcioNombreUrl/movimientos',
    //   name: 'movimientos',
    //   getComponent(nextState, cb) {
    //     const importModules = Promise.all([
    //       System.import('components/Consorcio/Movimientos'),
    //     ]);
    //
    //     const renderRoute = loadModule(cb);
    //
    //     importModules.then(([component]) => {
    //       renderRoute(component);
    //     });
    //
    //     importModules.catch(errorLoading);
    //   },
    //   onEnter(nextState, transition) {
    //     if (ls('token') == null) {
    //       transition('login');
    //     }
    //     store.dispatch({
    //       type: 'layout/selectConsorcio',
    //       consorcioNombreUrl: nextState.params.consorcioNombreUrl
    //     })
    //     obtenerConsorcio({nombreUrl: nextState.params.consorcioNombreUrl})(store.dispatch)
    //   }
    // },
    // {
    //   path: '/:consorcioNombreUrl/expensas',
    //   name: 'expensas',
    //   getComponent(nextState, cb) {
    //     const importModules = Promise.all([
    //       System.import('components/Consorcio/Expensas'),
    //     ]);
    //
    //     const renderRoute = loadModule(cb);
    //
    //     importModules.then(([component]) => {
    //       renderRoute(component);
    //     });
    //
    //     importModules.catch(errorLoading);
    //   },
    //   onEnter(nextState, transition) {
    //     if (ls('token') == null) {
    //       transition('login');
    //     }
    //     store.dispatch({
    //       type: 'layout/selectConsorcio',
    //       consorcioNombreUrl: nextState.params.consorcioNombreUrl
    //     })
    //     obtenerConsorcio({nombreUrl: nextState.params.consorcioNombreUrl})(store.dispatch)
    //   }
    // },
    {
      path: '*',
      name: 'notfound',
      getComponent(nextState, cb) {
        System.import('components/App/NotFoundPage')
          .then(loadModule(cb))
          .catch(errorLoading);
      },
    }
  ];
}
